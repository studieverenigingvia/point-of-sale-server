import os


def to_bool(value: str) -> bool:
    return value in ["1", "True", "true"]


basedir = os.path.abspath(os.path.dirname(__file__))
LOG_FILE = os.path.join(basedir, "log/app_logger.log")

DEBUG = to_bool(os.environ.get("DEBUG", "0"))

VIADUCT_CLIENT_ID = os.environ.get("VIADUCT_CLIENT_ID", "pos-local")
VIADUCT_CLIENT_SECRET = os.environ.get(
    "VIADUCT_CLIENT_SECRET", "[VIADUCT_LOCAL_SECRET_KEY]"
)

SQLALCHEMY_DATABASE_URI = os.environ.get("SQLALCHEMY_DATABASE_URI")
SQLALCHEMY_DATABASE_TEST_URI = os.environ.get("SQLALCHEMY_DATABASE_TEST_URI")
SQLALCHEMY_TRACK_MODIFICATIONS = to_bool(
    os.environ.get("SQLALCHEMY_TRACK_MODIFICATIONS", "0")
)

GOOGLE_API_JSON_KEY = os.environ.get("GOOGLE_API_JSON_KEY", "google-creds-test.json")
SQLALCHEMY_TRACK_MODIFICATIONS = to_bool(
    os.environ.get("SQLALCHEMY_TRACK_MODIFICATIONS", "0")
)
SQLALCHEMY_ECHO = to_bool(os.environ.get("SQLALCHEMY_ECHO", "0"))

BROKER_URL = os.environ.get("BROKER_URL", "redis://redis")

MOLLIE_API_KEY = os.environ.get("MOLLIE_API_KEY", "test_MOLLIE_API_KEY")
NGROK_HOST = os.environ.get("NGROK_HOST", "")

GSUITE_TREASURER_MAIL = os.environ.get("GSUITE_TREASURER_MAIL", "pos@svia.nl")

SENTRY_DSN = os.environ.get("SENTRY_DSN", "")
