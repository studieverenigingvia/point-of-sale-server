from unittest.mock import patch

import freezegun
import pytest

from app.models import Product
from sqlalchemy.orm import Session
from starlette.testclient import TestClient

from app.models.card_terminal import CardTerminal
from app.models.transaction import TransactionStatus, TransactionCard

from app.tasks.recurring.pending_card_transactions import (
    expire_idle_pending_card_transactions,
)

from tests.util.transactions import (
    _create_pending_direct_card_purchase,
)


def test_expiring(
    db_session: Session,
    bar_client: TestClient,
    products: dict[str, Product],
) -> None:
    with freezegun.freeze_time("2024-01-01"):
        r1 = bar_client.post(
            "/api/checkout/card/",
            json={
                "nonce": "123",
                "products": [{"id": products["bar_beer"].id, "amount": 1}],
            },
        )
        assert r1.status_code == 200, r1.json()
        db_session.commit()

    with freezegun.freeze_time("2024-01-02"):
        expire_idle_pending_card_transactions()
        db_session.commit()

    r2 = bar_client.post(f'/api/transactions/{r1.json()["transaction_id"]}/finish/')
    assert r2.status_code == 400, r2.json()
    assert r2.json()["error"] == "invalid_transaction_state"


@pytest.mark.usefixtures("exact_finished_settings")
def test_expire_multiple(
    db_session: Session,
    products: dict[str, Product],
) -> None:
    transaction1 = _create_pending_direct_card_purchase(
        db_session, products["bar_beer"]
    )
    transaction2 = _create_pending_direct_card_purchase(
        db_session, products["bar_beer"]
    )
    db_session.flush()

    # Should not throw
    expire_idle_pending_card_transactions()

    db_session.flush()
    db_session.refresh(transaction1)
    db_session.refresh(transaction2)

    assert transaction1.status == TransactionStatus.EXPIRED
    assert transaction2.status == TransactionStatus.EXPIRED


@pytest.mark.usefixtures("exact_finished_settings")
def test_expire_card_multiple(
    db_session: Session,
    products: dict[str, Product],
    card_terminals: dict[str, CardTerminal],
) -> None:
    card_data = TransactionCard(
        card_terminal_id=card_terminals["mollie"].id,
        external_id="123",
    )

    transaction1 = _create_pending_direct_card_purchase(
        db_session, products["bar_beer"], card_data
    )
    transaction2 = _create_pending_direct_card_purchase(
        db_session, products["bar_beer"], card_data
    )
    db_session.flush()

    # Should not throw
    with patch(
        "app.service.card_terminal_update_service._get_status_of_mollie_transaction",
        return_value=("details", TransactionStatus.EXPIRED),
    ):
        expire_idle_pending_card_transactions()

    db_session.flush()
    db_session.refresh(transaction1)
    db_session.refresh(transaction2)

    assert transaction1.status == TransactionStatus.EXPIRED
    assert transaction2.status == TransactionStatus.EXPIRED
