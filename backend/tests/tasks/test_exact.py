import logging
import math
from datetime import date
from typing import Literal
from xml.etree import ElementTree

import pytest
from _pytest.logging import LogCaptureFixture
from requests_mock import Mocker
from sqlalchemy.orm import Session
from starlette.testclient import TestClient

from app.models import SepaBatch, SepaBatchOrder
from app.domains.exact.export.exact_export import ExactExport
from app.domains.exact import exact_tasks

EXACT_CREATE_RESPONSE = """
<?xml version="1.0" encoding="utf-8"?>
<eExact xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="eExact-XML.xsd">
  <Messages>
    <Message type="2">
      <Topic code="GLTransactions" node="Journal">
        <Data keyAlt="20" />
      </Topic>
      <Date>2020-01-06T22:40:21</Date>
      <Description>Bijgewerkt</Description>
    </Message>
    <Message type="2">
      <Topic code="GLTransactions" node="GLTransaction">
        <Data keyAlt="19200013" />
      </Topic>
      <Date>2020-01-06T22:40:21</Date>
      <Description>Aangemaakt</Description>
    </Message>
  </Messages>
</eExact>""".strip()  # noqa: E501


@pytest.fixture
def administration_unexecuted_sepa(
    db_session, sepa_account, sepa_config, products, transaction_account_order
):
    # TODO The service classes all use the database in de application context.

    sepa_account.balance = -120

    sepa_batch = SepaBatch(
        created_at=date(2020, 9, 1),
        updated_at=date(2020, 9, 1),
        description="Unexported batch for exact testing",
        execution_date=date(2100, 9, 1),  # FAR FAR AWAY
        success=False,
    )

    sepa_batch.pending_orders = [
        SepaBatchOrder(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            account_id=sepa_account.id,
            price=120,
        ),
    ]

    db_session.add(sepa_batch)
    db_session.commit()

    return {"sepa_batch": sepa_batch}


@pytest.mark.usefixtures("exact_finished_settings", "sepa_config")
def test_exact_export_normal_transactions(
    db_session: Session,
    admin_client: TestClient,
    administration_unexecuted_sepa: dict[Literal["sepa_batch"], SepaBatch],
    requests_mocker: Mocker,
) -> None:
    exact_export = ExactExport(
        created_at=date(2020, 9, 2),
        updated_at=date(2020, 9, 2),
        date=date(2020, 9, 1),
        division="1337",
        status="unexported",
    )
    db_session.add(exact_export)
    db_session.commit()

    def assert_request_text(text: str) -> bool:
        xml_root = ElementTree.fromstring(text)
        opening_balance = xml_root.find(".//OpeningBalance")
        closing_balance = xml_root.find(".//ClosingBalance")
        assert opening_balance is not None and opening_balance.text is not None
        assert closing_balance is not None and closing_balance.text is not None
        assert math.isclose(float(opening_balance.text), 0)
        assert math.isclose(float(closing_balance.text), 1.2)
        return True

    requests_mocker.get(
        "https://start.exactonline.nl/api/v1/1337/"
        "financialtransaction/TransactionLines",
        json={"d": []},  # No exports currently present.
    )

    requests_mocker.post(
        "https://start.exactonline.nl/docs/XMLUpload.aspx",
        text=EXACT_CREATE_RESPONSE,
    )

    exact_tasks.export_transactions(exact_export_id=exact_export.id)
    db_session.refresh(exact_export)
    assert exact_export.status == "exported"
    assert assert_request_text(requests_mocker.request_history[-1].text)

    sepa_batch = administration_unexecuted_sepa["sepa_batch"]
    rv = admin_client.post(f"/api/admin/sepa_batches/{sepa_batch.id}/execute/")
    assert rv.status_code == 204, rv.json()
    rv = admin_client.delete(f"/api/admin/exact/export/{exact_export.id}/")
    assert rv.status_code == 200, rv.json()
    db_session.refresh(exact_export)
    assert exact_export.status == "deleted"

    exact_tasks.export_transactions(exact_export_id=exact_export.id)
    db_session.refresh(exact_export)
    assert exact_export.status == "exported"
    assert assert_request_text(requests_mocker.request_history[-1].text)


TRANSACTION_LINES_RESPONSE = {
    "d": [
        {
            "__metadata": {
                "uri": "https://start.exactonline.nl/api/v1/3119907/"
                "financialtransaction/TransactionLines(guid'abc')",
                "type": "Exact.Web.Api.Models.Financial.TransactionLine",
            },
            "Date": "/Date(1641600000000)/",
            "Description": "BOUGHT - Bier",
            "EntryNumber": 22240001,
        }
    ]
}


@pytest.mark.usefixtures("exact_finished_settings")
def test_exact_export_existing_booking(
    db_session: Session,
    requests_mocker: Mocker,
    caplog: LogCaptureFixture,
) -> None:
    exact_export = ExactExport(
        created_at=date(2020, 9, 2),
        updated_at=date(2020, 9, 2),
        date=date(2020, 9, 1),
        division="storage_division",
        status="unexported",
    )
    db_session.add(exact_export)
    db_session.commit()

    caplog.set_level(logging.ERROR)
    requests_mocker.get(
        "https://start.exactonline.nl/api/v1/1337/financialtransaction/TransactionLines"
        "?%24filter=JournalCode+eq+%2724%27+and+Date+eq+datetime%272020-09-01%27"
        "&%24select=EntryNumber%2CDate%2CDescription&%24top=1",
        json=TRANSACTION_LINES_RESPONSE,
    )

    exact_tasks.export_transactions(exact_export_id=exact_export.id)

    db_session.refresh(exact_export)
    assert exact_export.updated_at != date(2020, 9, 2)
    assert exact_export.status == "unexported"
    assert len(caplog.records) == 1
