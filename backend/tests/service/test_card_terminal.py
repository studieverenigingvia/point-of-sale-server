import datetime
from unittest.mock import patch, Mock

import freezegun
import pytest
from mollie.api.objects.payment import Payment
from requests_mock import Mocker
from sqlalchemy.orm import Session

from app.models import Product
from app.models.card_terminal import CardTerminal
from app.models.transaction import TransactionStatus, Transaction, TransactionCard
from app.service import card_terminal_service, card_terminal_update_service
from app.service.card_terminal_service import start_card_transaction
from app.service.flow.utils.process_flow import ProcessFlowCardTerminalData
from tests.util.transactions import _create_pending_direct_card_purchase

MOLLIE_PAYMENT_PAID = Payment(
    {
        "resource": "payment",
        "id": "tr_aLf4q5jNj2",
        "mode": "test",
        "createdAt": "2024-02-13T13:41:05+00:00",
        "amount": {"value": "1.00", "currency": "EUR"},
        "description": "asd",
        "method": "pointofsale",
        "metadata": None,
        "status": "paid",
        "paidAt": "2024-02-13T13:42:47+00:00",
        "amountRefunded": {"value": "0.00", "currency": "EUR"},
        "amountRemaining": {"value": "1.00", "currency": "EUR"},
        "locale": "en_US",
        "countryCode": "NL",
        "profileId": "pfl_Qq54yD43me",
        "sequenceType": "oneoff",
        "redirectUrl": None,
        "settlementAmount": {"value": "1.00", "currency": "EUR"},
        "details": {"terminalId": "term_Yz4LaumuV6xh2QYDrGMkH"},
        "_links": {
            "self": {
                "href": "https://api.mollie.com/v2/payments/tr_aLf4q5jNj2",
                "type": "application/hal+json",
            },
            "dashboard": {
                "href": "https://my.mollie.com/dashboard/org_1238121/payments/tr_aLf4q5jNj2",  # noqa
                "type": "text/html",
            },
            "changePaymentState": {
                "href": "https://www.mollie.com/checkout/test-mode?method=pointofsale\u0026token=6.c0utur",  # noqa
                "type": "text/html",
            },
            "terminal": {
                "href": "https://api.mollie.com/v2/terminals/term_Yz4LaumuV6xh2QYDrGMkH",  # noqa
                "type": "application/hal+json",
            },
            "documentation": {
                "href": "https://docs.mollie.com/reference/v2/payments-api/get-payment",  # noqa
                "type": "text/html",
            },
        },
    }
)

MOLLIE_PAYMENT_CANCELLED = Payment(
    {
        "resource": "payment",
        "id": "tr_YB68GnofWS",
        "mode": "test",
        "createdAt": "2024-02-13T13:43:16+00:00",
        "amount": {"value": "1.00", "currency": "EUR"},
        "description": "asd",
        "method": "pointofsale",
        "metadata": None,
        "status": "canceled",
        "canceledAt": "2024-02-13T13:43:39+00:00",
        "locale": "en_US",
        "countryCode": "NL",
        "profileId": "pfl_Qq54yD43me",
        "sequenceType": "oneoff",
        "redirectUrl": None,
        "details": {"terminalId": "term_Yz4LaumuV6xh2QYDrGMkH"},
        "_links": {
            "self": {
                "href": "https://api.mollie.com/v2/payments/tr_YB68GnofWS",
                "type": "application/hal+json",
            },
            "dashboard": {
                "href": "https://my.mollie.com/dashboard/org_1238121/payments/tr_YB68GnofWS",  # noqa
                "type": "text/html",
            },
            "terminal": {
                "href": "https://api.mollie.com/v2/terminals/term_Yz4LaumuV6xh2QYDrGMkH",  # noqa
                "type": "application/hal+json",
            },
            "documentation": {
                "href": "https://docs.mollie.com/reference/v2/payments-api/get-payment",  # noqa
                "type": "text/html",
            },
        },
    }
)

MOLLIE_PAYMENT_EXPIRED = Payment(
    {
        "resource": "payment",
        "id": "tr_LDXSWGrNfS",
        "mode": "test",
        "createdAt": "2024-02-13T13:43:50+00:00",
        "amount": {"value": "1.00", "currency": "EUR"},
        "description": "asd",
        "method": "pointofsale",
        "metadata": None,
        "status": "expired",
        "expiredAt": "2024-02-13T13:44:15+00:00",
        "locale": "en_US",
        "countryCode": "NL",
        "profileId": "pfl_Qq54yD43me",
        "sequenceType": "oneoff",
        "redirectUrl": None,
        "details": {"terminalId": "term_Yz4LaumuV6xh2QYDrGMkH"},
        "_links": {
            "self": {
                "href": "https://api.mollie.com/v2/payments/tr_LDXSWGrNfS",
                "type": "application/hal+json",
            },
            "dashboard": {
                "href": "https://my.mollie.com/dashboard/org_1238121/payments/tr_LDXSWGrNfS",  # noqa
                "type": "text/html",
            },
            "terminal": {
                "href": "https://api.mollie.com/v2/terminals/term_Yz4LaumuV6xh2QYDrGMkH",  # noqa
                "type": "application/hal+json",
            },
            "documentation": {
                "href": "https://docs.mollie.com/reference/v2/payments-api/get-payment",  # noqa
                "type": "text/html",
            },
        },
    }
)

MOLLIE_PAYMENT_PENDING = Payment(
    {
        "resource": "payment",
        "id": "tr_aLf4q5jNj2",
        "mode": "test",
        "createdAt": "2024-02-13T13:41:05+00:00",
        "amount": {"value": "1.00", "currency": "EUR"},
        "description": "asd",
        "method": "pointofsale",
        "metadata": None,
        "status": "open",
        "isCancelable": False,
        "expiresAt": "2024-02-13T13:56:05+00:00",
        "locale": "en_US",
        "countryCode": "NL",
        "profileId": "pfl_Qq54yD43me",
        "sequenceType": "oneoff",
        "redirectUrl": None,
        "settlementAmount": {"value": "1.00", "currency": "EUR"},
        "details": {"terminalId": "term_Yz4LaumuV6xh2QYDrGMkH"},
        "_links": {
            "self": {
                "href": "https://api.mollie.com/v2/payments/tr_aLf4q5jNj2",
                "type": "application/hal+json",
            },
            "dashboard": {
                "href": "https://my.mollie.com/dashboard/org_1238121/payments/tr_aLf4q5jNj2",  # noqa
                "type": "text/html",
            },
            "changePaymentState": {
                "href": "https://www.mollie.com/checkout/test-mode?method=pointofsale\u0026token=6.c0utur",  # noqa
                "type": "text/html",
            },
            "terminal": {
                "href": "https://api.mollie.com/v2/terminals/term_Yz4LaumuV6xh2QYDrGMkH",  # noqa
                "type": "application/hal+json",
            },
            "documentation": {
                "href": "https://docs.mollie.com/reference/v2/payments-api/get-payment",
                "type": "text/html",
            },
        },
    }
)


@pytest.mark.parametrize("brand", ["sepay", "mollie"])
def test_start_card_transaction(
    db_session: Session, brand: str, card_terminals: dict[str, CardTerminal]
) -> None:
    with patch(
        "app.tasks.single.sepay.sepay_create_transaction.delay", Mock()
    ) as mocked_method:
        with patch.object(
            card_terminal_service,
            "create_card_terminal_payment",
            Mock(return_value={"id": "id"}),
        ) as mocked_method2:
            input = ProcessFlowCardTerminalData(card_terminals[brand])

            transaction = 1
            total = 100

            terminal, transaction_card = start_card_transaction(
                input, transaction, total
            )

            if brand == "sepay":
                mocked_method.assert_called_once()
                mocked_method2.assert_not_called()
            elif brand == "mollie":
                mocked_method.assert_not_called()
                mocked_method2.assert_called_once()

            # Check if the transaction_card is created
            assert transaction_card.id == transaction
            assert transaction_card.card_terminal_id == input.terminal.id
            assert transaction_card.external_id == (None if brand == "sepay" else "id")


def test_unknown_card_terminal(
    db_session: Session, card_terminals: dict[str, CardTerminal]
) -> None:
    with pytest.raises(NotImplementedError):
        start_card_transaction(
            ProcessFlowCardTerminalData(card_terminals["unknown"]), 1, 100
        )


def test_unknown_card_terminal_update(
    db_session: Session, card_terminals: dict[str, CardTerminal]
) -> None:
    with pytest.raises(NotImplementedError):
        card_terminal_update_service.get_status_of_transaction_and_update(
            db_session,
            Transaction(
                id=1,
                card_data=TransactionCard(
                    card_terminal=card_terminals["unknown"], external_id=None
                ),
                created_at=datetime.datetime(2021, 1, 1, 0, 0, 0),
            ),
        )


def test_get_sepay_status(
    db_session: Session, card_terminals: dict[str, CardTerminal]
) -> None:
    with patch.object(
        card_terminal_update_service,
        "_get_status_of_sepay_transaction",
        Mock(return_value=("detailed_enum_description", TransactionStatus.DELETED)),
    ) as mock:
        with patch.object(
            card_terminal_update_service,
            "update_transaction_based_on_status",
            Mock(return_value=TransactionStatus.DELETED),
        ) as mock2:
            card_data = TransactionCard(
                card_terminal=card_terminals["sepay"], external_id=None
            )
            transaction = Transaction(
                id=1,
                card_data=card_data,
                created_at=datetime.datetime(2021, 1, 1, 0, 0, 0),
            )

            with freezegun.freeze_time("2021-01-01 00:00:30"):
                status = (
                    card_terminal_update_service.get_status_of_transaction_and_update(
                        db_session, transaction
                    )
                )

            assert status == ("detailed_enum_description", TransactionStatus.DELETED)

            mock.assert_called_once_with(1, card_terminals["sepay"].external_id, 30.0)
            mock2.assert_called_once_with(
                db_session, TransactionStatus.DELETED, transaction.id
            )


mapping = {
    TransactionStatus.PENDING: MOLLIE_PAYMENT_PENDING,
    TransactionStatus.COMPLETED: MOLLIE_PAYMENT_PAID,
    TransactionStatus.EXPIRED: MOLLIE_PAYMENT_EXPIRED,
    TransactionStatus.CANCELLED: MOLLIE_PAYMENT_CANCELLED,
}


@pytest.mark.usefixtures("exact_finished_settings")
@pytest.mark.parametrize("type", mapping.keys())
def test_get_mollie_status(
    card_terminals: dict[str, CardTerminal],
    type: TransactionStatus,
    requests_mocker: Mocker,
    db_session: Session,
    products: dict[str, Product],
) -> None:
    requests_mocker.get(
        "https://api.mollie.com/v2/payments/tr_test", json=mapping[type]
    )

    card_data = TransactionCard(
        card_terminal=card_terminals["mollie"], external_id="tr_test"
    )
    transaction = _create_pending_direct_card_purchase(
        db_session, products["bar_beer"], card_data
    )
    db_session.flush()

    (desc, status,) = card_terminal_update_service.get_status_of_transaction_and_update(
        db_session, transaction
    )

    assert status == type

    db_session.refresh(transaction)
    assert transaction.status == type
