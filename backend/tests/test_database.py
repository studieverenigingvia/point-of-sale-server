from pathlib import Path

import pytest
from alembic import command
from alembic.config import Config
from alembic.script import ScriptDirectory
from sqlalchemy import inspect, Table, String, Column
from sqlalchemy.orm import close_all_sessions

from app.models import Transaction
from app.models.database import engine, mapper_registry


@pytest.fixture
def alembic_cfg():
    return Config(str(Path(__file__).parent.parent / "alembic.ini"))


def test_single_head(alembic_cfg):
    script = ScriptDirectory.from_config(alembic_cfg)
    # This will raise if there are multiple heads
    script.get_current_head()


def test_no_flushed_models_visible_from_test(
    db_session, bar_client, products, normal_account
):
    """
    This test checks that items that are flushed, but not committed are not visible.

    Any request with -1 amount should fail during checkout_service._procces_orders,
    however an invalid transaction is created prior to that.

    When the connection between tests and the backend is shared, a Transaction
    with Type Invalid should be visible to this db_session. When the test database
    connection is properly isolated, no transactions should be visible after the
    request.

    Based on: test_checkout.py::test_checkout_room_purchase_direct_single_product
    """
    normal_account.balance = 1000
    db_session.add(normal_account)
    db_session.commit()

    r1 = bar_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "1234",
            "account_id": normal_account.id,
            "products": [{"id": products["bar_beer"].id, "amount": -1}],
        },
    )
    assert r1.status_code == 422, r1.json()
    assert not db_session.query(Transaction).count()


def test_upgrade_all_migrations(db_session, alembic_cfg):
    alembic_version_table = Table(
        "alembic_version", mapper_registry.metadata, Column("version_num", String)
    )

    command.stamp(alembic_cfg, "heads")
    command.downgrade(alembic_cfg, "base")

    assert db_session.execute(alembic_version_table.select()).scalar() is None

    # Make sure session are closed, otherwise .drop_all() might hang.
    close_all_sessions()

    # Drop the alembic_version.
    mapper_registry.metadata.drop_all(engine)

    inspector = inspect(engine)
    tables = inspector.get_table_names()
    assert not tables, "no tables should be left"

    script = ScriptDirectory.from_config(alembic_cfg)
    heads = script.get_revisions("heads")
    assert heads, "alembic should know the latest"
    assert len(heads) == 1, "Single head double check"

    command.upgrade(alembic_cfg, "heads")

    inspector = inspect(engine)
    tables = inspector.get_table_names()
    assert set(mapper_registry.metadata.tables).issubset(set(tables))

    r = db_session.execute(alembic_version_table.select())
    assert r.scalar() == heads[0].revision, "revision not latest"


@pytest.mark.parametrize(
    "mapper,relation",
    [
        (mapper, relation)
        for mapper in mapper_registry.mappers
        for relation in mapper.relationships
    ],
    ids=[
        f"{mapper.class_.__name__}-{relation.key}"
        for mapper in mapper_registry.mappers
        for relation in mapper.relationships
    ],
)
def test_no_backrefs_used(mapper, relation):
    if relation.backref is not None:
        pytest.fail(
            f"Mapper '{mapper.class_.__name__}' with relationship '{relation.key}' "
            f"has a backref. Backref's cannot be detected with typing, "
            f"instead use relationship(back_populates=...) on both models."
        )
