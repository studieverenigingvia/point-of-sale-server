from datetime import datetime, timezone, timedelta

import pytest

from app.dependencies import RequestContext
from app.models import Order, SepaBatch, Transaction
from app.models.order import OrderRegister
from app.models.transaction import TransactionStatus
from app.service.flow import flow_service
from app.service.flow.flow_types import NextProcessFlowRequest


def test_checkout_bar_direct_debit_and_event(
    db_session, bar_client, products, sepa_account
):
    product = products["bar_beer"]
    db_session.refresh(product)
    quantity_before = product.quantity

    assert sepa_account.balance == 0

    r1 = bar_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "123",
            "account_id": sepa_account.id,
            "products": [{"id": product.id, "amount": 1}],
        },
    )

    assert r1.status_code == 200, r1.json()

    db_session.refresh(sepa_account)
    db_session.refresh(product)
    assert quantity_before - 1 == product.quantity
    assert sepa_account.balance != 0

    r2 = bar_client.post(
        f"/api/transactions/{r1.json()['transaction_id']}/undo",
    )
    assert r2.status_code == 200, r2.json()
    db_session.refresh(product)
    assert quantity_before == product.quantity

    orders1 = (
        db_session.query(Order).filter(Order.account_id == product.ledger_id).all()
    )

    # Both product orders should cancel out
    assert sum(o.price for o in orders1) == 0

    orders2 = db_session.query(Order).filter(Order.account_id == sepa_account.id).all()

    # Both account orders should cancel out
    assert sum(o.price for o in orders2) == 0

    orders3 = db_session.query(Order).all()

    assert len(orders3) == 4

    db_session.refresh(sepa_account)

    assert sepa_account.balance == 0


@pytest.mark.usefixtures("sepa_config")
def test_delete_bounced_sepa_batch(db_session, admin_client, sepa_account):
    sepa_account.balance = -100
    db_session.add(sepa_account)
    db_session.commit()

    rv = admin_client.post(
        "/api/admin/sepa_batches/",
        json={
            "accounts": [sepa_account.id],
            "executionDate": (
                datetime.now(timezone.utc) + timedelta(days=-1)
            ).isoformat(),
            "description": "asdf",
        },
    )
    assert rv.status_code == 201, rv.json()

    batch = db_session.query(SepaBatch).one()

    rv = admin_client.post(f"/api/admin/sepa_batches/{batch.id}/execute/")
    assert rv.status_code == 204, rv.json()
    db_session.refresh(sepa_account)
    assert sepa_account.balance == 0

    t = db_session.query(Transaction).one()

    r2 = admin_client.delete(f"/api/admin/transactions/{t.id}/")
    assert r2.status_code == 200, r2.json()
    db_session.refresh(sepa_account)
    assert sepa_account.balance == -100


def test_delete_finished_card_transaction(
    db_session,
    bar_client,
    products,
):
    product = products["bar_beer"]
    db_session.refresh(product)
    quantity_before = product.quantity

    r1 = bar_client.post(
        "/api/checkout/card/",
        json={
            "nonce": "123",
            "products": [{"id": product.id, "amount": 1}],
        },
    )

    db_session.refresh(product)
    assert quantity_before == product.quantity
    assert r1.status_code == 200, r1.json()

    r2 = bar_client.post(
        f"/api/transactions/{r1.json()['transaction_id']}/finish",
    )
    assert r2.status_code == 200, r2.json()

    db_session.refresh(product)
    assert quantity_before - 1 == product.quantity

    amt_of_pending_orders = (
        db_session.query(Order).filter(Order.register == OrderRegister.PENDING).count()
    )

    # 2 sets of orders: 1 for the product, 1 for the account
    assert amt_of_pending_orders == 4

    amt_of_non_pending_orders = (
        db_session.query(Order).filter(Order.register == OrderRegister.DEFAULT).count()
    )

    assert amt_of_non_pending_orders == 2

    r3 = bar_client.post(
        f"/api/transactions/{r1.json()['transaction_id']}/undo",
    )

    assert r3.status_code == 200, r3.json()
    db_session.refresh(product)
    assert quantity_before == product.quantity

    amt_of_pending_orders = (
        db_session.query(Order).filter(Order.register == OrderRegister.PENDING).count()
    )

    # 2 sets of orders: 1 for the product, 1 for the account
    assert amt_of_pending_orders == 4

    amt_of_non_pending_orders = (
        db_session.query(Order).filter(Order.register == OrderRegister.DEFAULT).count()
    )

    assert amt_of_non_pending_orders == 4


def test_undo_pending_transaction(
    db_session,
    bar_client,
    products,
):
    r1 = bar_client.post(
        "/api/checkout/card/",
        json={
            "nonce": "123",
            "products": [{"id": products["bar_beer"].id, "amount": 1}],
        },
    )

    assert r1.status_code == 200, r1.json()

    r2 = bar_client.post(
        f"/api/transactions/{r1.json()['transaction_id']}/undo",
    )

    assert r2.status_code == 400, r2.json()


def test_delete_pending_transaction(
    db_session,
    bar_client,
    admin_client,
    products,
):
    r1 = bar_client.post(
        "/api/checkout/card/",
        json={
            "nonce": "123",
            "products": [{"id": products["bar_beer"].id, "amount": 1}],
        },
    )

    assert r1.status_code == 200, r1.json()
    amt_of_pending_orders = (
        db_session.query(Order).filter(Order.register == OrderRegister.PENDING).count()
    )

    # 1 for the product, 1 for card
    assert amt_of_pending_orders == 2

    amt_of_non_pending_orders = (
        db_session.query(Order).filter(Order.register == OrderRegister.DEFAULT).count()
    )

    assert amt_of_non_pending_orders == 0

    r2 = admin_client.delete(f"/api/admin/transactions/{r1.json()['transaction_id']}/")

    assert r2.status_code == 200, r2.json()
    amt_of_pending_orders = (
        db_session.query(Order).filter(Order.register == OrderRegister.PENDING).count()
    )
    # 2 sets of orders: 1 for the product, 1 for the account
    assert amt_of_pending_orders == 4

    amt_of_non_pending_orders = (
        db_session.query(Order).filter(Order.register == OrderRegister.DEFAULT).count()
    )
    assert amt_of_non_pending_orders == 0


def test_delete_expired_transaction(
    db_session,
    bar_client,
    products,
):
    r1 = bar_client.post(
        "/api/checkout/card/",
        json={
            "nonce": "123",
            "products": [{"id": products["bar_beer"].id, "amount": 1}],
        },
    )

    assert r1.status_code == 200, r1.json()

    context = RequestContext(
        db_session=db_session,
    )
    req = NextProcessFlowRequest(
        transaction_id=r1.json()["transaction_id"],
        desired_state=TransactionStatus.EXPIRED,
    )

    flow_service.process_flow_next(context, req)

    orders = (
        db_session.query(Order).filter(Order.register == OrderRegister.DEFAULT).all()
    )
    assert len(orders) == 0

    r2 = bar_client.post(
        f"/api/transactions/{r1.json()['transaction_id']}/undo",
    )

    assert r2.status_code == 400, r2.json()
    assert r2.json()["error"] == "invalid_transaction_state"


def test_delete_with_upgrade_card(db_session, bar_client, normal_account):
    upgrade_amount = 500

    r1 = bar_client.post(
        "/api/checkout/card/",
        json={
            "nonce": "123",
            "upgrade_account_id": normal_account.id,
            "upgrade_amount": upgrade_amount,
            "products": [],
        },
    )

    assert r1.status_code == 200, r1.json()

    transaction_id = r1.json()["transaction_id"]

    db_session.refresh(normal_account)
    assert normal_account.balance == 0

    r2 = bar_client.post(
        f"/api/transactions/{transaction_id}/finish",
    )
    assert r2.status_code == 200, r2.json()

    db_session.refresh(normal_account)
    assert normal_account.balance == upgrade_amount

    r3 = bar_client.post(
        f"/api/transactions/{transaction_id}/undo",
    )
    assert r3.status_code == 200, r3.json()

    db_session.refresh(normal_account)
    assert normal_account.balance == 0
