from unittest.mock import patch

from app.models import Transaction

from app.service.flow.utils.process_flow import account_repo as upgrade_account_repo
from app.service.flow.flows.account import (
    order_service as account_order_service,
)

from app.service.flow.flow_service import (
    transaction_repo,
    transaction_service,
    audit_service as account_audit_service,
)
from app.views.exceptions import ApplicationException


def raise_exception(*args, **kwargs):
    raise ApplicationException()


@patch.object(upgrade_account_repo, "get_by_id", raise_exception)
def test_checkout_crash_1(db_session, bar_client, products, normal_account):
    """Crashes process_upgrade"""
    normal_account.balance = 1000
    db_session.add(normal_account)
    db_session.commit()

    product = products["bar_beer"]
    t = db_session.query(Transaction).count()

    r2 = bar_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "123",
            "account_id": normal_account.id,
            "products": [{"id": product.id, "amount": 1}],
        },
    )

    assert r2.status_code == 500, r2.json()

    assert r2.json()["status"] == "err"
    assert db_session.query(Transaction).count() == t
    db_session.refresh(normal_account)
    assert normal_account.balance == 1000


@patch.object(account_order_service, "save_multiple", raise_exception)
def test_checkout_crash_2(db_session, bar_client, products, normal_account):
    """Crashes update to database."""
    normal_account.balance = 1000
    db_session.add(normal_account)
    db_session.commit()

    product = products["bar_beer"]
    t = db_session.query(Transaction).count()

    r2 = bar_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "123",
            "account_id": normal_account.id,
            "products": [{"id": product.id, "amount": 1}],
        },
    )

    assert r2.status_code == 500, r2.json()

    assert r2.json()["status"] == "err"
    assert db_session.query(Transaction).count() == t
    db_session.refresh(normal_account)
    assert normal_account.balance == 1000


@patch.object(transaction_service, "save", raise_exception)
def test_checkout_crash_4(db_session, bar_client, products, normal_account):
    normal_account.balance = 1000
    db_session.add(normal_account)
    db_session.commit()

    product = products["bar_beer"]
    t = db_session.query(Transaction).count()

    r2 = bar_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "123",
            "account_id": normal_account.id,
            "products": [{"id": product.id, "amount": 1}],
        },
    )

    assert r2.status_code == 500, r2.json()

    assert r2.json()["status"] == "err"
    assert db_session.query(Transaction).count() == t
    db_session.refresh(normal_account)
    assert normal_account.balance == 1000


@patch.object(account_audit_service, "audit_create", raise_exception)
def test_checkout_crash_5(db_session, bar_client, products, normal_account):
    normal_account.balance = 1000
    db_session.add(normal_account)
    db_session.commit()

    product = products["bar_beer"]
    t = db_session.query(Transaction).count()

    r2 = bar_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "123",
            "account_id": normal_account.id,
            "products": [{"id": product.id, "amount": 1}],
        },
    )

    assert r2.status_code == 500, r2.json()

    assert r2.json()["status"] == "err"
    assert db_session.query(Transaction).count() == t
    db_session.refresh(normal_account)
    assert normal_account.balance == 1000


@patch.object(transaction_repo, "create_empty_transaction", raise_exception)
def test_checkout_crash_6(db_session, bar_client, products, normal_account):
    normal_account.balance = 1000
    db_session.add(normal_account)
    db_session.commit()

    product = products["bar_beer"]
    t = db_session.query(Transaction).count()

    r2 = bar_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "123",
            "account_id": normal_account.id,
            "products": [{"id": product.id, "amount": 1}],
        },
    )

    assert r2.status_code == 500, r2.json()

    assert r2.json()["status"] == "err"
    assert db_session.query(Transaction).count() == t
    db_session.refresh(normal_account)
    assert normal_account.balance == 1000


def test_checkout_failed_balance(db_session, bar_client, products, normal_account):
    normal_account.balance = 1000
    db_session.add(normal_account)
    db_session.commit()

    product = products["bar_beer"]
    t = db_session.query(Transaction).count()

    r2 = bar_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "123",
            "account_id": normal_account.id,
            "products": [{"id": product.id, "amount": -1}],
        },
    )

    assert r2.status_code == 422, r2.json()
    assert db_session.query(Transaction).count() == t
    db_session.refresh(normal_account)

    assert normal_account.balance == 1000
