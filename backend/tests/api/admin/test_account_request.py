import pytest
from sqlalchemy.orm import Session
from starlette.testclient import TestClient

from app.models import User
from app.models.account import AccountRequest, AccountRequestStatus
from app.service.account_service import check_pin


@pytest.mark.usefixtures("account_request")
def test_api_admin_account_requests(admin_client: TestClient) -> None:
    rv = admin_client.get("/api/admin/account_requests/")
    assert rv.status_code == 200, rv.json()


@pytest.mark.parametrize("action", ["approve", "decline"])
def test_api_admin_account_requests_approve(admin_client, account_request, action):
    rv = admin_client.post(
        f"/api/admin/account_requests/{account_request.id}/status",
        json={"status": action},
    )
    d = rv.json()
    assert rv.status_code == 200, d
    assert d["status"] == action + "d"


def test_api_admin_account_requests_approve_decline_declined(
    admin_client: TestClient,
    account_request: AccountRequest,
    db_session: Session,
    users_without_account: dict[str, User],
) -> None:
    user_without_account = users_without_account["user1"]
    assert user_without_account.account is None

    account_request.status = AccountRequestStatus.DECLINED.value
    db_session.commit()
    rv = admin_client.post(
        f"/api/admin/account_requests/{account_request.id}/status",
        json={"status": "approve"},
    )
    assert rv.status_code == 200, rv.json()
    assert rv.json()["status"] == "approved"

    db_session.refresh(user_without_account)
    assert user_without_account.account is not None
    assert check_pin(user_without_account.account, "1234")


def test_api_admin_account_requests_approve_decline_accepted(
    admin_client, account_request, db_session
):
    account_request.status = AccountRequestStatus.APPROVED.value
    db_session.commit()
    rv = admin_client.post(
        f"/api/admin/account_requests/{account_request.id}/status",
        json={"status": "decline"},
    )
    assert rv.status_code == 409, rv.json()


def test_api_admin_account_request_approve_deleted_account(
    admin_client, account_request, normal_account, users, db_session
):
    assert normal_account.id == users["random"].account_id
    account_request.user_id = users["random"].id
    normal_account.deleted = True
    db_session.commit()

    rv = admin_client.post(
        f"/api/admin/account_requests/{account_request.id}/status",
        json={"status": "approve"},
    )
    assert rv.status_code == 200, rv.json()

    db_session.refresh(normal_account)
    assert normal_account.deleted is False
