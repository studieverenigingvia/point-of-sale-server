from typing import Literal
from unittest.mock import patch

from sqlalchemy.orm import Session
from starlette.testclient import TestClient

from app.models import User, Product
from app.models.card_terminal import CardTerminal
from app.models.transaction import TransactionCard, TransactionStatus
from tests.util.transactions import (
    _create_pending_direct_card_purchase,
    _create_direct_card_purchase,
)


def test_api_transactions(
    sepa_user: User,
    room_client: TestClient,
    products: dict[Literal["room_cola", "room_beer", "bar_beer"], Product],
    users: dict[str, User],
    pin: dict[str, str],
    sepa_client: TestClient,
) -> None:
    rv = room_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "123",
            "account_id": sepa_user.account_id,
            "pin": pin["pin"],
            "products": [{"id": products["room_cola"].id, "amount": 1}],
        },
    )
    assert rv.status_code == 200, rv.json()

    rv = sepa_client.get("/api/transactions/")
    assert rv.status_code == 200, rv.json()


def test_transaction_status(
    card_terminals: dict[str, CardTerminal],
    products: dict[Literal["room_cola", "room_beer", "bar_beer"], Product],
    db_session: Session,
    room_client: TestClient,
) -> None:
    card_data = TransactionCard(
        card_terminal_id=card_terminals["mollie"].id,
        external_id="123",
    )

    transaction1 = _create_pending_direct_card_purchase(
        db_session, products["bar_beer"], card_data
    )
    db_session.flush()

    with patch(
        "app.service.card_terminal_update_service._get_status_of_mollie_transaction",
        return_value=("details", TransactionStatus.EXPIRED),
    ):
        rv = room_client.get(f"/api/transactions/card/{transaction1.id}/status/")
        assert rv.status_code == 200, rv.json()
        assert rv.json() == {"status": "expired", "details": "details"}

    db_session.refresh(transaction1)
    assert transaction1.status == TransactionStatus.EXPIRED


def test_non_card_transaction_status(
    db_session: Session,
    products: dict[Literal["room_cola", "room_beer", "bar_beer"], Product],
    room_client: TestClient,
) -> None:
    transaction1 = _create_direct_card_purchase(db_session, [products["bar_beer"]])

    rv = room_client.get(f"/api/transactions/card/{transaction1.id}/status/")
    assert rv.status_code == 400, rv.json()


def test_transaction_status_not_found(
    db_session: Session, room_client: TestClient
) -> None:
    rv = room_client.get("/api/transactions/card/123/status/")
    assert rv.status_code == 404, rv.json()
