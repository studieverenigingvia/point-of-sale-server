from starlette.testclient import TestClient

from app.models import User


def test_account_request_anonymous(anonymous_client: TestClient) -> None:
    rv = anonymous_client.post("/api/account_request/request")
    assert rv.status_code == 403, rv.content
    assert rv.json()["error"] == "Not authenticated"

    rv = anonymous_client.post(
        "/api/account_request/request", headers={"Authorization": "Bearer NOTVALID"}
    )
    assert rv.status_code == 401, rv.content
    assert rv.json()["error"] == "invalid_access_token"


def test_account_request(
    room_client: TestClient, sale_points: tuple[User, User]
) -> None:
    room, bar = sale_points
    assert room.account_id is None

    rv = room_client.post("/api/account_request/request", json={"pin": "1234"})
    assert rv.status_code == 200, rv.json()

    rv = room_client.get("/api/account_request/pending")
    assert rv.status_code == 200, rv.json()

    rv = room_client.post("/api/account_request/request", json={"pin": "1234"})
    assert rv.status_code == 400, rv.json()
    assert rv.json()["error"] == "account_request_pending"
