from app.service.account_service import check_pin


def test_self_account(self_client):
    rv = self_client.get("/api/account/")
    assert rv.status_code == 200, rv.json()


def test_account_upgrade(self_client, requests_mocker):
    rv = self_client.post("/api/account/upgrade", json={})
    assert rv.status_code == 422, rv.json()
    assert rv.json()["detail"] == [
        {
            "loc": ["body", "amount"],
            "msg": "field required",
            "type": "value_error.missing",
        }
    ]

    rv = self_client.post("/api/account/upgrade", json={"amount": -1})
    assert rv.status_code == 422, rv.json()
    assert rv.json()["detail"] == [
        {
            "ctx": {"limit_value": 2000},
            "loc": ["body", "amount"],
            "msg": "ensure this value is greater than or equal to 2000",
            "type": "value_error.number.not_ge",
        }
    ]

    rv = self_client.post("/api/account/upgrade", json={"amount": 10001})
    assert rv.status_code == 422, rv.json()
    assert rv.json()["detail"] == [
        {
            "ctx": {"limit_value": 10000},
            "loc": ["body", "amount"],
            "msg": "ensure this value is less than or equal to 10000",
            "type": "value_error.number.not_le",
        }
    ]

    mollie_get_create_payment_response = {
        "resource": "payment",
        "id": "tr_7UhSN1zuXS",
        "mode": "test",
        "createdAt": "2018-03-20T09:13:37+00:00",
        "amount": {"value": "25.00", "currency": "EUR"},
        "description": "Order #12345",
        "method": None,
        "metadata": {"order_id": "12345"},
        "status": "open",
        "isCancelable": False,
        "expiresAt": "2018-03-20T09:28:37+00:00",
        "details": None,
        "profileId": "pfl_QkEhN94Ba",
        "sequenceType": "oneoff",
        "redirectUrl": "https://webshop.example.org/order/12345/",
        "webhookUrl": "https://webshop.example.org/payments/webhook/",
        "_links": {
            "self": {
                "href": "https://api.mollie.com/v2/payments/tr_C2Nwq9jZuA",
                "type": "application/hal+json",
            },
            "checkout": {
                "href": "https://www.mollie.com/checkout/select-issuer/ideal/CNwq9jZuA",
                "type": "text/html",
            },
        },
    }
    requests_mocker.post(
        "https://api.mollie.com/v2/payments", json=mollie_get_create_payment_response
    )
    requests_mocker.get(
        "https://api.mollie.com/v2/payments/tr_7UhSN1zuXS",
        json=mollie_get_create_payment_response,
    )

    rv = self_client.post("/api/account/upgrade", json={"amount": 2000})
    assert rv.status_code == 201, rv.json()


def test_api_account_transactions_count(self_client):
    rv = self_client.get("/api/account/transactions/count")
    assert rv.status_code == 200, rv.json()


def test_api_account_pin_set(normal_client, normal_account, db_session):
    # The normal_client fixtures uses normal_account internally
    assert check_pin(normal_account, "1234")

    rv = normal_client.post("/api/account/pin", json={"pin": "2000"})
    assert rv.status_code == 204, rv.json()

    db_session.refresh(normal_account)
    assert check_pin(normal_account, "2000")

    rv = normal_client.post("/api/account/pin", json={"pin": 2001})
    assert rv.status_code == 204, rv.json()

    db_session.refresh(normal_account)
    assert check_pin(normal_account, "2001")

    rv = normal_client.post("/api/account/pin", json={"pin": "word"})
    assert rv.status_code == 400, rv.json()
    assert rv.json()["error"] == "not_a_pin", rv.json()

    # Should still be the same as before
    db_session.refresh(normal_account)
    assert check_pin(normal_account, "2001")


# @pytest.mark.usefixtures("users")
def test_api_accounts(bar_client, normal_account, sepa_account):
    assert check_pin(normal_account, "1234")

    rv = bar_client.get("/api/accounts/")
    assert rv.status_code == 200, rv.json()

    accounts = rv.json()["accounts"]
    assert len(accounts) == 2

    for row in accounts:
        if row["id"] == normal_account.id:
            assert not row[
                "directDebit"
            ], "Direct debit should be false for normal account"
        elif row["id"] == sepa_account.id:
            assert row["directDebit"], "Direct debit should be true for sepa account"
