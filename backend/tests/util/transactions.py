from datetime import date
from random import random

from sqlalchemy.orm import Session

from app.dependencies import UserRequestContext
from app.models import Transaction, Order, Account, Product
from app.models.checkout import CheckoutMethod
from app.models.order import OrderRegister, OrderFlowType
from app.models.transaction import (
    TransactionStatus,
    TransactionFlowType,
    TransactionCard,
)
from app.service import order_service
from app.service.account_service import get_account_for_checkout_method


def _get_flow_for_checkout_method(method: CheckoutMethod) -> TransactionFlowType:
    if method == CheckoutMethod.CARD:
        return TransactionFlowType.CARD_PAYMENT
    elif method == CheckoutMethod.CASH:
        return TransactionFlowType.CASH_PAYMENT
    elif method == CheckoutMethod.ACCOUNT:
        return TransactionFlowType.ACCOUNT_PAYMENT
    elif method == CheckoutMethod.OTHER:
        return TransactionFlowType.OTHER_PAYMENT
    elif method == CheckoutMethod.IDEAL:
        return TransactionFlowType.IDEAL_PAYMENT
    elif method == CheckoutMethod.SEPA:
        return TransactionFlowType.SEPA_PAYMENT
    elif method == CheckoutMethod.TRANSFER:
        return TransactionFlowType.TRANSFER_PAYMENT
    else:
        raise ValueError(f"Unknown checkout method {method}")


def _create_top_up_card(
    db_session: Session,
    account: Account,
    amount: int = 1000,
    reason: str | None = None,
) -> Transaction:
    account.balance += amount
    transaction_top_up = Transaction(
        created_at=date(2020, 9, 1),
        updated_at=date(2020, 9, 1),
        reason=reason,
        nonce=f"nonce-top-up-{account.id}-{random()}",
        status=TransactionStatus.COMPLETED,
        flow=_get_flow_for_checkout_method(CheckoutMethod.CARD),
    )

    db_session.add_all([account, transaction_top_up])
    db_session.flush()

    transaction_top_up.orders = [
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method="account",
            account_id=account.id,
            price=-amount,
            register=OrderRegister.PENDING,
            flow_type=OrderFlowType.NONE_TO_PENDING,
        ),
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method="account",
            account_id=account.id,
            price=amount,
            register=OrderRegister.PENDING,
            flow_type=OrderFlowType.PENDING_TO_COMPLETED,
        ),
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method="account",
            account_id=account.id,
            price=-amount,
            register=OrderRegister.DEFAULT,
            flow_type=OrderFlowType.PENDING_TO_COMPLETED,
        ),
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method=CheckoutMethod.CARD.value,
            account_id=get_account_for_checkout_method(db_session, CheckoutMethod.CARD),
            price=amount,
            register=OrderRegister.PENDING,
            flow_type=OrderFlowType.NONE_TO_PENDING,
        ),
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method=CheckoutMethod.CARD.value,
            account_id=get_account_for_checkout_method(db_session, CheckoutMethod.CARD),
            price=-amount,
            register=OrderRegister.PENDING,
            flow_type=OrderFlowType.PENDING_TO_COMPLETED,
        ),
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method=CheckoutMethod.CARD.value,
            account_id=get_account_for_checkout_method(db_session, CheckoutMethod.CARD),
            price=amount,
            register=OrderRegister.DEFAULT,
            flow_type=OrderFlowType.PENDING_TO_COMPLETED,
        ),
    ]

    db_session.commit()
    return transaction_top_up


def _create_top_up(
    db_session: Session,
    account: Account,
    amount: int = 1000,
    method: CheckoutMethod = CheckoutMethod.CASH,
    reason: str | None = None,
) -> Transaction:
    assert method != CheckoutMethod.CARD

    account.balance += amount
    transaction_top_up = Transaction(
        created_at=date(2020, 9, 1),
        updated_at=date(2020, 9, 1),
        reason=reason,
        nonce=f"nonce-top-up-{account.id}-{random()}",
        status=TransactionStatus.COMPLETED,
        flow=_get_flow_for_checkout_method(method),
    )

    db_session.add_all([account, transaction_top_up])
    db_session.flush()

    transaction_top_up.orders = [
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method="account",
            account_id=account.id,
            price=-amount,
            register=OrderRegister.DEFAULT,
            flow_type=OrderFlowType.NONE_TO_COMPLETED,
        ),
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method=method.value,
            account_id=get_account_for_checkout_method(db_session, method),
            price=amount,
            register=OrderRegister.DEFAULT,
            flow_type=OrderFlowType.NONE_TO_COMPLETED,
        ),
    ]

    db_session.commit()
    return transaction_top_up


def _create_account_purchase(
    db_session: Session, account: Account, products: list[Product]
) -> Transaction:
    total_price = 0
    for p in products:
        total_price += p.price

    account.balance -= total_price
    transaction_buy = Transaction(
        created_at=date(2020, 9, 1),
        updated_at=date(2020, 9, 1),
        nonce=f"nonce-account-purchase-{account.id}-{random()}",
        status=TransactionStatus.COMPLETED,
        flow=_get_flow_for_checkout_method(CheckoutMethod.ACCOUNT),
    )

    db_session.add_all([account, transaction_buy])
    db_session.flush()

    transaction_buy.orders = [
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method="account",
            account_id=account.id,
            price=total_price,
            register=OrderRegister.DEFAULT,
            flow_type=OrderFlowType.NONE_TO_COMPLETED,
        ),
    ]

    for p in products:
        transaction_buy.orders.append(
            Order(
                created_at=date(2020, 9, 1),
                updated_at=date(2020, 9, 1),
                product_id=p.id,
                _method="account",
                account_id=p.ledger_id,
                price=-p.price,
                register=OrderRegister.DEFAULT,
                flow_type=OrderFlowType.NONE_TO_COMPLETED,
            )
        )

    db_session.commit()
    return transaction_buy


def _create_pending_direct_card_purchase(
    db_session: Session,
    product: Product,
    card_data: TransactionCard = None,
) -> Transaction:
    total_price = product.price

    transaction_buy = Transaction(
        created_at=date(2020, 9, 1),
        updated_at=date(2020, 9, 1),
        nonce=f"nonce-direct-purchase-{random()}",
        flow=_get_flow_for_checkout_method(CheckoutMethod.CARD),
        status=TransactionStatus.PENDING,
        card_data=card_data,
    )

    db_session.add_all([transaction_buy])
    db_session.flush()

    transaction_buy.orders = [
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method="card",
            account_id=get_account_for_checkout_method(db_session, CheckoutMethod.CARD),
            price=total_price,
            register=OrderRegister.PENDING,
            flow_type=OrderFlowType.NONE_TO_PENDING,
        ),
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            product_id=product.id,
            _method="card",
            account_id=product.ledger_id,
            price=-product.price,
            register=OrderRegister.PENDING,
            flow_type=OrderFlowType.NONE_TO_PENDING,
        ),
    ]

    db_session.commit()
    return transaction_buy


def _create_direct_card_purchase(
    db_session: Session,
    products: list[Product],
) -> Transaction:
    total_price = 0
    for p in products:
        total_price += p.price

    transaction_buy = Transaction(
        created_at=date(2020, 9, 1),
        updated_at=date(2020, 9, 1),
        nonce=f"nonce-direct-purchase-{random()}",
        flow=_get_flow_for_checkout_method(CheckoutMethod.CARD),
        status=TransactionStatus.COMPLETED,
    )

    db_session.add_all([transaction_buy])
    db_session.flush()

    transaction_buy.orders = [
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method="card",
            account_id=get_account_for_checkout_method(db_session, CheckoutMethod.CARD),
            price=total_price,
            register=OrderRegister.PENDING,
            flow_type=OrderFlowType.NONE_TO_PENDING,
        ),
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method="card",
            account_id=get_account_for_checkout_method(db_session, CheckoutMethod.CARD),
            price=-total_price,
            register=OrderRegister.PENDING,
            flow_type=OrderFlowType.PENDING_TO_COMPLETED,
        ),
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method="card",
            account_id=get_account_for_checkout_method(db_session, CheckoutMethod.CARD),
            price=total_price,
            register=OrderRegister.DEFAULT,
            flow_type=OrderFlowType.PENDING_TO_COMPLETED,
        ),
    ]

    for p in products:
        transaction_buy.orders.append(
            Order(
                created_at=date(2020, 9, 1),
                updated_at=date(2020, 9, 1),
                product_id=p.id,
                _method="card",
                account_id=p.ledger_id,
                price=-p.price,
                register=OrderRegister.PENDING,
                flow_type=OrderFlowType.NONE_TO_PENDING,
            )
        )
        transaction_buy.orders.append(
            Order(
                created_at=date(2020, 9, 1),
                updated_at=date(2020, 9, 1),
                product_id=p.id,
                _method="card",
                account_id=p.ledger_id,
                price=+p.price,
                register=OrderRegister.PENDING,
                flow_type=OrderFlowType.PENDING_TO_COMPLETED,
            )
        )
        transaction_buy.orders.append(
            Order(
                created_at=date(2020, 9, 1),
                updated_at=date(2020, 9, 1),
                product_id=p.id,
                _method="card",
                account_id=p.ledger_id,
                price=-p.price,
                register=OrderRegister.DEFAULT,
                flow_type=OrderFlowType.PENDING_TO_COMPLETED,
            )
        )

    db_session.commit()
    return transaction_buy


def _create_direct_purchase(
    db_session: Session,
    products: list[Product],
    method: CheckoutMethod = CheckoutMethod.CASH,
    reason: str = "",
) -> Transaction:
    assert method != CheckoutMethod.CARD

    total_price = 0
    for p in products:
        total_price += p.price

    transaction_buy = Transaction(
        created_at=date(2020, 9, 1),
        updated_at=date(2020, 9, 1),
        reason=reason,
        nonce=f"nonce-direct-purchase-{method.value.lower()}-{random()}",
        flow=_get_flow_for_checkout_method(method),
        status=TransactionStatus.COMPLETED,
    )

    db_session.add_all([transaction_buy])
    db_session.flush()

    transaction_buy.orders = [
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method=method.value.lower(),
            account_id=get_account_for_checkout_method(db_session, method),
            price=total_price,
            register=OrderRegister.DEFAULT,
            flow_type=OrderFlowType.NONE_TO_COMPLETED,
        ),
    ]

    for p in products:
        transaction_buy.orders.append(
            Order(
                created_at=date(2020, 9, 1),
                updated_at=date(2020, 9, 1),
                product_id=p.id,
                _method="account",
                account_id=p.ledger_id,
                price=-p.price,
                register=OrderRegister.DEFAULT,
                flow_type=OrderFlowType.NONE_TO_COMPLETED,
            )
        )

    db_session.commit()
    return transaction_buy


def _delete_transaction(db_session: Session, transaction: Transaction) -> Transaction:
    context = UserRequestContext(db_session, None)  # type: ignore[arg-type]
    transaction.deleted = True
    transaction.status = TransactionStatus.DELETED
    order_service.delete_orders(
        context, OrderFlowType.COMPLETED_TO_DELETED, transaction.orders, do_commit=False
    )
    db_session.add(transaction)
    db_session.commit()
    return transaction


def _create_account_purchase_with_top_up(
    db_session: Session,
    account: Account,
    products: list[Product],
    amount: int,
) -> Transaction:
    product_price = sum(p.price for p in products)
    assert product_price <= amount, "top up not more than products prices"

    topup_amount = amount - product_price
    account.balance += topup_amount

    transaction = Transaction(
        created_at=date(2020, 9, 1),
        updated_at=date(2020, 9, 1),
        nonce=f"nonce-purchase-with-top-up-cash-{random()}",
        status=TransactionStatus.COMPLETED,
        flow=TransactionFlowType.CASH_PAYMENT,
    )

    db_session.add_all([transaction])
    db_session.flush()

    transaction.orders = [
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method="account",
            account_id=account.id,
            price=-topup_amount,
            register=OrderRegister.DEFAULT,
            flow_type=OrderFlowType.NONE_TO_COMPLETED,
        ),
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method="cash",
            account_id=get_account_for_checkout_method(db_session, CheckoutMethod.CASH),
            price=amount,
            register=OrderRegister.DEFAULT,
            flow_type=OrderFlowType.NONE_TO_COMPLETED,
        ),
    ]

    for product in products:
        transaction.orders.append(
            Order(
                created_at=date(2020, 9, 1),
                updated_at=date(2020, 9, 1),
                product_id=product.id,
                _method="cash",
                account_id=product.ledger_id,
                price=-product.price,
                register=OrderRegister.DEFAULT,
                flow_type=OrderFlowType.NONE_TO_COMPLETED,
            ),
        )

    db_session.commit()
    return transaction
