import pytest
from sqlalchemy.orm import Session

from app.models.card_terminal import CardTerminal


@pytest.fixture()
def card_terminals(db_session: Session) -> dict[str, CardTerminal]:
    mollie = CardTerminal(
        id=1,
        brand="mollie",
        external_id="1",
        friendly_name="Mollie",
    )
    sepay = CardTerminal(
        id=2,
        brand="sepay",
        external_id="2",
        friendly_name="SEPAY",
    )

    unknown = CardTerminal(
        id=3,
        brand="unknown",
        external_id="3",
        friendly_name="Unknown",
    )

    db_session.add_all([mollie, sepay, unknown])
    db_session.commit()

    return {
        "mollie": mollie,
        "sepay": sepay,
        "unknown": unknown,
    }
