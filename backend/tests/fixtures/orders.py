from datetime import date

import pytest
from sqlalchemy.orm import Session

from app.models import Transaction, Order, Account, Product
from app.models.order import OrderFlowType, OrderRegister
from app.models.transaction import TransactionStatus, TransactionFlowType


@pytest.fixture
def transaction_account_order(
    db_session: Session, products: dict[str, Product], sepa_account: Account
) -> Transaction:
    assert sepa_account.balance == 0
    sepa_account.balance = -120
    transaction_buy = Transaction(
        created_at=date(2020, 9, 1),
        updated_at=date(2020, 9, 1),
        nonce="buy-nonce",
        status=TransactionStatus.COMPLETED,
        flow=TransactionFlowType.ACCOUNT_PAYMENT,
    )

    db_session.add_all([transaction_buy])
    db_session.flush()

    transaction_buy.orders = [
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            product_id=products["bar_beer"].id,
            _method="account",
            account_id=products["bar_beer"].ledger_id,
            price=-120,
            flow_type=OrderFlowType.NONE_TO_COMPLETED,
            register=OrderRegister.DEFAULT,
        ),
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method="account",
            account_id=sepa_account.id,
            price=120,
            flow_type=OrderFlowType.NONE_TO_COMPLETED,
            register=OrderRegister.DEFAULT,
        ),
    ]
    db_session.commit()
    return transaction_buy
