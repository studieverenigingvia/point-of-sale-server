from datetime import datetime, timedelta
from typing import Literal, Any

import pytest
from sqlalchemy.orm import Session

from app.models import User, Register, UserDirectDebit, Account


@pytest.fixture(scope="session")
def password() -> dict[Literal["hash", "password"], str]:
    """Single password hash for the session to decrease testing time."""
    password = "password"
    u = User()
    u.set_password(password)
    assert u.password
    return {"hash": u.password, "password": password}


@pytest.fixture()
def sale_points(
    db_session: Session,
    registers: dict[Literal["room", "bar", "self", "admin", "admin_self"], Register],
    password: dict[Literal["hash", "password"], str],
) -> tuple[User, User]:
    room = User(username="room")
    room.password = password["hash"]
    room.long_login = True
    room.register_id = registers["room"].id

    bar = User(username="bar")
    bar.password = password["hash"]
    bar.long_login = True
    bar.register_id = registers["bar"].id

    db_session.add(room)
    db_session.add(bar)
    db_session.commit()
    return room, bar


@pytest.fixture()
def sepa_user(
    db_session: Session, registers: dict[str, Register], password: dict[str, str]
) -> User:
    sepa_user = User(username="user3@gmail.com", name="User 3")
    sepa_user.password = password["hash"]
    sepa_user.register_id = registers["self"].id
    db_session.add(sepa_user)
    db_session.flush()

    created_at = updated_at = datetime(year=2018, month=1, day=1)
    yesterday = datetime.now() - timedelta(days=1)

    direct_debit = UserDirectDebit(
        created_at=created_at,
        updated_at=updated_at,
        user_id=sepa_user.id,
        enabled=True,
        iban="NL91ABNA0417164300",
        bic="ABNANL2A",
        mandate_id="123",
        sign_date=yesterday,
    )

    db_session.add(direct_debit)
    db_session.commit()

    return sepa_user


@pytest.fixture()
def users(
    db_session: Session,
    registers: dict[Literal["room", "bar", "self", "admin", "admin_self"], Register],
    normal_account: Account,
    password: dict[str, str],
) -> dict[str, User]:
    random_user = User(username="user1@gmail.com", name="User 1")
    random_user.password = password["hash"]
    # Uses default register.
    random_user.register_id = None
    random_user.account_id = normal_account.id

    random_user2 = User(username="user2@gmail.com", name="User 2")
    random_user2.password = password["hash"]
    random_user2.register_id = registers["self"].id
    random_user2.account_id = None

    db_session.add_all([random_user, random_user2])

    db_session.commit()
    return {"random": random_user, "random2": random_user2}


@pytest.fixture
def users_without_account(
    db_session: Session, password: dict[str, str], registers: dict[str, Register]
) -> dict[str, User]:
    u = User(username="acc_req@example.com", name="User Account request")
    u.password = password["hash"]
    u.register_id = registers["self"].id

    u2 = User(username="acc_req2@example.com", name="User Account request 2")
    u2.password = password["hash"]
    u2.register_id = registers["self"].id

    db_session.add_all((u, u2))
    db_session.commit()

    return {
        "user1": u,
        "user2": u2,
    }


@pytest.fixture(params=[(), ("admin",)])
def viaduct_user(db_session: Session, request: Any) -> User:
    user = User(username="kelder")
    user.is_admin = "admin" in request.param
    user.viaduct_user_id = 69  # Maico
    db_session.add(user)
    db_session.commit()
    return user
