from typing import Literal

import pytest
from sqlalchemy.orm import Session

from app.models import Register, RegisterCap, Category


@pytest.fixture()
def registers(
    db_session: Session, categories: tuple[Category, Category, Category]
) -> dict[Literal["room", "bar", "self", "admin", "admin_self"], Register]:
    category_room_cans, category_room_alcohol, category_bar = categories

    register_room = Register(
        name="Room Sale",
        capabilities=RegisterCap.PAYMENT_CASH
        | RegisterCap.PAYMENT_CARD
        | RegisterCap.PAYMENT_OTHER
        | RegisterCap.PAYMENT_ACCOUNT_AUTHED,
        categories=[category_room_cans, category_room_alcohol],
    )

    register_bar = Register(
        name="Bar Sale",
        capabilities=RegisterCap.PAYMENT_CASH
        | RegisterCap.PAYMENT_CARD
        | RegisterCap.PAYMENT_ACCOUNT_OTHER
        | RegisterCap.PAYMENT_UNDO,
        categories=[category_bar],
    )

    register_self = Register(
        name="Self service",
        capabilities=RegisterCap.PAYMENT_ACCOUNT_SELF,
        categories=[category_room_alcohol],
        default=True,
    )

    register_admin_self = Register(
        name="Admin (Self)",
        capabilities=RegisterCap.PAYMENT_ACCOUNT_SELF | RegisterCap.PAYMENT_UNDO,
        categories=list(categories),
    )

    register_admin = Register(
        name="Admin",
        capabilities=RegisterCap.PAYMENT_CASH
        | RegisterCap.PAYMENT_CARD
        | RegisterCap.PAYMENT_ACCOUNT_OTHER
        | RegisterCap.PAYMENT_OTHER
        | RegisterCap.PAYMENT_UNDO,
        categories=list(categories),
    )

    db_session.add(register_room)
    db_session.add(register_bar)
    db_session.add(register_self)
    db_session.add(register_admin)
    db_session.add(register_admin_self)
    db_session.flush()
    return {
        "room": register_room,
        "bar": register_bar,
        "self": register_self,
        "admin": register_admin,
        "admin_self": register_admin_self,
    }
