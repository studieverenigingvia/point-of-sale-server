import re
from datetime import datetime

import pytest
import requests_mock
from starlette.testclient import TestClient

from app import fastapi_app
from app.models import LoginToken, UserCondition
from app.service.user_condition_service import CURRENT_CONDITION_VERSION


@pytest.fixture
def room_token(db_session, sale_points):
    room_token = LoginToken(
        user=sale_points[0], token="SOMEACCESSTOKEN_ROOM", created_at=datetime.now()
    )
    db_session.add(room_token)
    db_session.commit()
    return room_token


@pytest.fixture
def bar_token(db_session, sale_points):
    bar_token = LoginToken(
        user=sale_points[1], token="SOMEACCESSTOKEN_BAR", created_at=datetime.now()
    )
    db_session.add(bar_token)
    db_session.commit()
    return bar_token


@pytest.fixture
def normal_token(db_session, users):
    self_token = LoginToken(
        user=users["random"],
        token="SOMEACCESSTOKEN_NORMAL_USR",
        created_at=datetime.now(),
    )

    db_session.add_all(
        [
            self_token,
            UserCondition(
                user_id=users["random"].id,
                accepted_cond_version=CURRENT_CONDITION_VERSION,
            ),
        ]
    )
    db_session.commit()
    return self_token


@pytest.fixture
def sepa_token(db_session, sepa_account):
    self_token = LoginToken(
        user=sepa_account.user,
        token="SOMEACCESSTOKEN_SEPA_USR",
        created_at=datetime.now(),
    )
    db_session.add_all(
        [
            self_token,
            UserCondition(
                user_id=sepa_account.user.id,
                accepted_cond_version=CURRENT_CONDITION_VERSION,
            ),
        ]
    )
    db_session.commit()
    return self_token


def get_test_client(requests_mocker):
    client = TestClient(fastapi_app)
    prefix = re.compile(rf"{re.escape(client.base_url.raw_path)}(/.*)?")
    requests_mocker.register_uri(requests_mock.ANY, prefix, real_http=True)
    return client


@pytest.fixture
def anonymous_client(requests_mocker):
    return get_test_client(requests_mocker)


@pytest.fixture
def admin_client(requests_mocker, admin_token, account_ids_checkout_method):
    test_client = get_test_client(requests_mocker)
    test_client.headers.update({"Authorization": f"Bearer {admin_token.token}"})
    return test_client


@pytest.fixture
def room_client(requests_mocker, room_token, account_ids_checkout_method):
    test_client = get_test_client(requests_mocker)
    test_client.headers.update({"Authorization": f"Bearer {room_token.token}"})
    return test_client


@pytest.fixture
def bar_client(requests_mocker, bar_token, account_ids_checkout_method):
    test_client = get_test_client(requests_mocker)
    test_client.headers.update({"Authorization": f"Bearer {bar_token.token}"})
    return test_client


@pytest.fixture
def normal_client(requests_mocker, normal_token, account_ids_checkout_method):
    test_client = get_test_client(requests_mocker)
    test_client.headers.update({"Authorization": f"Bearer {normal_token.token}"})
    return test_client


@pytest.fixture
def sepa_client(requests_mocker, sepa_token, account_ids_checkout_method):
    test_client = get_test_client(requests_mocker)
    test_client.headers.update({"Authorization": f"Bearer {sepa_token.token}"})
    return test_client


@pytest.fixture(params=["normal", "sepa"])
def self_client(request, sepa_client, normal_client):
    return sepa_client if request.param == "sepa" else normal_client
