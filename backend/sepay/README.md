# Sepay

The SEPAY api is based on a SOAP API (2010 called, and they want their technology back)
The API specification is defined in their asmx file: https://wecr.sepay.nl/v2/wecr.asmx?WSDL
You can look at this file to figure out what calls/arguments exist.
There is also a PDF provided by SEPAY about their docs, see ict@svia.nl email

## API Definition
To update the API definition, run the following: `wget -O sepay_api.wsdl "https://wecr.sepay.nl/v2/wecr.asmx?WSDL"`

## Certificates

For the SEPAY integration to work, you need two certificaties, both stored in the bestuur bitwarden.

```
sepay.pem: the public certificate of SEPAY
viapos.pem: the private key of via
```

## Nudge bytes

The card terminal from SEPAY can automatically fetch the transaction from the server.
For this to work, you need to send a nudge byte to the terminal on port 1234.

The 2 **via** servers (via-true and via-sne01) have a firewall rule that allows the nudge byte to be sent to the terminal from outside of the UvA.

The current terminal ip (seems to be static) is:
``146.50.97.221``

In order to send the nudge byte on development, you can use the following command:
```
ssh -L 1234:146.50.97.221:1234 <username>@svia.nl
```

This will forward requests to 127.0.0.1:1234 to the terminal!


## Terminal ID's

The 2 terminal ID's of SEPAY are:
Blauwe pin: 8357552
Grijze pin: 8169349
