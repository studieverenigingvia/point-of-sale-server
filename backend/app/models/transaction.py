import enum
from datetime import datetime
from typing import Any

from pydantic import BaseModel
from sqlalchemy import Index, Column, String, Integer, ForeignKey
from sqlalchemy.orm import relationship, Mapped

from ._base import Base
from .database import Model
from .order import Order, OrderData
from .card_terminal import CardTerminal
from sqlalchemy import Enum as SQLEnum


class TransactionFlowType(enum.Enum):
    CARD_PAYMENT = "card_payment"
    CASH_PAYMENT = "cash_payment"
    SEPA_PAYMENT = "sepa_payment"
    IDEAL_PAYMENT = "ideal_payment"
    ACCOUNT_PAYMENT = "account_payment"
    TRANSFER_PAYMENT = "transfer_payment"
    OTHER_PAYMENT = "other_payment"
    # It should not be possible to upgrade with Other, only when an admin does it
    OTHER_ADMIN_PAYMENT = "other_admin_payment"
    # When using the topup ui, we do not need pending transactions
    CARD_ADMIN_PAYMENT = "card_admin_payment"


TransactionFlowTypeSQL = SQLEnum(
    TransactionFlowType,
    values_callable=lambda x: [e.value for e in x],
    name="transaction_flow_types",
)


class TransactionStatus(enum.Enum):
    INITIALIZING = "initializing"
    PENDING = "pending"
    COMPLETED = "completed"
    EXPIRED = "expired"
    DELETED = "deleted"
    CANCELLED = "cancelled"


TransactionStatusSQL = SQLEnum(
    TransactionStatus,
    values_callable=lambda x: [e.value for e in x],
    name="transaction_status",
)


class TransactionData(BaseModel):
    id: int
    createdAt: datetime
    deletedAt: datetime | None
    orders: list[OrderData]
    status: TransactionStatus
    deleted: bool
    flow: TransactionFlowType
    reason: str | None


class TransactionCard(Model):
    __tablename__ = "transaction_card"

    id: Mapped[int] = Column(  # type: ignore[assignment]
        Integer, ForeignKey("transaction.id"), primary_key=True
    )

    card_terminal_id: Mapped[int] = Column(
        # type: ignore[assignment]
        Integer,
        ForeignKey("card_terminal.id"),
        nullable=False,
    )

    card_terminal: Mapped[CardTerminal] = relationship(
        # type: ignore[assignment]
        "CardTerminal",
    )

    external_id: Mapped[str | None] = Column(
        String, nullable=True
    )  # type: ignore[assignment]


class Transaction(Model, Base):
    __tablename__ = "transaction"

    nonce: Mapped[str] = Column(
        String, unique=True, nullable=False
    )  # type: ignore[assignment]
    reason: Mapped[str | None] = Column(
        String(60), nullable=True
    )  # type: ignore[assignment]

    orders: Mapped[list[Order]] = relationship(
        # type: ignore[assignment]
        "Order",
        uselist=True,
        back_populates="transaction",
        order_by=Order.price.asc(),
    )

    status: Mapped[TransactionStatus] = Column(  # type: ignore[assignment]
        TransactionStatusSQL,
        nullable=False,
    )

    flow: Mapped[TransactionFlowType] = Column(  # type: ignore[assignment]
        TransactionFlowTypeSQL,
        nullable=False,
    )

    card_data: Mapped[TransactionCard] = relationship(  # type: ignore[assignment]
        "TransactionCard",
        uselist=False,
    )

    __table_args__ = (Index("transaction_created_at_idx", "created_at"),)

    def to_pydantic(self) -> TransactionData:
        return TransactionData(
            id=self.id,
            createdAt=self.created_at.astimezone(),
            deletedAt=self.deleted_at.astimezone() if self.deleted_at else None,
            orders=[order.to_pydantic() for order in self.orders],
            deleted=self.deleted_at is not None,
            status=self.status,
            flow=self.flow,
            reason=self.reason,
        )

    def serialize(self) -> dict[str, Any]:
        return {
            "id": self.id,
            "createdAt": self.created_at.astimezone().isoformat(),
            "deletedAt": self.deleted_at.astimezone().isoformat()
            if self.deleted_at
            else None,
            "status": self.status.value,
            "orders": [order.serialize() for order in self.orders],
            "deleted": self.deleted_at is not None,
            "flow": self.flow.value,
            "reason": self.reason,
        }
