from enum import Enum
from sqlalchemy import Enum as SQLEnum


class CheckoutMethod(Enum):
    CASH = "cash"
    CARD = "card"
    OTHER = "other"
    ACCOUNT = "account"
    IDEAL = "ideal"
    SEPA = "sepa"
    TRANSFER = "transfer"


CheckoutMethodSQL = SQLEnum(
    "cash",
    "card",
    "account",
    "transfer",
    "ideal",
    "sepa",
    "other",
    name="payment_methods",
)
