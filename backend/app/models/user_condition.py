from typing import Any

from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import Mapped

from ._base import Base
from .database import Model


class UserCondition(Model, Base):
    __tablename__ = "user_condition"

    user_id: Mapped[int] = Column(Integer, ForeignKey("user.id"), nullable=False)  # type: ignore[assignment]  # noqa: E501
    accepted_cond_version: Mapped[int] = Column(Integer, nullable=False)  # type: ignore[assignment]  # noqa: E501

    def serialize(self) -> dict[str, Any]:
        return {
            "id": self.id,
            "userId": self.user_id,
            "acceptedConditionVersion": self.accepted_cond_version,
        }

    def deserialize(self, obj: dict[str, Any]) -> None:
        self.id = obj.get("id", self.id)
        self.user_id = obj.get("userId", self.user_id)
        self.accepted_cond_version = obj.get(
            "acceptedConditionVersion", self.accepted_cond_version
        )
