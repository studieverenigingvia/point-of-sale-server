import pydantic
from sqlalchemy import Column, String, Integer
from sqlalchemy.orm import Mapped

from ._base import Base
from .database import Model


class CardTerminalData(pydantic.BaseModel):
    id: int
    friendly_name: str
    brand: str
    externalId: str


class CardTerminal(Model, Base):
    __tablename__ = "card_terminal"

    id: Mapped[int] = Column(Integer, primary_key=True)  # type: ignore[assignment]

    friendly_name: Mapped[str] = Column(
        String(64), nullable=False
    )  # type: ignore[assignment]
    brand: Mapped[str] = Column(String(64), nullable=False)  # type: ignore[assignment]
    external_id: Mapped[str] = Column(
        String(64), nullable=False
    )  # type: ignore[assignment]

    def to_pydantic(self) -> CardTerminalData:
        return CardTerminalData(
            id=self.id,
            friendly_name=self.friendly_name,
            brand=self.brand,
            externalId=self.external_id,
        )
