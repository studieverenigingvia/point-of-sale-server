import enum
import typing
from typing import Any

from pydantic import BaseModel, Field
from sqlalchemy import Column, Integer, ForeignKey, Index
from sqlalchemy.ext.hybrid import hybrid_property, Comparator
from sqlalchemy.orm import Mapped, relationship
from sqlalchemy import Enum as SQLEnum

from ._base import Base
from app.models.checkout import CheckoutMethod, CheckoutMethodSQL
from .account_type import AccountType
from .database import Model

if typing.TYPE_CHECKING:
    from app.models.account import Account
    from app.models.product import Product
    from app.models.transaction import Transaction
    from app.models.sepa_batch import SepaBatch


class OrderRegister(enum.Enum):
    DEFAULT = "default"
    PENDING = "pending"


OrderRegisterSQL = SQLEnum(
    OrderRegister,
    values_callable=lambda x: [e.value for e in x],
    name="order_register",
)


class OrderFlowType(enum.Enum):
    NONE_TO_COMPLETED = "none_to_completed"
    NONE_TO_PENDING = "none_to_pending"
    PENDING_TO_COMPLETED = "pending_to_completed"
    PENDING_TO_DELETED = "pending_to_deleted"
    PENDING_TO_CANCELLED = "pending_to_cancelled"
    PENDING_TO_EXPIRED = "pending_to_expired"
    COMPLETED_TO_DELETED = "completed_to_deleted"


OrderFlowTypeSQL = SQLEnum(
    OrderFlowType,
    values_callable=lambda x: [e.value for e in x],
    name="order_flow_type",
)


def account_type_to_order_type(account_type: AccountType) -> str:
    if account_type == AccountType.USER or account_type == AccountType.EVENT:
        return "ACCOUNT_TOP_UP_OR_ACCOUNT_PAYMENT"

    if account_type == AccountType.PRODUCT_LEDGER:
        return "PRODUCT_PURCHASE"

    if account_type == AccountType.INCOME_LEDGER:
        return "DIRECT_PAYMENT"

    # Should be unreachable
    raise ValueError("Invalid AccountType/OrderType")


class CheckoutMethodComparator(Comparator):
    """Hybrid value representing a lower case representation of a word."""

    def __eq__(self, other):
        return self.__clause_element__() == other.name.lower()


class OrderData(BaseModel):
    class Product(BaseModel):
        id: int
        name: str

    class Account(BaseModel):
        id: int
        name: str
        ledgerAccount: bool

    id: int
    product: Product | None
    account: Account
    method: str
    count: int
    price: int
    type: str
    order_register: str = Field(alias="register")
    flow_type: str
    deleted: bool


class Order(Model, Base):
    # the deleted property shows if this order is used to delete a different order
    __tablename__ = "order"

    account_id: Mapped[int] = Column(  # type: ignore[assignment]
        Integer,
        ForeignKey("account.id"),
        nullable=False,
    )
    account: "Mapped[Account]" = relationship(
        "Account", back_populates="orders"
    )  # type: ignore[assignment]
    product_id: Mapped[int | None] = Column(  # type: ignore[assignment]
        Integer,
        ForeignKey("product.id"),
        nullable=True,
    )
    product: "Mapped[Product | None]" = relationship(
        "Product", back_populates="orders"
    )  # type: ignore[assignment]

    transaction_id: Mapped[int] = Column(  # type: ignore[assignment]
        Integer,
        ForeignKey("transaction.id"),
        nullable=False,
    )
    transaction: "Mapped[Transaction]" = relationship(
        # type: ignore[assignment]
        "Transaction",
        back_populates="orders",
    )
    sepa_batch_id: Mapped[int | None] = Column(
        # type: ignore[assignment]
        Integer,
        ForeignKey("sepa_batch.id"),
        nullable=True,
    )
    sepa_batch: "Mapped[SepaBatch | None]" = relationship(
        # type: ignore[assignment]
        "SepaBatch",
        back_populates="orders",
    )

    _method: Mapped[CheckoutMethod] = Column(  # type: ignore[assignment]
        CheckoutMethodSQL,
        name="method",
        nullable=False,
    )

    price: Mapped[int] = Column(Integer, nullable=False)  # type: ignore[assignment]

    register: Mapped[OrderRegister] = Column(  # type: ignore[assignment]
        OrderRegisterSQL,
        nullable=False,
    )

    flow_type: Mapped[OrderFlowType] = Column(  # type: ignore[assignment]
        OrderFlowTypeSQL,
        nullable=False,
    )

    __table_args__ = (
        Index("order_transaction_id_idx", "transaction_id"),
        Index("order_account_id_idx", "account_id"),
        Index("order_product_id_idx", "product_id"),
        Index("order_price_idx", "price"),
    )

    @hybrid_property
    def method(self) -> CheckoutMethod:
        return CheckoutMethod[self._method.upper()]

    @method.setter  # type: ignore[no-redef]
    def method(self, value):
        self._method = value.name.lower()

    @method.expression  # type: ignore[no-redef]
    def method(self):
        return self._method

    @method.comparator  # type: ignore[no-redef]
    def method(self):
        """
        CheckoutMethod enum can be used in SQLAlchemy queries directly.
        """
        return CheckoutMethodComparator(self._method)

    def to_pydantic(self) -> OrderData:
        return OrderData(
            id=self.id,
            product=OrderData.Product(
                id=self.product_id,
                name=self.product.name,
            )
            if self.product
            else None,
            account=OrderData.Account(
                id=self.account_id,
                name=self.account.name,
                ledgerAccount=self.account.type == AccountType.PRODUCT_LEDGER
                or self.account.type == AccountType.INCOME_LEDGER,
            ),
            method=self.method.name.lower(),  # type: ignore[attr-defined]
            price=self.price,
            count=1,
            type=account_type_to_order_type(self.account.type),
            register=self.register.name.lower(),
            flow_type=self.flow_type.name.lower(),
            deleted=self.deleted_at is not None,
        )

    def serialize(self) -> dict[str, Any]:
        return {
            "product": {
                "id": self.product_id,
                "name": self.product.name,
            }
            if self.product
            else None,
            "account": {
                "id": self.account_id,
                "name": self.account.name,
            }
            if self.account
            else None,
            "method": self.method.name.lower(),  # type: ignore[attr-defined]
            "price": self.price,
            "type": self.type.name,  # type: ignore[attr-defined]
            "flow_type": self.flow_type.name.lower(),
            "register": self.register.name.lower(),
            "deleted": self.deleted_at is not None,
        }
