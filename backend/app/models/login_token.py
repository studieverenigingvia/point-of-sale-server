"""Represent a token used by users to authenticate with the API."""
from sqlalchemy import Integer, ForeignKey, Column, String, Boolean
from sqlalchemy.orm import relationship, Mapped

from ._base import Base
from .database import Model
from .user import User


class LoginToken(Model, Base):
    """Represent a token used by users to authenticate with the API."""

    __tablename__ = "login_token"

    user_id: Mapped[int] = Column(Integer, ForeignKey(User.id), nullable=False)  # type: ignore[assignment]  # noqa: E501
    user: Mapped[User] = relationship(User, back_populates="tokens", uselist=False)  # type: ignore[assignment]  # noqa: E501
    token: Mapped[str] = Column(String(32), unique=True, nullable=False)  # type: ignore[assignment]  # noqa: E501
    tfa_enabled: Mapped[bool | None] = Column(Boolean, default=False, nullable=True)  # type: ignore[assignment]  # noqa: E501
