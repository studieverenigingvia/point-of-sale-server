import typing
from typing import Any

import bcrypt
from pydantic import BaseModel, Field
from sqlalchemy import Column, String, Boolean, Integer, ForeignKey, UniqueConstraint
from sqlalchemy.orm import Session, relationship, Mapped

from ._base import Base
from .database import Model

from app.models.user_direct_debit import UserDirectDebitData

if typing.TYPE_CHECKING:
    from app.models.account import Account
    from .register import Register
    from app.models import LoginToken, UserDirectDebit


class UserData(BaseModel):
    class UserRegisterData(BaseModel):
        id: int
        name: str
        isDefault: bool

    id: int
    username: str
    isAdmin: bool
    longLogin: bool
    accountId: int | None
    viaductUserId: int | None
    register_: UserRegisterData | None = Field(alias="register")
    name: str
    directDebit: UserDirectDebitData


class User(Model, Base):
    __tablename__ = "user"

    username: Mapped[str] = Column(String(64), unique=True)  # type: ignore[assignment]  # noqa: E501
    password: Mapped[str | None] = Column(String(64), nullable=True)  # type: ignore[assignment]  # noqa: E501

    is_admin: Mapped[bool] = Column(Boolean, default=False, nullable=False)  # type: ignore[assignment]  # noqa: E501
    long_login: Mapped[bool] = Column(Boolean, default=False, nullable=False)  # type: ignore[assignment]  # noqa: E501

    account_id: Mapped[int | None] = Column(  # type: ignore[assignment]
        Integer,
        ForeignKey("account.id"),
        nullable=True,
    )
    account: Mapped["Account | None"] = relationship("Account", back_populates="user")  # type: ignore[assignment]  # noqa: E501

    viaduct_user_id: Mapped[int | None] = Column(Integer, nullable=True)  # type: ignore[assignment]  # noqa: E501

    register_id: Mapped[int | None] = Column(Integer, ForeignKey("register.id"), nullable=True)  # type: ignore[assignment]  # noqa: E501
    register: Mapped["Register | None"] = relationship("Register")  # type: ignore[assignment]  # noqa: E501

    name: Mapped[str] = Column(String(512), nullable=False, default="")  # type: ignore[assignment]  # noqa: E501

    tokens: Mapped["LoginToken"] = relationship("LoginToken", back_populates="user")  # type: ignore[assignment]  # noqa: E501

    direct_debit: Mapped["UserDirectDebit | None"] = relationship(  # type: ignore[assignment]  # noqa: E501
        "UserDirectDebit", uselist=False, back_populates="user"
    )

    __table_args__ = (UniqueConstraint(account_id, name="user_account_id_key"),)

    def __init__(self, username: str | None = None, **kwargs: Any) -> None:
        if username is not None:
            username = username.lower()

        super().__init__(username=username, **kwargs)

    # TODO Move this to service.
    @classmethod
    def register_user(
        cls,
        db_session: Session,
        username: str,
        password: str | None,
        is_admin: bool = False,
        viaduct_user_id: int | None = None,
    ) -> "User":
        user = cls(username=username)

        if password is not None:
            user.set_password(password)
        user.is_admin = is_admin
        if is_admin:
            user.long_login = True
        user.viaduct_user_id = viaduct_user_id

        db_session.add(user)
        return user

    def set_password(self, password: str) -> None:
        pwhash = bcrypt.hashpw(password.encode("utf-8"), bcrypt.gensalt())
        self.password = pwhash.decode("utf-8")

    def to_pydantic(self) -> UserData:
        return UserData(
            id=self.id,
            username=self.username,
            isAdmin=self.is_admin,
            longLogin=self.long_login,
            accountId=self.account_id,
            viaductUserId=self.viaduct_user_id,
            register=self.register.to_pydantic(include_categories=False)
            if self.register
            else None,
            name=self.name,
            directDebit=self.direct_debit.to_pydantic()
            if self.direct_debit
            else UserDirectDebitData(enabled=False, userId=self.id),
        )

    def serialize(self) -> dict[str, Any]:
        return {
            "id": self.id,
            "username": self.username,
            "isAdmin": self.is_admin,
            "longLogin": self.long_login,
            "accountId": self.account_id,
            "viaductUserId": self.viaduct_user_id,
            "registerId": self.register_id,
            "name": self.name,
        }
