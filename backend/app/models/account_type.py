from enum import Enum
from sqlalchemy import Enum as SQLEnum


class AccountType(Enum):
    USER = "user"
    EVENT = "event"
    PRODUCT_LEDGER = "product_ledger"
    INCOME_LEDGER = "income_ledger"


AccountTypeSQL = SQLEnum(
    AccountType,
    values_callable=lambda x: [e.value for e in x],
    name="account_type",
)
