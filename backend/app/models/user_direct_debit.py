from datetime import date
from typing import TYPE_CHECKING
import pydantic

from sqlalchemy import (
    CheckConstraint,
    Column,
    Index,
    Integer,
    ForeignKey,
    Date,
    String,
    Boolean,
)
from sqlalchemy.orm import Mapped, relationship

from ._base import Base
from .database import Model

if TYPE_CHECKING:
    from .user import User


class UserDirectDebitData(pydantic.BaseModel):
    userId: int
    enabled: bool
    iban: str | None = None
    bic: str | None = None
    mandateId: str | None = None
    signDate: str | None = None


class UserDirectDebit(Model, Base):
    enabled: Mapped[bool] = Column(Boolean, nullable=False)  # type: ignore[assignment]  # noqa: E501
    user_id: Mapped[int] = Column(Integer, ForeignKey("user.id"), nullable=False)  # type: ignore[assignment]  # noqa: E501
    iban: Mapped[str | None] = Column(String(48), nullable=True)  # type: ignore[assignment]  # noqa: E501
    bic: Mapped[str | None] = Column(String(16), nullable=True)  # type: ignore[assignment]  # noqa: E501
    mandate_id: Mapped[str | None] = Column(String(64), nullable=True)  # type: ignore[assignment]  # noqa: E501
    sign_date: Mapped[date | None] = Column(Date, nullable=True)  # type: ignore[assignment]  # noqa: E501
    user: Mapped["User"] = relationship("User", back_populates="direct_debit", uselist=False)  # type: ignore[assignment]  # noqa: E501

    __tablename__ = "user_direct_debit"

    __table_args__ = (
        Index("user_direct_debit_user_id_idx", "user_id", unique=True),
        CheckConstraint(
            "enabled IS FALSE OR "
            + "(iban IS NOT NULL AND bic IS NOT NULL AND mandate_id IS NOT NULL "
            + "AND sign_date IS NOT NULL)",
            name="user_direct_debit_enabled_check",
        ),
    )

    def to_pydantic(self) -> UserDirectDebitData:
        return UserDirectDebitData(
            enabled=self.enabled,
            userId=self.user_id,
            iban=self.iban,
            bic=self.bic,
            mandateId=self.mandate_id,
            signDate=self.sign_date.isoformat() if self.sign_date else None,
        )
