from datetime import datetime, timedelta, date
import re
from typing import TypedDict

from sqlalchemy.orm import Session

from app.service import setting_service
from app.domains.exact.api.exact_api import ExactApi


class TransactionLine(TypedDict):
    entry_number: int
    financial_period: int
    financial_year: int
    journal_code: str
    amount_default_currency: float
    date: date
    status: int
    type: int


def upload_gl_transactions(db_session: Session, api: ExactApi, xml_body: bytes) -> str:
    """Uploads an xml of GLTransactions to Exact.

    Returns an XML string with Exact status messages."""

    journal_code = setting_service.get_db_settings(db_session).exact_journal_pos_code
    response = api.raw_request(
        "POST",
        "docs/XMLUpload.aspx",
        params={
            "Topic": "GLTransactions",
            "_Division_": api.division,
            "Params_Journal": journal_code,
        },
        headers={
            "Accept": "application/xml",
        },
        omit_api_from_url=True,
        body=xml_body,
    )

    # Automatically detect encoding.
    response.encoding = response.apparent_encoding

    # Raises HTTPError when not 2xx
    response.raise_for_status()

    return response.text


def get_bulk_financial_transactions_lines(
    api: ExactApi, filter: str
) -> list[TransactionLine]:
    lines = api.json_request(
        "GET",
        "bulk/Financial/TransactionLines",
        select="EntryNumber, FinancialPeriod, FinancialYear, "
        + "JournalCode, AmountDC, Date, Status, Type",
        filter=filter,
        order_by="Date asc",
    )

    return [
        TransactionLine(
            entry_number=line["EntryNumber"],
            financial_period=line["FinancialPeriod"],
            financial_year=line["FinancialYear"],
            journal_code=line["JournalCode"],
            amount_default_currency=line["AmountDC"],
            date=_parse_exact_date_str(line["Date"]).date(),
            status=line["Status"],
            type=line["Type"],
        )
        for line in lines
    ]


def _parse_exact_date_str(date: str) -> datetime:
    """Parses Exact date strings.

    String are in the format '/Date(<ms_since_utc>)/', e.g. '/Date(1643068800000)/'"""
    match = re.match(r"\/Date\((\d+)\)\/", date)

    if match is None:
        raise ValueError("Not an expected Exact date string.")

    (ms_since_utc_str,) = match.groups()

    if not ms_since_utc_str:
        raise ValueError("Not an expected Exact date string.")

    return datetime(1970, 1, 1) + timedelta(milliseconds=int(ms_since_utc_str))
