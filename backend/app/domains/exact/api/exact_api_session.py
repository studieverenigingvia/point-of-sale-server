from fastapi import Depends
from sqlalchemy.orm import Session
from app.dependencies import get_db
from app.domains.exact.api.exact_api import ExactApi
from app.domains.exact.setup import exact_setup_service
from app.views.exceptions import NotAcceptableException, ValidationException
from app.service import setting_service
from dataclasses import dataclass


@dataclass
class ExactContext:
    api: ExactApi


def exact_api(db_session: Session = Depends(get_db)) -> ExactContext:
    return ExactContext(api=get_exact_api_for_session(db_session))


def exact_api_export_ready(db_session: Session = Depends(get_db)) -> ExactContext:
    try:
        api = get_exact_api_for_session(db_session)
    except ValidationException as e:
        raise NotAcceptableException("setup_not_finished") from e

    if not exact_setup_service.can_export(db_session, api).is_ready():
        raise NotAcceptableException("setup_not_finished")

    return ExactContext(api=api)


def get_exact_api_for_session(db_session: Session) -> ExactApi:
    db_settings = setting_service.get_db_settings(db_session)
    if (
        db_settings.exact_storage_client_id is None
        or db_settings.exact_storage_client_secret is None
    ):
        raise ValidationException("exact_storage_settings_not_set")
    return ExactApi(
        db_session=db_session,
        base_url=db_settings.exact_storage_base_url,
        client_id=db_settings.exact_storage_client_id,
        client_secret=db_settings.exact_storage_client_secret,
        division=db_settings.exact_storage_division,
    )
