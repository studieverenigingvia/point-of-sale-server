import logging
from typing import Any

import requests
from redis.lock import Lock
from requests import Session, Response
from sqlalchemy import orm

# TODO Python 3.9 import from typing, 3.8 + pydantic requires typing_extensions
from typing_extensions import TypedDict

from app.models.database import RedisKey, broker
from app.service import setting_service
from app.views.exceptions import (
    ServiceUnavailableException,
    NotFoundException,
    ApplicationException,
)

_logger = logging.getLogger(__name__)
AUTH_URL = "https://start.exactonline.nl/api/oauth2/auth"
REST_URL = "https://start.exactonline.nl/api/"
TOKEN_URL = "https://start.exactonline.nl/api/oauth2/token/"

# How many pages to get when there is a __next present
ITERATION_LIMIT = 50


class ExactApiException(ApplicationException):
    error = "exact_error"

    def __init__(
        self, exact_status_code: int, exact_content: str, error: str = None
    ) -> None:
        super().__init__(error)
        _logger.debug(
            "ExactApiException [status=%s]: %s", exact_status_code, exact_content
        )
        self.exact_status_code = exact_status_code
        self.exact_content = exact_content


class IterationLimitReached(ApplicationException):
    pass


class ExactTokenException(ApplicationException):
    code = 503
    error = "exact_token_expired"


class ExactRedisKeys(RedisKey):
    ACCESS_TOKEN = "exact_service.access_token"
    REFRESH_LOCK = "exact_service.refresh_token_lock"


class ExactOAuthTokenJsonResponse(TypedDict):
    expires_in: int
    access_token: str
    refresh_token: str


class ExactApi:
    def __init__(
        self,
        db_session: orm.Session,
        base_url: str,
        client_id: str,
        client_secret: str,
        division: str | None = None,
        code: str | None = None,
    ) -> None:
        self.session = Session()
        self.db_session = db_session

        self.base_url = base_url
        self.client_id = client_id
        self.client_secret = client_secret

        self.code = code
        self.division = division if division is not None else "0"

        # A lock that:
        # - Lasts maximum 5 seconds, which should be more than enough time call
        #   Exact Online refresh oauth API
        # - Waits maximum 10 seconds to be acquired, when taken.
        self.refresh_lock = Lock(
            broker, ExactRedisKeys.REFRESH_LOCK, timeout=5, blocking_timeout=10
        )

    def __del__(self):
        self.session.close()

    @property
    def access_token(self) -> str:
        access_token = broker.get(ExactRedisKeys.ACCESS_TOKEN)
        if not access_token:
            return self._refresh_token()
        return access_token.decode()

    def json_request(
        self,
        method: str,
        resource: str,
        body: dict = None,
        filter: str = None,
        select: str = None,
        top: str = None,
        order_by: str = None,
    ) -> list:

        resource = f"v1/{self.division}/{resource}"

        response = self.raw_request(
            method,
            resource,
            body=body,
            filter=filter,
            select=select,
            top=top,
            order_by=order_by,
        )

        if not response.ok:
            raise ExactApiException(
                exact_status_code=response.status_code,
                exact_content=response.content.decode(),
            )

        if method == "DELETE" or method == "PUT":
            return []

        return self._parse_paginated_json(response.json(), method)

    def raw_request(
        self,
        method: str,
        resource: str,
        body: dict | bytes | None = None,
        filter: str = None,
        select: str = None,
        top: str = None,
        order_by: str = None,
        params: dict | None = None,
        headers: dict | None = None,
        omit_api_from_url: bool = False,
    ) -> Response:
        url = REST_URL.rstrip("api/") + "/" if omit_api_from_url else REST_URL
        url += resource
        token = self.access_token
        request = requests.Request(
            method,
            url,
            params={
                "$filter": filter,
                "$select": select,
                "$top": top,
                "$orderby": order_by,
                **(params if params is not None else {}),
            },
            headers={
                "Accept": "application/json",
                "Authorization": f"Bearer {token}",
                **(headers if headers is not None else {}),
            },
        )

        if body is not None:
            request.data = body

        prepared_request = request.prepare()

        response = self.session.send(prepared_request)
        if response.status_code == 401:
            _logger.warning(
                "Exact API responded 401, should not happen. Response: %s",
                response.content,
            )
            raise ExactTokenException()
        return response

    def get_divisions(self) -> tuple[dict[str, str], str]:
        resp1 = self.raw_request("GET", "v1/current/Me", select="CurrentDivision")
        current_division = resp1.json()["d"]["results"][0]["CurrentDivision"]

        resource = f"v1/{current_division}/hrm/Divisions"
        resp2 = self.raw_request("GET", resource, select="Code,Description")
        json = resp2.json()["d"]["results"]

        choices = {i["Code"]: i["Description"] for i in json}
        return choices, current_division

    def check_division_exists(self, division: str) -> None:
        resource = f"v1/{division}/crm/Accounts"
        response = self.raw_request(
            "GET", resource, filter="Name eq 'NON_EXISTENT'", select="ID"
        )

        if not response.ok:
            _logger.debug("%s", response.content)
            raise NotFoundException("division_not_found")

    def request_token(self, code: str) -> None:
        redirect_uri = ExactApi.create_redirect_url(self.base_url)
        _logger.debug(
            "Request token: 'authorization_code' grant,"
            "client_id '%s', redirect_url '%s'",
            self.client_id,
            redirect_uri,
        )
        request = requests.Request(
            "POST",
            TOKEN_URL,
            data={
                "client_id": self.client_id,
                "client_secret": self.client_secret,
                "code": code,
                "grant_type": "authorization_code",
                "redirect_uri": redirect_uri,
            },
        ).prepare()

        response = self.session.send(request)

        if not response.ok:
            _logger.debug("%s", response.content)
            raise ServiceUnavailableException("exact_online_unavailable")

        self._set_token(response.json())

    def _parse_paginated_json(self, json_response: dict[str, Any], method: str) -> list:
        d = json_response["d"]

        if isinstance(d, list):
            #  Sometimes d is a list instead of a dict. This means there are
            #  no more pages.
            return d

        results = d["results"]
        iteration = 0
        while "__next" in d:
            if iteration > ITERATION_LIMIT:
                raise IterationLimitReached()

            #  Strip base url from the url the api returned
            next_url = d["__next"][len(REST_URL) :]
            response = self.raw_request(method, next_url)

            if not response.ok:
                _logger.debug("%s", response.content)
                raise ExactApiException(
                    exact_status_code=response.status_code,
                    exact_content=response.content.decode(),
                )

            d = response.json()["d"]
            results.extend(d["results"])

            iteration += 1

        return results

    def _refresh_token(self) -> str:
        lock = self.refresh_lock.acquire(blocking=False)
        if lock:
            _logger.debug("Refresh lock acquired, attempting refresh")
            refresh_token = setting_service.get_db_settings(
                self.db_session
            ).exact_storage_refresh_token
            if not refresh_token:
                raise ExactTokenException()

            _logger.debug(
                "Request token: 'refresh_token' grant, client_id '%s'", self.client_id
            )

            request = requests.Request(
                "POST",
                TOKEN_URL,
                data={
                    "client_id": self.client_id,
                    "client_secret": self.client_secret,
                    "grant_type": "refresh_token",
                    "refresh_token": refresh_token,
                },
            ).prepare()

            response = self.session.send(request)

            if not response.ok:
                _logger.debug("%s", response.content)
                if response.json().get("error") == "unauthorized_client":
                    raise ExactTokenException()
                raise ServiceUnavailableException("exact_online_unavailable")

            return self._set_token(response.json())
        else:
            _logger.debug("Refresh token lock taken, waiting for new token")
            with self.refresh_lock:
                _logger.debug("Refresh lock acquired, token should be refreshed.")
                token = broker.get(ExactRedisKeys.ACCESS_TOKEN)
                if not token:
                    raise ExactTokenException()
                return token.decode()

    def _set_token(self, json_response: ExactOAuthTokenJsonResponse) -> str:
        expires_in = int(json_response["expires_in"])

        _logger.debug("Saving token with expiry %d", expires_in)

        broker.setex(
            name=ExactRedisKeys.ACCESS_TOKEN,
            time=expires_in,
            value=json_response["access_token"],
        )

        db_settings = setting_service.get_db_settings(self.db_session)
        db_settings.exact_storage_refresh_token = json_response["refresh_token"]
        setting_service.save_db_settings(self.db_session, db_settings)
        return json_response["access_token"]

    @staticmethod
    def create_auth_request_url(base_url: str, client_id: str) -> str:
        request = requests.Request(
            "GET",
            AUTH_URL,
            params={
                "client_id": client_id,
                "redirect_uri": ExactApi.create_redirect_url(base_url),
                "response_type": "code",
            },
        )

        url = request.prepare().url

        if url is None:
            #  Parameters are well-formed, this cannot happen.
            raise RuntimeError("invalid_state")

        return url

    @staticmethod
    def create_redirect_url(base_url: str) -> str:
        return base_url + "/admin/exact/oauth/success"
