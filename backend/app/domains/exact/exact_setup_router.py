from http import HTTPStatus

import pydantic
from fastapi import APIRouter, Depends, Response
from pydantic import BaseModel

from app.dependencies import UserRequestContext, admin_required
from app.domains.exact.api.exact_api_session import ExactContext, exact_api
from app.service import setting_service
from app.domains.exact.setup import exact_setup_service
from app.domains.exact import exact_tasks as exact_tasks

router = APIRouter()


class ExactClientSetupJson(pydantic.BaseModel):
    baseUrl: str
    clientId: str
    clientSecret: str


class ExactAuthorizeUrlResponse(BaseModel):
    url: str


@router.post("/client/", response_model=ExactAuthorizeUrlResponse)
def api_admin_exact_client(
    request_json: ExactClientSetupJson,
    context: UserRequestContext = Depends(admin_required),
) -> ExactAuthorizeUrlResponse:
    """
    The entire setup process is as follows:
    * POST exact/setup/client - Set client id/secret, this is copied by the
                user from Exact's app center. Will return a redirect URL
                for authentication.
    * POST exact/setup/token - Set auth token gotten from Exact's redirect.
    * GET exact/setup/divisions - Get list of available administrations (divisions)
    * POST exact/setup/division - Sets the division we'll be working in.
    * POST exact/setup/journals - Sets the journal code and name to which
                we will export.
    * PUT  exact/setup/finish - Finished the setup. (Just a confirmation)

    The setup is finished, but we before we can export we need the following:
    * PUT  accounts/ledgers/exact/ - Link every PoS ledger to a ledger account
                in Exact Online.

    Now we can check if we are ready to Export using:
    * GET  exact/export/ready - returns three bools indiciating ready status.
    """
    url = exact_setup_service.set_client(
        context, request_json.baseUrl, request_json.clientId, request_json.clientSecret
    )

    return ExactAuthorizeUrlResponse(url=url)


@router.get("/reauth/", response_model=ExactAuthorizeUrlResponse)
def api_admin_exact_oauth_url(
    context: UserRequestContext = Depends(admin_required),
) -> ExactAuthorizeUrlResponse:
    return ExactAuthorizeUrlResponse(url=exact_setup_service.get_oauth_url(context))


@router.post("/unlink/", response_class=Response, status_code=HTTPStatus.NO_CONTENT)
def api_admin_exact_setup_unlink(
    context: UserRequestContext = Depends(admin_required),
) -> None:
    """Deletes all Exact related settings and sets finished to false."""
    exact_setup_service.unlink(context)


class ExactSetupTokenJson(pydantic.BaseModel):
    code: str


class ExactTokenResponse(BaseModel):
    finished: bool


@router.post("/token/", response_model=ExactTokenResponse)
def api_admin_exact_token(
    request_json: ExactSetupTokenJson,
    context: UserRequestContext = Depends(admin_required),
    exact_context: ExactContext = Depends(exact_api),
) -> ExactTokenResponse:
    finished = exact_setup_service.set_token(
        context, exact_context.api, request_json.code
    )

    return ExactTokenResponse(finished=finished)


class ExactSetupDivisionTokenJson(pydantic.BaseModel):
    code: str


@router.post("/division/", response_class=Response, status_code=HTTPStatus.NO_CONTENT)
def api_admin_exact_division(
    request_json: ExactSetupDivisionTokenJson,
    context: UserRequestContext = Depends(admin_required),
    exact_context: ExactContext = Depends(exact_api),
) -> None:
    exact_setup_service.set_division(
        context, exact_context.api, division=request_json.code
    )


class ExactSetupJournalJson(pydantic.BaseModel):
    code: str
    description: str


@router.post("/journals/", response_class=Response, status_code=HTTPStatus.NO_CONTENT)
def api_admin_exact_set_journal(
    request_json: ExactSetupJournalJson,
    context: UserRequestContext = Depends(admin_required),
) -> None:
    db_settings = setting_service.get_db_settings(context.db_session)
    db_settings.exact_journal_pos_code = request_json.code
    db_settings.exact_journal_pos_description = request_json.description
    setting_service.save_db_settings(context.db_session, db_settings)


class ExactSetupFinishJson(pydantic.BaseModel):
    finished: bool


@router.put("/finish/", response_class=Response, status_code=HTTPStatus.NO_CONTENT)
def api_admin_exact_setup_finish(
    request_json: ExactSetupFinishJson,
    context: UserRequestContext = Depends(admin_required),
) -> None:
    db_settings = setting_service.get_db_settings(context.db_session)
    db_settings.exact_setup_finished = request_json.finished
    setting_service.save_db_settings(context.db_session, db_settings)
