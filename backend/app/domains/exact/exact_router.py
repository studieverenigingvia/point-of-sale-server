from fastapi import APIRouter, Depends
from pydantic import BaseModel

from app.dependencies import UserRequestContext, admin_required
from app.domains.exact.api.exact_api_session import ExactContext, exact_api
from app.domains.exact.external import exact_external_service
from app.domains.exact.external.exact_external_service import (
    ExactDivision,
    ExactJournal,
    ExactLedger,
    ExactCostCenter,
    ExactVat,
)
from app.domains.exact import exact_tasks as exact_tasks

router = APIRouter()


class ExactDivisionResponse(BaseModel):
    administrations: list[ExactDivision]
    current: str


@router.get("/divisions/", response_model=ExactDivisionResponse)
def api_admin_exact_divisions(
    context: UserRequestContext = Depends(admin_required),
    exact_context: ExactContext = Depends(exact_api),
) -> ExactDivisionResponse:
    administrations, current = exact_external_service.get_divisions(exact_context.api)

    return ExactDivisionResponse(administrations=administrations, current=current)


class ExactJournalsResponse(BaseModel):
    journals: list[ExactJournal]


@router.get("/journals/", response_model=ExactJournalsResponse)
def api_admin_exact_journals(
    context: UserRequestContext = Depends(admin_required),
    exact_context: ExactContext = Depends(exact_api),
) -> ExactJournalsResponse:
    return ExactJournalsResponse(
        journals=exact_external_service.get_journals(exact_context.api)
    )


class ExactLedgersResponse(BaseModel):
    ledgers: list[ExactLedger]


@router.get("/ledgers/", response_model=ExactLedgersResponse)
def api_admin_exact_ledgers(
    context: UserRequestContext = Depends(admin_required),
    exact_context: ExactContext = Depends(exact_api),
) -> ExactLedgersResponse:
    return ExactLedgersResponse(
        ledgers=exact_external_service.get_ledgers(exact_context.api)
    )


class ExactCostCenterResponse(BaseModel):
    costCenters: list[ExactCostCenter]


@router.get("/cost-centers/", response_model=ExactCostCenterResponse)
def api_admin_exact_cost_centers(
    context: UserRequestContext = Depends(admin_required),
    exact_context: ExactContext = Depends(exact_api),
) -> ExactCostCenterResponse:
    return ExactCostCenterResponse(
        costCenters=exact_external_service.get_cost_centers(exact_context.api)
    )


class ExactVatCodesResponse(BaseModel):
    vatCodes: list[ExactVat]


@router.get("/vat-codes/", response_model=ExactVatCodesResponse)
def api_admin_exact_vat_codes(
    context: UserRequestContext = Depends(admin_required),
    exact_context: ExactContext = Depends(exact_api),
) -> ExactVatCodesResponse:
    return ExactVatCodesResponse(
        vatCodes=exact_external_service.get_vat_codes(exact_context.api)
    )
