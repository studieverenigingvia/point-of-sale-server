from datetime import datetime, timezone

import pytest
from requests_mock import Mocker
from sqlalchemy.orm import Session
from starlette.testclient import TestClient

from app.models.account_type import AccountType
from app.models.product import Product
from tests.util.transactions import (
    _create_direct_purchase,
    _create_direct_card_purchase,
)


@pytest.mark.usefixtures("exact_finished_settings")
def test_exact_comparison_card_orders(
    admin_client: TestClient,
    db_session: Session,
    requests_mocker: Mocker,
    products: dict[str, Product],
) -> None:
    requests_mocker.get(
        ("https://start.exactonline.nl/api/v1/1337/financial/GLAccounts"),
        json={
            "d": {
                "results": [
                    {
                        "ID": "d646794e-5641-45bc-a63a-aa16665b71fc",
                        "Code": "44200",
                        "Description": "Account ledger in exact",
                    }
                ]
            }
        },
    )

    # This creates purchases in 2020
    _create_direct_card_purchase(
        db_session, [products["room_beer"], products["room_beer"]]
    )
    _create_direct_card_purchase(
        db_session, [products["room_beer"], products["room_beer"]]
    )
    _create_direct_card_purchase(
        db_session, [products["room_beer"], products["room_beer"]]
    )

    sept2 = int(datetime(2020, 9, 2, tzinfo=timezone.utc).timestamp()) * 1000
    sept6 = int(datetime(2020, 9, 6, tzinfo=timezone.utc).timestamp()) * 1000

    requests_mocker.get(
        "https://start.exactonline.nl/api/v1/1337/bulk/Financial/TransactionLines",
        json={
            "d": {
                "results": [
                    {
                        "AmountDC": 10,
                        "Date": f"/Date({sept2})/",
                        "EntryNumber": 24900001,
                        "FinancialPeriod": 9,
                        "FinancialYear": 2020,
                        "JournalCode": "90",
                        "Status": 20,
                        "Type": 90,
                    },
                    {
                        "AmountDC": 20,
                        "Date": f"/Date({sept6})/",
                        "EntryNumber": 24900002,
                        "FinancialPeriod": 6,
                        "FinancialYear": 2020,
                        "JournalCode": "90",
                        "Status": 20,
                        "Type": 90,
                    },
                ]
            }
        },
    )

    rv = admin_client.get(
        "/api/admin/exact/comparison?dateFilter=2020-09-01~2020-09-10"
    )
    assert rv.status_code == 200, rv.json()

    resp = rv.json()
    assert resp["account"]["type"] == AccountType.INCOME_LEDGER.value

    # 3 orders for 3 products, instead of 1 for 3, because card transactions
    # have 2 extra orders.
    assert {"amount": 420, "count": 3 * 3, "date": "2020-09-01"} in resp[
        "dailyOrders"
    ], resp["dailyOrders"]

    # 10 days between September 1st and 10th
    assert len(resp["dailyTransactions"]) == 10

    #  Note that these are shifted one day compared to the exact data
    assert {"amount": -1000, "date": "2020-09-01"} in resp["dailyTransactions"]
    assert {"amount": -2000, "date": "2020-09-05"} in resp["dailyTransactions"]


@pytest.mark.usefixtures("exact_finished_settings")
def test_exact_comparison_product_ledger(
    admin_client: TestClient,
    db_session: Session,
    requests_mocker: Mocker,
    products: dict[str, Product],
) -> None:
    requests_mocker.get(
        ("https://start.exactonline.nl/api/v1/1337/financial/GLAccounts"),
        json={
            "d": {
                "results": [
                    {
                        "ID": "d646794e-5641-45bc-a63a-aa16665b71fc",
                        "Code": "44200",
                        "Description": "Account ledger in exact",
                    }
                ]
            }
        },
    )

    # This creates purchases in 2020
    _create_direct_purchase(db_session, [products["room_beer"], products["room_beer"]])
    _create_direct_purchase(db_session, [products["room_beer"], products["room_beer"]])
    _create_direct_purchase(db_session, [products["room_beer"], products["room_beer"]])

    sept1 = int(datetime(2020, 9, 1, tzinfo=timezone.utc).timestamp()) * 1000

    requests_mocker.get(
        "https://start.exactonline.nl/api/v1/1337/bulk/Financial/TransactionLines",
        json={
            "d": {
                "results": [
                    {
                        "AmountDC": -4.2,
                        "Date": f"/Date({sept1})/",
                        "EntryNumber": 24900001,
                        "FinancialPeriod": 9,
                        "FinancialYear": 2020,
                        "JournalCode": "90",
                        "Status": 20,
                        "Type": 90,
                    },
                ]
            }
        },
    )

    ledger_id = products["room_beer"].ledger_id
    rv = admin_client.get(
        f"/api/admin/exact/comparison?dateFilter=2020-09-01~2020-09-01&accountFilter={ledger_id}"
    )
    assert rv.status_code == 200, rv.json()

    resp = rv.json()
    assert resp["account"]["id"] == ledger_id
    assert resp["account"]["type"] == AccountType.PRODUCT_LEDGER.value

    # Now minus 420
    assert {"amount": -420, "count": 6, "date": "2020-09-01"} in resp["dailyOrders"]

    # Now not shifted
    assert len(resp["dailyTransactions"]) == 1
    assert {"amount": 420, "date": "2020-09-01"} in resp["dailyTransactions"]
