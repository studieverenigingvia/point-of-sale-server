from fastapi import APIRouter, Depends, Query
from pydantic import BaseModel
from sqlalchemy import or_

from app.dependencies import UserRequestContext, admin_required
from app.domains.account.ledger.ledger_filters import IncomeLedgerFilter
from app.models.account import Account, AccountExactLedger
from app.models.account_type import AccountType

from app.repository.util.filters import (
    LiteralEqualsFilter,
)
from app.repository.util.pagination import (
    PageParameters,
    PaginatedResponse,
    PaginatedSearch,
    SearchParameters,
    SortParameters,
    page_sort_parameters,
)
from app.domains.exact import exact_tasks as exact_tasks
from app.service import account_service
from app.service.account_service import AccountLedgerExactUpdate


router = APIRouter()


class AccountLedgerData(BaseModel):
    id: int
    name: str
    incomeLedger: bool
    ledgerCode: str | None
    costCenterCode: str | None
    vatCode: str | None
    associatedCheckoutMethod: str | None


@router.get("/", response_model=PaginatedResponse[AccountLedgerData])
def api_admin_accounts_ledgers(
    context: UserRequestContext = Depends(admin_required),
    page_params: PageParameters = Depends(),
    search_params: SearchParameters = Depends(),
    sort_params: SortParameters = Depends(
        page_sort_parameters(
            "name",
        )
    ),
    type_filter: LiteralEqualsFilter | None = Query(None, alias="typeFilter"),
    exact_vat_filter: LiteralEqualsFilter | None = Query(None, alias="vatFilter"),
    exact_ledger_filter: LiteralEqualsFilter | None = Query(None, alias="ledgerFilter"),
    exact_cost_center_filter: LiteralEqualsFilter
    | None = Query(None, alias="costCenterFilter"),
    income_ledger_filter: IncomeLedgerFilter
    | None = Query(None, alias="incomeLedgerFilter"),
) -> PaginatedResponse[AccountLedgerData]:
    q = (
        context.db_session.query(Account, AccountExactLedger)
        .outerjoin(AccountExactLedger)
        .filter(
            or_(
                Account.type == AccountType.PRODUCT_LEDGER.value,
                Account.type == AccountType.INCOME_LEDGER.value,
            )
        )
    )

    return (
        PaginatedSearch[tuple[Account, AccountExactLedger], AccountLedgerData](
            q, page_params
        )
        .set_result_mapper(
            lambda row: AccountLedgerData(
                id=row.Account.id,
                name=row.Account.name,
                incomeLedger=row.Account.type == AccountType.INCOME_LEDGER,
                ledgerCode=row.AccountExactLedger.exact_ledger_code
                if row.AccountExactLedger
                else None,
                costCenterCode=row.AccountExactLedger.exact_cost_center_code
                if row.AccountExactLedger
                else None,
                vatCode=row.AccountExactLedger.exact_vat_code
                if row.AccountExactLedger
                else None,
                associatedCheckoutMethod=row.AccountExactLedger.associated_checkout_method
                if row.AccountExactLedger
                else None,
            )
        )
        .enable_searching(search_params, [Account.name])
        .enable_sorting(sort_params, {"name": Account.name})
        .enable_filtering(
            {
                # type filter takes precedence over income ledger filter if they
                # are both defined
                Account.type: type_filter or income_ledger_filter,
                AccountExactLedger.exact_vat_code: exact_vat_filter,
                AccountExactLedger.exact_ledger_code: exact_ledger_filter,
                AccountExactLedger.exact_cost_center_code: exact_cost_center_filter,
            }
        )
        .execute()
    )


@router.put("/{account_id}/", response_model=AccountLedgerData)
def api_admin_accounts_ledger_update(
    account_id: int,
    exact_ledger: AccountLedgerExactUpdate,
    context: UserRequestContext = Depends(admin_required),
) -> AccountLedgerData:
    account_exact_ledger = account_service.set_account_exact_ledger(
        context, account_id, exact_ledger
    )
    return AccountLedgerData(
        id=account_id,
        name=account_exact_ledger.account.name,
        incomeLedger=account_exact_ledger.account.type == AccountType.INCOME_LEDGER,
        ledgerCode=account_exact_ledger.exact_ledger_code,
        costCenterCode=account_exact_ledger.exact_cost_center_code,
        vatCode=account_exact_ledger.exact_vat_code,
        associatedCheckoutMethod=account_exact_ledger.associated_checkout_method,
    )
