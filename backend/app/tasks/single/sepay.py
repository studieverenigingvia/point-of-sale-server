import os
import socket
from decimal import Decimal
from enum import IntEnum
from typing import Any

from celery import task
from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives.asymmetric.rsa import RSAPrivateKey
from pydantic import BaseSettings
from zeep import Client

import config
from app.models.transaction import TransactionStatus


class SepaySettings(BaseSettings):
    sepay_username = "penningmeester@svia.nl"


settings = SepaySettings()


class SepayException(Exception):
    pass


# These are mapped from section 2.4 Status codes
# The names are made up by us (you are allowed to change these).
class SepayStartTransactionResponseCode(IntEnum):
    OK = 0
    REQUIRED_FIELDS_MISSING = 1
    SIGNATURE_INVALID = 2
    INVALID_PARAMETERS = 4
    DUPLICATE_REQUEST = 6
    TERMINAL_NOT_ACTIVE = 7
    PENDING_REQUEST = 11
    UNDEFINED_ERROR = 99


class SepayGetTransactionCode(IntEnum):
    OK = 0
    REQUIRED_FIELDS_MISSING = 1
    SIGNATURE_INVALID = 2
    INVALID_PARAMETERS = 4
    TERMINAL_NOT_ACTIVE = 7
    TRANSACTION_FAILED = 13
    TRANSACTION_CANCELED = 14
    TRANSACTION_IS_PENDING = 15
    TRANSACTION_IS_PENDING_CANNOT_BE_CANCELED = 17
    UNDEFINED_ERROR = 99


class SepayCancelTransactionResponseCode(IntEnum):
    OK = 0
    REQUIRED_FIELDS_MISSING = 1
    SIGNATURE_INVALID = 2
    INVALID_PARAMETERS = 4
    TERMINAL_NOT_ACTIVE = 7
    TRANSACTION_IS_PENDING_CANNOT_BE_CANCELED = 17
    UNDEFINED_ERROR = 99


def _format_arg(value: Any) -> str:
    if isinstance(value, Decimal):
        return f"{value:.2f}"
    elif value is None:
        return ""
    elif value is True:
        return "true"
    elif value is False:
        return "false"
    return str(value)


# Lazy loaded VIA_key
class SepayCerts:
    private_key: RSAPrivateKey | None = None
    public_key = None

    def sign_args(self, args: list[Any]) -> bytes:
        # Generate a signature based on the given argument list
        # Returns the sign in bytes.
        to_sign = ";".join(map(_format_arg, args))

        if self.private_key is None:
            with open("sepay/viapos.pem", "rb") as f:
                key = serialization.load_pem_private_key(
                    f.read(), password=None, backend=default_backend()
                )
                if isinstance(key, RSAPrivateKey):
                    self.private_key = key
                else:
                    raise ValueError("Expected RSA private key, got something else.")

        return self.private_key.sign(
            to_sign.encode(), padding.PKCS1v15(), hashes.SHA256()
        )

    def verify_args(self, signature_b64_encoded: str, args: list[Any]) -> None:
        # Verifies the given base64-encoded string signature with the
        # given argument list.
        # Raises InvalidSignature on failure.
        (";".join(map(_format_arg, args)))

        if self.public_key is None:
            with open("sepay/sepay.pem", "rb") as f:
                self.public_key = x509.load_pem_x509_certificate(f.read())

        # self.public_key.public_key().verify(
        #     b64decode(signature_b64_encoded),
        #     to_verify.encode(),
        #     padding.PKCS1v15(),
        #     hashes.SHA256()
        # )


class SepayClient:
    client: Client | None = None

    def get_client(self) -> Client:
        if self.client is None:
            self.client = Client(
                wsdl="./sepay/sepay_api.wsdl",
            )  # type: ignore
        return self.client


sepay_certificates = SepayCerts()
sepay_client = SepayClient()


def wecr_create_transaction(
    transaction_ref: str, merchant_ref: str, value: str, sepay_sid: str
) -> tuple[dict[str, str], SepayStartTransactionResponseCode]:
    signature = sepay_certificates.sign_args(
        [
            "0",
            "2",
            settings.sepay_username,
            sepay_sid,
            transaction_ref,
            merchant_ref,
            value,
            "",
        ]
    )
    response = sepay_client.get_client().service.StartTransaction(
        key_index=0,
        version="2",
        login=settings.sepay_username,
        sid=sepay_sid,
        transactionref=transaction_ref,
        merchantref=merchant_ref,
        amount=value,
        signature=signature,
    )

    sepay_certificates.verify_args(
        response["signature"],
        [
            response["key_index"],
            response["version"],
            response["login"],
            response["sid"],
            response["transactionref"],
            response["merchantref"],
            response["amount"],
            response["status"],
            response["message"],
            response["terminalip"],
            response["terminalport"],
        ],
    )

    response_status = SepayStartTransactionResponseCode(int(response["status"]))

    return response, response_status


def wecr_cancel_transactions(
    transactionref: str, sepay_sid: str
) -> tuple[dict[str, str], SepayCancelTransactionResponseCode]:
    signature = sepay_certificates.sign_args(
        [0, "2", settings.sepay_username, sepay_sid, transactionref, False]
    )
    response = sepay_client.get_client().service.CancelTransaction(
        key_index=0,
        version="2",
        login=settings.sepay_username,
        sid=sepay_sid,
        transactionref=transactionref,
        force=False,
        signature=signature,
    )
    sepay_certificates.verify_args(
        response["signature"],
        [
            response["key_index"],
            response["version"],
            response["login"],
            response["sid"],
            response["transactionref"],
            response["status"],
            response["message"],
        ],
    )

    response_status = SepayCancelTransactionResponseCode(int(response["status"]))

    return response, response_status


def wecr_transaction_status(
    transactionref: str, sepay_sid: str, timeout: int = 10
) -> tuple[dict[str, str], SepayGetTransactionCode]:
    signature = sepay_certificates.sign_args(
        [0, "2", settings.sepay_username, sepay_sid, transactionref, timeout]
    )
    response = sepay_client.get_client().service.GetTransactionStatus(
        key_index=0,
        version="2",
        login=settings.sepay_username,
        sid=sepay_sid,
        transactionref=transactionref,
        timeout=timeout,
        signature=signature,
    )
    sepay_certificates.verify_args(
        response["signature"],
        [
            response["key_index"],
            response["version"],
            response["login"],
            response["sid"],
            response["transactionref"],
            response["merchantref"],
            response["amount"],
            response["transactiontime"],
            response["transactionerror"],
            response["transactionresult"],
            response["status"],
            response["message"],
            response["brand"],
            response["ticket"],
        ],
    )

    response_status = SepayGetTransactionCode(int(response["status"]))

    return response, response_status


def sepay_get_transaction_status(
    transactionref: str, terminal_id: str
) -> tuple[dict[str, str], SepayGetTransactionCode]:
    return wecr_transaction_status(transactionref, terminal_id)


@task
def sepay_create_transaction(
    amt: float, transaction_ref: str, terminal_id: str
) -> None:
    # Convert Amount into string with 2 decimals, as required by the API
    value = f"{amt:.2f}"

    # Generate TransactionRef
    transaction_ref = transaction_ref
    merchant_ref = f'POS-{os.environ.get("ENVIRONMENT", "production").upper()[0:4]}'

    create_or_retry_response, create_response_status = wecr_create_transaction(
        transaction_ref, merchant_ref, value, terminal_id
    )

    if create_response_status == SepayStartTransactionResponseCode.PENDING_REQUEST:
        cancel_response, cancel_response_status = wecr_cancel_transactions(
            create_or_retry_response["transactionref"], terminal_id
        )
        if cancel_response_status != SepayCancelTransactionResponseCode.OK:
            raise SepayException(cancel_response_status)

        create_or_retry_response, retry_response_status = wecr_create_transaction(
            transaction_ref, merchant_ref, value, terminal_id
        )
        if retry_response_status != SepayStartTransactionResponseCode.OK:
            raise SepayException(retry_response_status)

    elif create_response_status != SepayStartTransactionResponseCode.OK:
        raise SepayException(create_response_status)

    contact = (
        create_or_retry_response["terminalip"],
        int(create_or_retry_response["terminalport"]),
    )
    if getattr(config, "DEBUG", False):
        contact = ("127.0.0.1", 1234)

    try:
        s = socket.socket()
        s.connect(contact)
        s.send(b"\x93")
        s.close()
    except Exception as e:
        print("Error nudging device", e, contact)


def sepay_status_to_transaction_status(
    status: SepayGetTransactionCode, age: float = 0
) -> TransactionStatus:
    # Invalid parameters occurs when the transaction is not yet at the PIN,
    # this is not an error. The create_transaction method is just slow, and
    # executed on a background thread. It could also mean that the transaction
    # failed, but we can't know that for sure.
    if age > 60:
        if status == SepayGetTransactionCode.INVALID_PARAMETERS:
            return TransactionStatus.EXPIRED

    mapping = {
        SepayGetTransactionCode.OK: TransactionStatus.COMPLETED,
        SepayGetTransactionCode.TRANSACTION_CANCELED: TransactionStatus.CANCELLED,
        SepayGetTransactionCode.INVALID_PARAMETERS: TransactionStatus.PENDING,
        SepayGetTransactionCode.TRANSACTION_IS_PENDING: TransactionStatus.PENDING,
        SepayGetTransactionCode.TRANSACTION_IS_PENDING_CANNOT_BE_CANCELED: TransactionStatus.PENDING,  # noqa
    }

    return mapping.get(status, TransactionStatus.DELETED)
