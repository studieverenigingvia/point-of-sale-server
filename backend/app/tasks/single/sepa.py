from datetime import datetime
from app import worker
from app.dependencies import RequestContext
from app.models import SepaBatch
from app.models.database import SessionLocal
from app.service import sepa_service, mail_service
from app.views.exceptions import NotFoundException


@worker.task
def send_sepa_notification(sepa_batch_id: int) -> None:
    with SessionLocal.begin() as db_session:
        context = RequestContext(db_session)
        sepa_batch = db_session.get(SepaBatch, sepa_batch_id)
        if sepa_batch is None:
            raise NotFoundException("sepa_batch_not_found")
        pending_orders = sepa_service.get_pending_orders_with_user_accounts(
            context, sepa_batch
        )

        for order, _, user, _ in pending_orders:
            if order.deleted:
                continue

            command = mail_service.MailCommand(db_session=db_session, to=user.username)
            command.with_template(
                "email/sepa_batch_amount_notification.html",
                batch=sepa_batch,
                user=user,
                amount=order.price / 100,
            )  # To EUR

            mail_service.send_email(command)

        # Mark batch as all mails send.
        sepa_batch.mail_date = datetime.now()
