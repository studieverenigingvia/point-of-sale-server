import logging
from datetime import timedelta

from celery.task import periodic_task
from sqlalchemy.orm import Session, sessionmaker

import config
from app.dependencies import RequestContext
from app.models.database import SessionLocal, engine
from app.models.transaction import TransactionFlowType, TransactionStatus
from app.repository import transactions
from app.service import card_terminal_update_service, transaction_service
from app.service.flow import flow_service
from app.service.flow.flow_types import NextProcessFlowRequest
from app.views.exceptions import ValidationException

_logger = logging.getLogger(__name__)
TIMEOUT_FOR_PENDING_TRANSACTIONS = timedelta(minutes=10)
INTERVAL = timedelta(minutes=30)

if getattr(config, "DEBUG", False):
    _logger.info("Setting interval for periodic tasks to 10 seconds.")
    INTERVAL = timedelta(seconds=10)
    TIMEOUT_FOR_PENDING_TRANSACTIONS = INTERVAL


@periodic_task(run_every=INTERVAL)
def expire_idle_pending_card_transactions() -> None:
    # First fetch all card transactions that are still in pending state.
    # We also fetch the card_data to determine if the transaction was made
    # on a card terminal.

    expire_on_commit_sessionmaker = sessionmaker(
        bind=engine, future=True, expire_on_commit=False
    )

    with expire_on_commit_sessionmaker.begin() as db_session:
        db_pending = transactions.get_transactions_in_flow_and_status(
            db_session,
            TransactionFlowType.CARD_PAYMENT,
            TransactionStatus.PENDING,
            TIMEOUT_FOR_PENDING_TRANSACTIONS,
        )
        db_session.commit()

    for transaction, card_data in db_pending:
        with SessionLocal.begin() as db_session:
            is_card_terminal_transaction = bool(card_data)

            request_context = RequestContext(db_session=db_session)

            _logger.warning(
                "Transaction %s is still in pending state after %s minutes.",
                transaction.id,
                TIMEOUT_FOR_PENDING_TRANSACTIONS.total_seconds() / 60,
            )

            try:
                # If the transaction was made on a card terminal, we need to check
                # the status of the transaction at the provider, otherwise just
                # expire it.
                if is_card_terminal_transaction:
                    process_card_terminal_transaction(db_session, transaction.id)
                else:
                    req = NextProcessFlowRequest(
                        transaction_id=transaction.id,
                        desired_state=TransactionStatus.EXPIRED,
                    )

                    flow_service.process_flow_next(request_context, req)
            except ValidationException as e:
                # This can happen in the concurrent case where a transaction is
                # updated by a different process.
                _logger.warning(
                    "Transaction %s could not be expired: %s", transaction.id, e
                )


def process_card_terminal_transaction(db_session: Session, transaction_id: int) -> None:
    transaction = transaction_service.get_by_id(db_session, transaction_id)

    _, status = card_terminal_update_service.get_status_of_transaction_and_update(
        db_session, transaction
    )
    _logger.warning(
        "Transaction: %s new status from card_terminal_provider: %s",
        transaction_id,
        status,
    )
