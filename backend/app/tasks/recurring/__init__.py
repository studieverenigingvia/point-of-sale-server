import logging

from app.domains.exact import exact_tasks
from app.tasks.recurring import (
    sepa,
    account,
    quantity,
    pending_card_transactions as pending,
)

_logger = logging.getLogger(__name__)
_logger.info("Registered recurring tasks in %s", sepa)
_logger.info("Registered recurring tasks in %s", exact_tasks)
_logger.info("Registered recurring tasks in %s", account)
_logger.info("Registered recurring tasks in %s", quantity)
_logger.info("Registered recurring tasks in %s", pending)
