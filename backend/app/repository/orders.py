from datetime import date
from pydantic import BaseModel
from sqlalchemy import Date, func, select
from sqlalchemy.orm import Session

from app.models import Order
from app.repository.util.filters import DateTimeFilter, LiteralEqualsFilter


def get_by_transaction_id(db_session: Session, transaction_id: int) -> list[Order]:
    return db_session.query(Order).filter(Order.transaction_id == transaction_id).all()


def save_multiple(
    db_session: Session, order_list: list[Order], do_commit: bool = True
) -> None:
    db_session.add_all(order_list)

    if do_commit:
        db_session.commit()


class DailyOrder(BaseModel):
    date: date
    amount: int
    count: int


def get_daily_orders_for_account(
    db_session: Session,
    date_filter: DateTimeFilter,
    account_filter: LiteralEqualsFilter,
) -> list[DailyOrder]:
    day_col = func.cast(Order.created_at, Date).label("date")

    q = (
        select(
            day_col,
            func.sum(Order.price).label("amount"),
            func.count(Order.id).label("count"),
        )
        .select_from(Order)
        .group_by(day_col)
        .order_by(day_col.asc())
    )

    q = date_filter.filter(q, day_col)
    q = account_filter.filter(q, Order.account_id)

    return [
        DailyOrder(date=date, amount=amount, count=count)
        for (date, amount, count) in db_session.execute(q).all()
    ]
