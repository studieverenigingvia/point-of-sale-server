from app.models.audit_trail import AuditTrail, AuditTrailData
from app.repository.util.pagination import (
    PageParameters,
    PaginatedSearch,
    PaginatedResponse,
    SearchParameters,
    SortParameters,
)
from sqlalchemy.orm.session import Session

from app.repository.util.filters import DateTimeFilter, LiteralEqualsFilter


def audit_get_logs(
    db_session: Session,
    page_params: PageParameters,
    search_params: SearchParameters,
    sort_params: SortParameters,
    id_filter: LiteralEqualsFilter = None,
    created_at_filter: DateTimeFilter = None,
    user_filter: LiteralEqualsFilter = None,
) -> PaginatedResponse[AuditTrailData]:
    #  For now, we filter out account checkouts because it makes audit trials not
    #  very useful.
    query = db_session.query(AuditTrail).filter(
        AuditTrail.event != "transaction.checkout.account"
    )

    return (
        PaginatedSearch[AuditTrail, AuditTrailData](query, page_params)
        .enable_searching(search_params, [AuditTrail.event])
        .enable_sorting(
            sort_params,
            sorting_column_definition={
                "event": AuditTrail.event,
                "createdAt": AuditTrail.created_at,
            },
        )
        .enable_filtering(
            {
                AuditTrail.id: id_filter,
                AuditTrail.created_at: created_at_filter,
                AuditTrail.user_id: user_filter,
            }
        )
        .execute()
    )
