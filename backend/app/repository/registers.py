from sqlalchemy import func, select, true
from sqlalchemy.orm import Session, joinedload

from app.models import Register, User
from app.models.register import RegisterCategory, RegisterData
from app.repository.util.filters import (
    BooleanFilter,
    ForeignKeyFilter,
    LiteralEqualsFilter,
    NullFilter,
)
from app.repository.util.pagination import (
    PageParameters,
    PaginatedResponse,
    PaginatedSearch,
    SearchParameters,
    SortParameters,
)


def paginated_search(
    db_session: Session,
    page_params: PageParameters,
    search_params: SearchParameters,
    sort_params: SortParameters,
    id_filter: LiteralEqualsFilter = None,
    category_filter: ForeignKeyFilter = None,
    default_filter: BooleanFilter = None,
    deleted_filter: NullFilter = None,
) -> PaginatedResponse[RegisterData]:
    q = db_session.query(Register).options(joinedload(Register.categories))
    return (
        PaginatedSearch[Register, RegisterData](q, page_params)
        .enable_searching(search_params, [Register.name])
        .enable_sorting(
            sort_params,
            sorting_column_definition={
                "name": func.lower(Register.name),
                "default": Register.default,
            },
        )
        .enable_filtering(
            {
                Register.id: id_filter,
                RegisterCategory.category_id: category_filter,
                Register.default: default_filter,
                Register.deleted_at: deleted_filter,
            }
        )
        .execute()
    )


def get_by_id(db_session: Session, register_id: int) -> Register:
    return db_session.query(Register).filter_by(id=register_id).one()


def create(db_session: Session, register: Register) -> Register:
    db_session.add(register)
    db_session.flush()
    return register


def update(db_session: Session, register: Register) -> Register:
    db_session.add(register)
    db_session.flush()
    return register


def get_default(db_session: Session) -> Register | None:
    register = db_session.query(Register).filter_by(default=True).one_or_none()
    if not register:
        register = db_session.query(Register).order_by(Register.id).first()
    return register


def set_default_register(db_session: Session, register: Register) -> Register:
    old_default = get_default(db_session)
    if old_default is not None:
        old_default.default = False
        db_session.add(old_default)

    register.default = True
    db_session.add(register)
    db_session.commit()

    return register


def is_any_user_using_register(db_session: Session, register: Register) -> bool:
    stmt = select(true).where(User.register_id == register.id).limit(1)
    # Cast to bool as `None` will become False
    return bool(db_session.scalar(stmt))
