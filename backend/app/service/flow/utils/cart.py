import pydantic


MAX_CHECKOUT_AMOUNT = 50
MAX_CHECKOUT_AMOUNT_ADMIN = 99999


class CartItemAmountStr(pydantic.ConstrainedInt):
    gt = 0
    lt = MAX_CHECKOUT_AMOUNT_ADMIN


class CartItem(pydantic.BaseModel):
    id: int
    amount: CartItemAmountStr
