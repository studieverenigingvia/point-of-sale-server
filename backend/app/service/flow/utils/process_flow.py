from abc import ABC, abstractmethod
from typing import NamedTuple

from sqlalchemy.orm import Session

from app.dependencies import RequestContext
from app.models import Register, Order, Account, Product
from app.models.card_terminal import CardTerminal
from app.models.checkout import CheckoutMethod
from app.models.order import OrderRegister, OrderFlowType
from app.models.transaction import Transaction, TransactionStatus
from app.repository import accounts as account_repo
from app.repository import products
from app.service import order_service, audit_service, account_service
from app.service.flow.utils.cart import CartItem
from app.views.exceptions import (
    ValidationException,
    AuthorizationException,
    NotFoundException,
    UnprocessableEntityException,
)


class CreateOrdersResult(NamedTuple):
    total: int
    orders: list[Order]


class ProcessFlowBaseData(ABC):
    @abstractmethod
    def create_orders(
        self, context: RequestContext, transaction: Transaction, method: CheckoutMethod
    ) -> CreateOrdersResult:
        pass


class ProcessFlowBaseRequest(ABC):
    @abstractmethod
    def validate(self, db_session: Session) -> ProcessFlowBaseData:
        pass


class ProcessFlowCardTerminalData(ProcessFlowBaseData):
    terminal: CardTerminal

    def __init__(self, card_terminal: CardTerminal) -> None:
        self.terminal = card_terminal

    def create_orders(
        self, context: RequestContext, transaction: Transaction, method: CheckoutMethod
    ) -> CreateOrdersResult:
        pass


class ProcessFlowCardTerminalRequest:
    register: Register

    def __init__(self, register: Register) -> None:
        self.register = register

    def validate(self, db_session: Session) -> ProcessFlowCardTerminalData | None:
        card_terminal = self.register.card_terminal
        if card_terminal is None:
            return None

        return ProcessFlowCardTerminalData(card_terminal)


class ProcessFlowUpgradeData(ProcessFlowBaseData):
    account: Account
    upgrade_amount: int
    sepa_batch_id: int | None = None

    def __init__(
        self,
        upgrade_amount: int,
        account: Account,
        sepa_batch_id: int | None = None,
    ) -> None:
        self.upgrade_amount = upgrade_amount
        self.sepa_batch_id = sepa_batch_id
        self.account = account

    def create_orders(
        self, context: RequestContext, transaction: Transaction, method: CheckoutMethod
    ) -> CreateOrdersResult:
        """
        This creates the order for the upgrade.
        KEEP IN MIND: This does not update the account balance.
        """
        audit_service.audit_create(
            context,
            "transaction.upgrade." + method.name.lower(),
            object_id=transaction.id,
        )

        flow_type = (
            OrderFlowType.NONE_TO_PENDING
            if transaction.status == TransactionStatus.PENDING
            else OrderFlowType.NONE_TO_COMPLETED
        )

        register = (
            OrderRegister.PENDING
            if transaction.status == TransactionStatus.PENDING
            else OrderRegister.DEFAULT
        )

        order = Order(
            account_id=self.account.id,
            transaction_id=transaction.id,
            price=-self.upgrade_amount,
            register=register,
            flow_type=flow_type,
        )
        order.method = CheckoutMethod.ACCOUNT

        if self.sepa_batch_id is not None:
            order.sepa_batch_id = self.sepa_batch_id

        return CreateOrdersResult(self.upgrade_amount, [order])


class ProcessFlowUpgradeRequest(ProcessFlowBaseRequest):
    upgrade_account_id: int
    upgrade_amount: int
    # TODO: InitProcessFlowRequest also has a reason. Investigate if this is needed
    reason: str | None
    sepa_batch_id: int | None = None

    def __init__(
        self,
        upgrade_account_id: int,
        upgrade_amount: int,
        reason: str | None,
        sepa_batch_id: int | None = None,
    ) -> None:
        self.upgrade_account_id = upgrade_account_id
        self.upgrade_amount = upgrade_amount
        self.reason = reason
        self.sepa_batch_id = sepa_batch_id

    def validate(self, db_session: Session) -> ProcessFlowUpgradeData:
        if self.upgrade_amount == 0:
            raise ValidationException("invalid_upgrade_amount")

        if self.upgrade_amount < 0 and not self.reason:
            raise ValidationException("reason_expected")

        acc = account_repo.get_by_id(db_session, self.upgrade_account_id)

        if acc is None or acc.deleted:
            raise NotFoundException("account_not_found")

        return ProcessFlowUpgradeData(
            upgrade_amount=self.upgrade_amount,
            account=acc,
            sepa_batch_id=self.sepa_batch_id,
        )


class ProcessFlowPurchaseData(ProcessFlowBaseData):
    cart: dict[Product, int]

    def __init__(self, cart: dict[Product, int]) -> None:
        self.cart = cart

    def create_orders(
        self, context: RequestContext, transaction: Transaction, method: CheckoutMethod
    ) -> CreateOrdersResult:
        total = 0
        orders: list[Order] = []

        flow_type = (
            OrderFlowType.NONE_TO_PENDING
            if transaction.status == TransactionStatus.PENDING
            else OrderFlowType.NONE_TO_COMPLETED
        )

        register = (
            OrderRegister.PENDING
            if transaction.status == TransactionStatus.PENDING
            else OrderRegister.DEFAULT
        )

        for product, amount in self.cart.items():
            total += product.price * amount
            for _ in range(amount):
                order = order_service.create_order(
                    product, transaction.id, method, flow_type, register
                )
                orders.append(order)

        return CreateOrdersResult(total, orders)


class ProcessFlowPurchaseRequest(ProcessFlowBaseRequest):
    cart_items: list[CartItem]
    register: Register
    max_checkout_amount: int

    def __init__(
        self, cart_items: list[CartItem], register: Register, max_checkout_amount: int
    ) -> None:
        self.cart_items = cart_items
        self.register = register
        self.max_checkout_amount = max_checkout_amount

    def validate(self, db_session: Session) -> ProcessFlowPurchaseData:
        register_cat_ids = [c.id for c in self.register.categories]
        all_products: dict[int, Product] = products.get_dict_from_ids(
            db_session, [c.id for c in self.cart_items]
        )
        result: dict[Product, int] = {}

        for cart_item in self.cart_items:
            if cart_item.amount > self.max_checkout_amount:
                raise ValidationException("invalid_product_in_cart")

            product = all_products.get(cart_item.id)
            if product is None or product.deleted:
                raise NotFoundException("product_deleted")

            # Check if the product's category is part of the register
            product_cat_ids = [c.id for c in product.categories]
            if not set(product_cat_ids) & set(register_cat_ids):
                raise AuthorizationException("invalid_product_category")

            result[product] = cart_item.amount

        return ProcessFlowPurchaseData(result)


class ProcessFlowAccountSourceData(ProcessFlowBaseData):
    account: Account

    def __init__(self, account: Account) -> None:
        self.account = account

    def create_orders(
        self, context: RequestContext, transaction: Transaction, method: CheckoutMethod
    ) -> CreateOrdersResult:
        pass


class ProcessFlowAccountSource(ProcessFlowBaseRequest):
    account_id: int
    check_pin: bool = False
    pin_code: str | None = None

    def __init__(self, account_id: int, check_pin: bool, pin_code: str | None = None):
        self.account_id = account_id
        self.check_pin = check_pin
        self.pin_code = pin_code

    account: Account | None = None

    def validate(self, db_session: Session) -> ProcessFlowAccountSourceData:
        self.account = account_repo.get_by_id(db_session, self.account_id)

        if not self.account or self.account.deleted:
            raise NotFoundException("invalid_account")

        # Check the PIN code if the user can't check out on other accounts without
        # authorization.
        if self.check_pin and (
            not self.pin_code
            or not account_service.check_pin(self.account, self.pin_code)
        ):
            raise UnprocessableEntityException("invalid_account_pin")

        return ProcessFlowAccountSourceData(self.account)
