from app.dependencies import RequestContext
from app.service import account_service
from app.service.flow.flow_types import StateTransitionOutput


def process_account_balance_after_transaction(
    context: RequestContext, input: StateTransitionOutput
) -> StateTransitionOutput:
    balance_result = account_service.update_account_balances_from_orders(
        context.db_session, input.new_orders
    )
    return StateTransitionOutput(
        input.transaction, input.new_orders, balance_result, input.terminal
    )
