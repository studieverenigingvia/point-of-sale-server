from app.dependencies import RequestContext
from app.service.flow.flow_types import StateTransitionOutput
from app.service.product_service import mass_update_quantity


def process_quantity_after_transaction(
    context: RequestContext, input: StateTransitionOutput
) -> None:
    orders_added = input.new_orders

    contains_deleted = any(order.deleted for order in orders_added)

    mass_update_quantity(context, orders_added, 1 if contains_deleted else -1)
