from app.models.checkout import CheckoutMethod
from app.models.transaction import TransactionStatus, TransactionFlowType
from app.service.flow.flow_types import StateTransition, FlowInputTypes
from app.service.flow.flows.account import account_payment_none_to_completed
from app.service.flow.flows.card import (
    card_payment_none_to_pending,
    card_payment_pending_to_completed,
    card_payment_completed_to_deleted,
    card_payment_pending_to_expired,
    card_payment_pending_to_cancelled,
    card_payment_pending_to_deleted,
)
from app.service.flow.flows.generic import (
    generic_payment_none_to_completed,
    generic_payment_completed_to_deleted,
)


def generate_generic_transition(
    method: CheckoutMethod, allowed_inputs: list[FlowInputTypes]
) -> list[StateTransition]:
    return [
        {
            "current": None,
            "next": TransactionStatus.COMPLETED,
            "inputs": allowed_inputs,
            "action": lambda ctx, input: generic_payment_none_to_completed(
                ctx, input, method
            ),
        },
        {
            "current": TransactionStatus.COMPLETED,
            "next": TransactionStatus.DELETED,
            "inputs": [],
            "action": generic_payment_completed_to_deleted,
        },
    ]


STATE_TRANSITIONS: dict[TransactionFlowType, list[StateTransition]] = {
    TransactionFlowType.CASH_PAYMENT: generate_generic_transition(
        CheckoutMethod.CASH, [FlowInputTypes.UPGRADE, FlowInputTypes.ITEM_PURCHASE]
    ),
    TransactionFlowType.OTHER_PAYMENT: generate_generic_transition(
        CheckoutMethod.OTHER, [FlowInputTypes.ITEM_PURCHASE]
    ),
    TransactionFlowType.OTHER_ADMIN_PAYMENT: generate_generic_transition(
        CheckoutMethod.OTHER, [FlowInputTypes.UPGRADE]
    ),
    TransactionFlowType.TRANSFER_PAYMENT: generate_generic_transition(
        CheckoutMethod.TRANSFER, [FlowInputTypes.UPGRADE]
    ),
    TransactionFlowType.SEPA_PAYMENT: generate_generic_transition(
        CheckoutMethod.SEPA, [FlowInputTypes.UPGRADE]
    ),
    TransactionFlowType.IDEAL_PAYMENT: generate_generic_transition(
        CheckoutMethod.IDEAL, [FlowInputTypes.UPGRADE]
    ),
    TransactionFlowType.ACCOUNT_PAYMENT: [
        {
            "current": None,
            "next": TransactionStatus.COMPLETED,
            "inputs": [
                FlowInputTypes.ITEM_PURCHASE,
                FlowInputTypes.ACCOUNT_SOURCE,
            ],
            "action": account_payment_none_to_completed,
        },
        {
            "current": TransactionStatus.COMPLETED,
            "next": TransactionStatus.DELETED,
            "inputs": [],
            "action": generic_payment_completed_to_deleted,
        },
    ],
    TransactionFlowType.CARD_PAYMENT: [
        {
            "current": None,
            "next": TransactionStatus.PENDING,
            "inputs": [
                FlowInputTypes.ITEM_PURCHASE,
                FlowInputTypes.UPGRADE,
                FlowInputTypes.CARD_TERMINAL,
            ],
            "action": card_payment_none_to_pending,
        },
        {
            "current": TransactionStatus.PENDING,
            "next": TransactionStatus.COMPLETED,
            "inputs": [],
            "action": card_payment_pending_to_completed,
        },
        {
            "current": TransactionStatus.PENDING,
            "next": TransactionStatus.CANCELLED,
            "inputs": [],
            "action": card_payment_pending_to_cancelled,
        },
        {
            "current": TransactionStatus.PENDING,
            "next": TransactionStatus.EXPIRED,
            "inputs": [],
            "action": card_payment_pending_to_expired,
        },
        {
            "current": TransactionStatus.PENDING,
            "next": TransactionStatus.DELETED,
            "inputs": [],
            "action": card_payment_pending_to_deleted,
        },
        {
            "current": TransactionStatus.COMPLETED,
            "next": TransactionStatus.DELETED,
            "inputs": [],
            "action": card_payment_completed_to_deleted,
        },
    ],
    TransactionFlowType.CARD_ADMIN_PAYMENT: generate_generic_transition(
        CheckoutMethod.CARD, [FlowInputTypes.UPGRADE]
    ),
}
