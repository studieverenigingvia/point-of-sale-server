from app.dependencies import RequestContext
from app.models.transaction import (
    TransactionStatus,
    TransactionFlowType,
)
from app.repository import transactions as transaction_repo
from app.service import transaction_service, audit_service
from app.service.flow.flow_types import (
    checkout_method_to_flow_type,
    FlowInputTypes,
    StateTransitionInput,
    StateTransition,
    InitProcessFlowRequest,
    NextProcessFlowRequest,
    StateTransitionOutput,
)
from app.service.flow.state_transitions import STATE_TRANSITIONS
from app.service.flow.utils.process_balances import (
    process_account_balance_after_transaction,
)
from app.service.flow.utils.process_quantity import process_quantity_after_transaction
from app.views.exceptions import ValidationException


def get_possible_transitions_from_current_flow_type_and_state(
    flow_type: TransactionFlowType, current_state: TransactionStatus | None
) -> list[StateTransition]:
    transitions = STATE_TRANSITIONS[flow_type]

    return [
        transition
        for transition in transitions
        if transition["current"] == current_state
    ]


def get_possible_transitions_for_desired_state(
    transitions: list[StateTransition], desired_state: TransactionStatus
) -> list[StateTransition]:
    return [
        transition for transition in transitions if transition["next"] == desired_state
    ]


def process_flow_init(
    context: RequestContext,
    req: InitProcessFlowRequest,
    flow_type: TransactionFlowType | None = None,
    # TODO: Do we want to remove this? Only used in SEPA service, to do an entire sepa
    #  batch in 1 SQL transaction
    do_commit: bool = True,
) -> StateTransitionOutput:
    if flow_type is None:
        flow_type = checkout_method_to_flow_type(req["method"])

    possible_transitions = get_possible_transitions_from_current_flow_type_and_state(
        flow_type, None
    )

    possible_transitions = get_possible_transitions_for_desired_state(
        possible_transitions, req["desired_state"]
    )

    if len(possible_transitions) == 0:
        raise ValidationException("invalid_transaction_state")

    if len(possible_transitions) > 1:
        raise ValidationException("too_many_possible_states")

    transition = possible_transitions[0]
    action = transition["action"]

    if "upgrade_request" in req and req["upgrade_request"] is not None:
        if FlowInputTypes.UPGRADE not in transition["inputs"]:
            raise ValidationException("invalid_input")

    if "purchase_request" in req and req["purchase_request"] is not None:
        if FlowInputTypes.ITEM_PURCHASE not in transition["inputs"]:
            raise ValidationException("invalid_input")

    if "account_source" in req and req["account_source"] is not None:
        if FlowInputTypes.ACCOUNT_SOURCE not in transition["inputs"]:
            raise ValidationException("invalid_input")

    if "card_terminal" in req and req["card_terminal"] is not None:
        if FlowInputTypes.CARD_TERMINAL not in transition["inputs"]:
            raise ValidationException("invalid_input")

    new_transaction = transaction_repo.create_empty_transaction(
        context.db_session, req["nonce"], TransactionStatus.INITIALIZING, flow_type
    )

    state_transition_input = StateTransitionInput(
        new_transaction,
        purchase=req["purchase_request"].validate(context.db_session)
        if "purchase_request" in req and req["purchase_request"] is not None
        else None,
        upgrade=req["upgrade_request"].validate(context.db_session)
        if "upgrade_request" in req and req["upgrade_request"] is not None
        else None,
        account_source=req["account_source"].validate(context.db_session)
        if "account_source" in req and req["account_source"] is not None
        else None,
        card_terminal=req["card_terminal"].validate(context.db_session)
        if "card_terminal" in req and req["card_terminal"] is not None
        else None,
        reason=req["reason"],
    )

    result_from_action = action(context, state_transition_input)

    # Flush, so we can use the orders in the following queries
    context.db_session.flush()

    process_quantity_after_transaction(context, result_from_action)

    result_from_balance = process_account_balance_after_transaction(
        context, result_from_action
    )

    audit_service.audit_create(
        context,
        "transaction.init." + flow_type.name.lower(),
        object_id=new_transaction.id,
    )
    transaction_service.save(context, new_transaction, do_commit)

    return result_from_balance


def process_flow_next(
    context: RequestContext, req: NextProcessFlowRequest
) -> StateTransitionOutput:
    # `For_update` locks the row, so async tasks cannot run any other process_flow_next
    # on this transaction. Note that other transactions will simply wait on this line,
    # meaning that if the transaction status changes, there is a high chance that this
    # desired status is no longer possible. Meaning this will throw
    # `invalid_transaction_state`.
    transaction = transaction_service.get_by_id(
        context.db_session, req["transaction_id"], for_update=True
    )
    flow_type = transaction.flow

    possible_transitions = get_possible_transitions_from_current_flow_type_and_state(
        flow_type, transaction.status
    )
    possible_transitions = get_possible_transitions_for_desired_state(
        possible_transitions, req["desired_state"]
    )
    if len(possible_transitions) == 0:
        raise ValidationException("invalid_transaction_state")

    if len(possible_transitions) > 1:
        raise ValidationException("too_many_possible_states")

    transition = possible_transitions[0]
    action = transition["action"]

    state_transition_input = StateTransitionInput(transaction)

    result_from_action = action(context, state_transition_input)

    # Flush, so we can use the orders in the following queries
    context.db_session.flush()

    process_quantity_after_transaction(context, result_from_action)

    result_from_balance = process_account_balance_after_transaction(
        context, result_from_action
    )

    audit_service.audit_create(
        context,
        "transaction.next." + flow_type.name.lower(),
        object_id=transaction.id,
    )

    context.db_session.add(transaction)
    context.db_session.commit()

    return result_from_balance
