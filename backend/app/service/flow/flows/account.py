from app.dependencies import RequestContext
from app.models import Order
from app.models.checkout import CheckoutMethod
from app.models.order import OrderRegister, OrderFlowType
from app.models.transaction import TransactionStatus
from app.service import order_service
from app.service.flow.flow_types import (
    StateTransitionInput,
    StateTransitionOutput,
)
from app.views.exceptions import (
    ValidationException,
    NotAcceptableException,
)


def account_payment_none_to_completed(
    context: RequestContext, input: StateTransitionInput
) -> StateTransitionOutput:
    if input.account_source is None:
        raise ValidationException("no_account_source")

    account = input.account_source.account
    if account is None:
        raise ValidationException("no_account")

    total = 0
    orders: list[Order] = []

    if input.purchase:
        result = input.purchase.create_orders(
            context, input.transaction, CheckoutMethod.ACCOUNT
        )
        total += result.total
        orders += result.orders

    if len(orders) == 0:
        raise ValidationException("no_orders")

    if not account.allow_negative_balance and total > account.balance:
        raise NotAcceptableException("insufficient_credits")

    compensation = Order(
        account_id=input.account_source.account.id,
        transaction_id=input.transaction.id,
        price=total,
        register=OrderRegister.DEFAULT,
        flow_type=OrderFlowType.NONE_TO_COMPLETED,
    )
    compensation.method = CheckoutMethod.ACCOUNT
    orders.append(compensation)

    order_service.save_multiple(context, orders, do_commit=False)

    input.transaction.status = TransactionStatus.COMPLETED

    return StateTransitionOutput(input.transaction, orders)
