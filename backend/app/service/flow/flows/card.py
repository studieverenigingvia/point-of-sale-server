from app.dependencies import RequestContext
from app.models import Order
from app.models.checkout import CheckoutMethod
from app.models.order import OrderFlowType, OrderRegister
from app.models.transaction import TransactionStatus
from app.service import order_service, audit_service, card_terminal_service
from app.service.account_service import (
    get_account_for_checkout_method,
)
from app.service.flow.flow_types import (
    StateTransitionInput,
    StateTransitionOutput,
)
from app.views.exceptions import ValidationException


def card_payment_none_to_pending(
    context: RequestContext, input: StateTransitionInput
) -> StateTransitionOutput:
    input.transaction.reason = input.reason
    input.transaction.status = TransactionStatus.PENDING

    total = 0
    orders: list[Order] = []

    if input.purchase:
        result = input.purchase.create_orders(
            context, input.transaction, CheckoutMethod.CARD
        )
        total += result.total
        orders += result.orders

    if input.upgrade:
        result = input.upgrade.create_orders(
            context, input.transaction, CheckoutMethod.CARD
        )
        total += result.total
        orders += result.orders

    for order in orders:
        order.flow_type = OrderFlowType.NONE_TO_PENDING
        order.register = OrderRegister.PENDING

    compensation = Order(
        account_id=get_account_for_checkout_method(
            context.db_session, CheckoutMethod.CARD
        ),
        transaction_id=input.transaction.id,
        price=total,
        flow_type=OrderFlowType.NONE_TO_PENDING,
        register=OrderRegister.PENDING,
    )
    compensation.method = CheckoutMethod.CARD

    orders.append(compensation)
    order_service.save_multiple(context, orders, do_commit=False)

    card_terminal_result = None

    if input.card_terminal:
        (
            card_terminal_result,
            card_transaction,
        ) = card_terminal_service.start_card_transaction(
            input.card_terminal, input.transaction.id, total
        )

        context.db_session.add(card_transaction)

    return StateTransitionOutput(
        input.transaction,
        orders,
        terminal=card_terminal_result,
    )


def card_payment_pending_to_completed(
    context: RequestContext, input: StateTransitionInput
) -> StateTransitionOutput:
    new_orders = []

    for order in input.transaction.orders:
        assert order.flow_type == OrderFlowType.NONE_TO_PENDING
        assert order.register == OrderRegister.PENDING

        compensation_to_pending = Order(
            account_id=order.account_id,
            transaction_id=input.transaction.id,
            price=order.price,
            flow_type=OrderFlowType.PENDING_TO_COMPLETED,
            register=OrderRegister.DEFAULT,
            product_id=order.product_id,
        )
        compensation_to_pending.method = CheckoutMethod.CARD
        new_orders.append(compensation_to_pending)

        compensation_from_pending = Order(
            account_id=order.account_id,
            transaction_id=input.transaction.id,
            price=-order.price,
            flow_type=OrderFlowType.PENDING_TO_COMPLETED,
            register=OrderRegister.PENDING,
        )
        compensation_from_pending.method = CheckoutMethod.CARD

        new_orders.append(compensation_from_pending)

    input.transaction.orders.extend(new_orders)
    input.transaction.status = TransactionStatus.COMPLETED

    return StateTransitionOutput(input.transaction, new_orders)


def card_payment_completed_to_deleted(
    context: RequestContext, input: StateTransitionInput
) -> StateTransitionOutput:
    # TODO: Double keeping track of deleted
    input.transaction.deleted = True
    input.transaction.status = TransactionStatus.DELETED

    audit_service.audit_create(
        context,
        "transaction.delete",
        object_id=input.transaction.id,
    )

    orders_to_delete = []
    total_transfer_to_default_register = 0
    for o in input.transaction.orders:
        if o.register == OrderRegister.DEFAULT:
            total_transfer_to_default_register += o.price

            orders_to_delete.append(o)

    if total_transfer_to_default_register != 0:
        raise ValidationException("pending_orders_did_not_sum_to_0")

    new_orders = []
    for order in orders_to_delete:
        if order.deleted:
            # This IS impossible, we can never enter this flow with a deleted order
            raise ValidationException("order_is_deletion_order")

        new_orders.append(
            order_service.create_reverse_order(
                order, OrderFlowType.COMPLETED_TO_DELETED
            )
        )

    input.transaction.orders.extend(new_orders)

    return StateTransitionOutput(
        input.transaction,
        new_orders,
    )


def card_payment_pending_to_expired(
    context: RequestContext, input: StateTransitionInput
) -> StateTransitionOutput:
    # TODO: Double keeping track of deleted
    input.transaction.deleted = True
    input.transaction.status = TransactionStatus.EXPIRED

    audit_service.audit_create(
        context,
        "transaction.expire",
        object_id=input.transaction.id,
    )

    new_orders = []
    for order in input.transaction.orders:
        if order.deleted:
            # This IS impossible, we can never enter this flow with a deleted order
            raise ValidationException("order_is_deletion_order")

        new_orders.append(
            order_service.create_reverse_order(order, OrderFlowType.PENDING_TO_EXPIRED)
        )

    input.transaction.orders.extend(new_orders)

    return StateTransitionOutput(
        input.transaction,
        new_orders,
    )


def card_payment_pending_to_cancelled(
    context: RequestContext, input: StateTransitionInput
) -> StateTransitionOutput:
    input.transaction.deleted = True
    input.transaction.status = TransactionStatus.CANCELLED

    audit_service.audit_create(
        context,
        "transaction.cancel",
        object_id=input.transaction.id,
    )

    new_orders = []
    for order in input.transaction.orders:
        if order.deleted:
            # This IS impossible, we can never enter this flow with a deleted order
            raise ValidationException("order_is_deletion_order")

        new_orders.append(
            order_service.create_reverse_order(
                order, OrderFlowType.PENDING_TO_CANCELLED
            )
        )

    input.transaction.orders.extend(new_orders)

    return StateTransitionOutput(
        input.transaction,
        new_orders,
    )


def card_payment_pending_to_deleted(
    context: RequestContext, input: StateTransitionInput
) -> StateTransitionOutput:
    input.transaction.deleted = True
    input.transaction.status = TransactionStatus.DELETED

    audit_service.audit_create(
        context,
        "transaction.delete",
        object_id=input.transaction.id,
    )

    new_orders = []
    for order in input.transaction.orders:
        if order.deleted:
            # This IS impossible, we can never enter this flow with a deleted order
            raise ValidationException("order_is_deletion_order")

        new_orders.append(
            order_service.create_reverse_order(order, OrderFlowType.PENDING_TO_DELETED)
        )

    input.transaction.orders.extend(new_orders)

    return StateTransitionOutput(
        input.transaction,
        new_orders,
    )
