from app.dependencies import RequestContext
from app.models import Order
from app.models.checkout import CheckoutMethod
from app.models.order import OrderRegister, OrderFlowType
from app.models.transaction import TransactionStatus
from app.service import order_service, audit_service
from app.service.account_service import get_account_for_checkout_method
from app.service.flow.flow_types import (
    StateTransitionInput,
    StateTransitionOutput,
)
from app.views.exceptions import ValidationException


def generic_payment_none_to_completed(
    context: RequestContext, input: StateTransitionInput, method: CheckoutMethod
) -> StateTransitionOutput:
    if method in (CheckoutMethod.OTHER, CheckoutMethod.TRANSFER) and not input.reason:
        raise ValidationException("reason_expected")

    input.transaction.reason = input.reason

    total = 0
    new_orders: list[Order] = []

    sepa_batch_id: int | None = None

    if input.purchase:
        result = input.purchase.create_orders(context, input.transaction, method)
        total += result.total
        new_orders += result.orders

    if input.upgrade:
        result = input.upgrade.create_orders(context, input.transaction, method)
        total += result.total
        new_orders += result.orders

        if input.upgrade.sepa_batch_id:
            sepa_batch_id = input.upgrade.sepa_batch_id

    compensation = Order(
        account_id=get_account_for_checkout_method(context.db_session, method),
        transaction_id=input.transaction.id,
        price=total,
        register=OrderRegister.DEFAULT,
        flow_type=OrderFlowType.NONE_TO_COMPLETED,
    )
    compensation.method = method

    new_orders.append(compensation)

    if sepa_batch_id:
        for order in new_orders:
            order.sepa_batch_id = sepa_batch_id

    input.transaction.status = TransactionStatus.COMPLETED
    order_service.save_multiple(context, new_orders, do_commit=False)

    return StateTransitionOutput(
        input.transaction,
        new_orders,
    )


def generic_payment_completed_to_deleted(
    context: RequestContext, input: StateTransitionInput
) -> StateTransitionOutput:
    # TODO: Double keeping track of deleted
    input.transaction.deleted = True
    input.transaction.status = TransactionStatus.DELETED

    audit_service.audit_create(
        context,
        "transaction.delete",
        object_id=input.transaction.id,
    )

    new_orders = []
    for order in input.transaction.orders:
        if order.deleted:
            # This IS impossible, we can never enter this flow with a deleted order
            raise ValidationException("order_is_deletion_order")

        new_orders.append(
            order_service.create_reverse_order(
                order, OrderFlowType.COMPLETED_TO_DELETED
            )
        )

    input.transaction.orders.extend(new_orders)

    return StateTransitionOutput(
        input.transaction,
        new_orders,
    )
