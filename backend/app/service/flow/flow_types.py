import enum
from typing import TypedDict, Callable

from app.dependencies import RequestContext
from app.models import Order
from app.models.card_terminal import CardTerminal
from app.models.checkout import CheckoutMethod
from app.models.transaction import TransactionStatus, Transaction, TransactionFlowType
from app.service.flow.utils.process_flow import (
    ProcessFlowUpgradeRequest,
    ProcessFlowPurchaseRequest,
    ProcessFlowAccountSource,
    ProcessFlowAccountSourceData,
    ProcessFlowPurchaseData,
    ProcessFlowUpgradeData,
    ProcessFlowCardTerminalData,
    ProcessFlowCardTerminalRequest,
)


def checkout_method_to_flow_type(method: CheckoutMethod) -> TransactionFlowType:
    if method == CheckoutMethod.CARD:
        return TransactionFlowType.CARD_PAYMENT
    elif method == CheckoutMethod.CASH:
        return TransactionFlowType.CASH_PAYMENT
    elif method == CheckoutMethod.SEPA:
        return TransactionFlowType.SEPA_PAYMENT
    elif method == CheckoutMethod.IDEAL:
        return TransactionFlowType.IDEAL_PAYMENT
    elif method == CheckoutMethod.ACCOUNT:
        return TransactionFlowType.ACCOUNT_PAYMENT
    elif method == CheckoutMethod.TRANSFER:
        return TransactionFlowType.TRANSFER_PAYMENT
    elif method == CheckoutMethod.OTHER:
        return TransactionFlowType.OTHER_PAYMENT

    raise Exception("Invalid checkout method")


class FlowInputTypes(enum.Enum):
    UPGRADE = 1
    ITEM_PURCHASE = 2
    ACCOUNT_SOURCE = 3
    CARD_TERMINAL = 4


class StateTransitionInput:
    # This is the current object in the database. If we are transitioning from none ->
    # *something* it will be a brand new transaction object.
    # If we are deleting a transaction, this will contain the orders that have been
    # made previously. The logic in a state transition function will use the existing
    # orders (and e.g. check what accounts/account types) are associated with them to
    # determine what new orders should be created.
    transaction: Transaction
    reason: str | None

    account_source: ProcessFlowAccountSourceData | None
    purchase: ProcessFlowPurchaseData | None
    upgrade: ProcessFlowUpgradeData | None
    card_terminal: ProcessFlowCardTerminalData | None

    def __init__(
        self,
        transaction: Transaction,
        account_source: ProcessFlowAccountSourceData | None = None,
        purchase: ProcessFlowPurchaseData | None = None,
        upgrade: ProcessFlowUpgradeData | None = None,
        card_terminal: ProcessFlowCardTerminalData | None = None,
        reason: str | None = None,
    ) -> None:
        self.transaction = transaction
        self.account_source = account_source
        self.purchase = purchase
        self.upgrade = upgrade
        self.card_terminal = card_terminal
        self.reason = reason


class BalanceResult:
    before: int
    after: int

    def __init__(self, before: int, after: int):
        self.before = before
        self.after = after


class StateTransitionOutput:
    transaction: Transaction
    new_orders: list[Order]
    balance: BalanceResult | None
    terminal: CardTerminal | None

    def __init__(
        self,
        transaction: Transaction,
        orders: list[Order],
        balance: BalanceResult | None = None,
        terminal: CardTerminal | None = None,
    ):
        self.transaction = transaction
        self.new_orders = orders
        self.balance = balance
        self.terminal = terminal


class StateTransition(TypedDict):
    current: TransactionStatus | None
    next: TransactionStatus
    inputs: list[FlowInputTypes]
    action: Callable[[RequestContext, StateTransitionInput], StateTransitionOutput]


class InitProcessFlowRequest(TypedDict):
    nonce: str
    desired_state: TransactionStatus

    method: CheckoutMethod

    purchase_request: ProcessFlowPurchaseRequest | None
    upgrade_request: ProcessFlowUpgradeRequest | None
    account_source: ProcessFlowAccountSource | None
    card_terminal: ProcessFlowCardTerminalRequest | None

    reason: str | None


class NextProcessFlowRequest(TypedDict):
    desired_state: TransactionStatus
    transaction_id: int
