import logging

import pydantic
from celery.exceptions import CeleryError
from sqlalchemy.orm import Session

from app import worker
from app.repository import health_repo

_logger = logging.getLogger(__name__)


def get_worker_count() -> int:
    try:
        workers = worker.control.inspect().active()
        if workers:
            return len(workers)
    except CeleryError as e:
        _logger.error("Celery health check failed: %s", e)
    return 0


class SystemHealth(pydantic.BaseModel):
    healthy: bool
    database: bool
    workers: int


def get_system_health(db_session: Session) -> SystemHealth:
    db_status = health_repo.get_database_status(db_session)
    workers = get_worker_count()
    return SystemHealth(
        healthy=all((db_status, workers)), database=db_status, workers=workers
    )
