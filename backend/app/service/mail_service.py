import base64
import email.utils
import logging
import mimetypes
import uuid
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from typing import Any

from google.oauth2 import service_account
from googleapiclient import errors
from googleapiclient.discovery import build, Resource
from jinja2 import FileSystemLoader, Environment, StrictUndefined
from sqlalchemy.orm import Session

import config
from app.service import setting_service
from app.views.exceptions import ValidationException, ServiceUnavailableException

_logger = logging.getLogger(__name__)


class MailCommand:
    def __init__(self, db_session: Session, to: str) -> None:
        self.to: str = to
        self.subject: str = ""
        self.sender = email.utils.formataddr(
            (
                "Point of Sale",
                setting_service.get_db_settings(db_session).notification_sender,
            )
        )
        self.html: str = ""

        # Dict with the name to a filepath.
        self.inline_attachments: dict[str, str] = {}

        # List of tuples with the name and filepath.
        self.added_attachments: list[tuple[str, str]] = []

        # List of tuples with the sid id and filepath.
        self.added_inline_attachments: list[tuple[str, str]] = []

        self._init()

    def _init(self) -> None:
        # Insert default assets.
        self.with_inline_attachment(
            "via_logo", "app/templates/email/attachments/via_logo_with_mark.png"
        )

    def with_template(self, template_name: str, **kwargs: Any) -> None:
        context = dict(
            {
                "get_cid": self._template_get_cid,
            },
            **kwargs,
        )

        env = Environment(
            loader=FileSystemLoader("app/templates"),
            # extensions = [SkipBlockExtension],
            cache_size=0,  # disable cache because we do 2 get_template()
            undefined=StrictUndefined,
        )

        template = env.get_template(template_name)

        self.subject = "".join(template.blocks["title"](template.new_context(context)))
        self.html = template.render(context)

    def _template_get_cid(self, name: str) -> str:
        """
        Get an cid to inline in the email html, and register it to be send
        along with the email as an attachment.
        """

        if name not in self.inline_attachments:
            raise Exception(
                "Could not find an attachment with the "
                "name '{}', call with_inline_attachment('{}', "
                "filepath) first.".format(name, name)
            )

        generated_id = str(uuid.uuid4())
        self.added_inline_attachments.append(
            (generated_id, self.inline_attachments[name])
        )

        return generated_id

    def with_attachment(self, name: str, filepath: str) -> None:
        self.added_attachments.append((name, filepath))

    def with_inline_attachment(self, name: str, filepath: str) -> None:
        self.inline_attachments[name] = filepath

    def validate(self) -> None:
        if not self.to:
            raise ValidationException("invalid_recipient_address")

        if not self.sender:
            raise ValidationException("invalid_sender_address")

        if not self.subject:
            raise ValidationException("invalid_subject")

        if not self.html:
            raise ValidationException("no_content")


def build_service(service_type: str, api_version: str, scope: str) -> Resource | None:
    try:
        # https://developers.google.com/identity/protocols/OAuth2ServiceAccount
        credentials = service_account.Credentials.from_service_account_file(
            filename=config.GOOGLE_API_JSON_KEY,
            scopes=[scope],
        ).with_subject(config.GSUITE_TREASURER_MAIL)

        return build(
            service_type, api_version, credentials=credentials, cache_discovery=False
        )
    except Exception as e:
        if not isinstance(e, FileNotFoundError):
            _logger.exception(e)
        return None


def build_gmail_service() -> Resource | None:
    return build_service("gmail", "v1", "https://www.googleapis.com/auth/gmail.send")


def get_treasurer_aliases() -> list[str]:
    service = build_service(
        "admin",
        "directory_v1",
        "https://www.googleapis.com/auth/admin.directory.user.alias.readonly",
    )

    gsuite_mail = getattr(config, "GSUITE_TREASURER_MAIL", None)

    if not service:
        if gsuite_mail:
            return [gsuite_mail]
        return []

    try:
        resp = (
            service.users()
            .aliases()
            .list(userKey=config.GSUITE_TREASURER_MAIL)
            .execute()
        )
        return [alias.get("alias") for alias in resp.get("aliases")] + [
            config.GSUITE_TREASURER_MAIL
        ]
    except errors.HttpError as e:
        _logger.exception(e)
        raise ServiceUnavailableException("google_error") from e


def send_email(command: MailCommand) -> Any | None:
    command.validate()

    # https://developers.google.com/gmail/api/guides/sending
    service = build_gmail_service()
    if not service:
        _logger.warning(
            "No google credentials available, did not send %s", command.subject
        )
        debug_html = (
            f"<p>DEBUGGING: {command.to} was original address</p>\n" + command.html
        )
        for line in debug_html.splitlines():
            _logger.debug(line)

        return None

    mime_document = MIMEMultipart()
    mime_document["To"] = command.to
    mime_document["From"] = command.sender
    mime_document["Subject"] = command.subject

    text_document = MIMEText(command.html, "html")
    mime_document.attach(text_document)

    for cid, filepath in command.added_inline_attachments:
        mime_document.attach(_get_mime_document_for_file(cid, filepath, True))

    for name, filepath in command.added_attachments:
        mime_document.attach(_get_mime_document_for_file(name, filepath, False))
    #
    # # Gmail requires a raw field with the base64url encoded document.
    message = {"raw": base64.urlsafe_b64encode(mime_document.as_bytes()).decode()}

    try:
        email = service.users().messages().send(userId="me", body=message).execute()
        _logger.info(
            "Email sent from '%s' to '%s': '%s'",
            command.sender,
            command.to,
            command.subject,
        )
        return email
    except errors.HttpError as e:
        _logger.exception(e)
        raise ServiceUnavailableException("google_error") from e


def _get_mime_document_for_file(
    name: str, filepath: str, inline: bool = False
) -> MIMEBase:
    """
    Get the mime document that can be attached to the main document.
    """
    content_type, encoding = mimetypes.guess_type(filepath)

    if content_type is None or encoding is not None:
        content_type = "application/octet-stream"

    main_type, sub_type = content_type.split("/", 1)
    fp = open(filepath, "rb")

    message: MIMEBase
    if main_type == "text":
        message = MIMEText(fp.read().decode(), sub_type)
    elif main_type == "image":
        message = MIMEImage(fp.read(), sub_type)
    elif main_type == "audio":
        message = MIMEAudio(fp.read(), sub_type)
    else:
        message = MIMEBase(main_type, sub_type)
        message.set_payload(fp.read())

    fp.close()

    # Reference the name in the Content-ID header.
    # The email must have an <img> tag with the src set to 'cid:name_here'
    # for it to be inlined.
    if inline:
        message.add_header("Content-ID", f"<{name}>")
    else:
        message.add_header("Content-Disposition", "attachment", filename=name)

    return message
