from sqlalchemy.orm import Session

from app.dependencies import UserRequestContext
from app.models import Register, User, RegisterCap, CONFLICTING_CAPABILITIES
from app.repository import registers, categories
from app.service import audit_service
from app.views.api_admin.registers import RegisterCreateUpdateBody
from app.views.exceptions import (
    ValidationException,
    ResourceConflictException,
    NotFoundException,
)


def get_by_id(context: UserRequestContext, register_id: int) -> Register:
    register = registers.get_by_id(context.db_session, register_id)
    if register.deleted:
        raise NotFoundException("register_deleted")
    return register


def create(context: UserRequestContext, o: RegisterCreateUpdateBody) -> Register:
    register = Register()
    register.name = o.name
    register.categories = categories.get_all_with_ids(context.db_session, o.categoryIds)
    register.card_terminal = o.cardTerminal

    _set_capabilities(register, o.capabilities)
    _check_conflicting_capabilities(register)

    register = registers.create(context.db_session, register)
    audit_service.audit_create(context, "register.create", object_id=register.id)

    context.db_session.commit()
    return register


def update(
    context: UserRequestContext, register_id: int, o: RegisterCreateUpdateBody
) -> Register:
    register = registers.get_by_id(context.db_session, register_id)
    register.name = o.name
    register.categories = categories.get_all_with_ids(context.db_session, o.categoryIds)
    register.card_terminal_id = o.cardTerminal

    _set_capabilities(register, o.capabilities)

    _check_conflicting_capabilities(register)

    register = registers.update(context.db_session, register)

    audit_service.audit_create(context, "register.update", object_id=register.id)

    context.db_session.commit()
    return register


def get_capabilities_for_register(register: Register) -> list[RegisterCap]:
    return [cap for cap in RegisterCap if (cap.value & register.capabilities) != 0]


def _set_capabilities(register: Register, caps: list[RegisterCap]) -> None:
    register.capabilities = 0
    for cap in caps:
        register.capabilities += int(cap)


def _check_conflicting_capabilities(register: Register) -> None:
    for cap in RegisterCap:
        if (register.capabilities & cap.value) != 0 and cap in CONFLICTING_CAPABILITIES:
            for conflicting_cap in CONFLICTING_CAPABILITIES[cap]:
                if (register.capabilities & conflicting_cap.value) != 0:
                    raise ValidationException("conflicting_capabilities")


def get_default_register(db_session: Session) -> Register:
    register = registers.get_default(db_session)
    if not register:
        raise NotFoundException("no_registers_present")
    return register


def set_default_register(context: UserRequestContext, register_id: int) -> Register:
    register = get_by_id(context, register_id)

    audit_service.audit_create(context, "register.default.set", object_id=register.id)

    return registers.set_default_register(context.db_session, register)


def get_register_for_user(db_session: Session, user: User) -> Register:
    if user.register:
        return user.register
    return get_default_register(db_session)


def delete(context: UserRequestContext, register_id: int) -> Register:
    register = get_by_id(context, register_id)
    have_register = registers.is_any_user_using_register(context.db_session, register)
    if have_register:
        raise ResourceConflictException("register_enabled_for_users")

    if register.default:
        raise ResourceConflictException("register_is_default")

    register.deleted = True

    audit_service.audit_create(context, "register.delete", object_id=register.id)
    context.db_session.commit()
    return register
