import re
from typing import IO

from pydantic import BaseModel

from app.service.invoice.rules import INVOICE_RULES
from app.service.invoice.s3 import upload_and_analyse_with_textract
from app.service.invoice.spec import RuleSpec
from app.views.exceptions import ValidationException


class InvoiceLineItems(BaseModel):
    item: str
    quantity: int
    totalPrice: int
    multipack: int = 1
    productId: int = 0
    productPrice: int = 0
    matchWasGuessed = False


class Invoice(BaseModel):
    id: int
    invoice_id: str
    vendor: str
    errors: list[str]
    line_items: list[InvoiceLineItems]


class ExpenseType(BaseModel):
    Confidence: float
    Text: str


class ExpenseDetection(BaseModel):
    Text: str
    Confidence: float


class GroupProperty(BaseModel):
    Types: list[str]


class ExpenseFields(BaseModel):
    Type: ExpenseType
    LabelDetection: ExpenseDetection | None
    ValueDetection: ExpenseDetection | None
    GroupProperties: list[GroupProperty] | None


class LineItemFields(BaseModel):
    LineItemExpenseFields: list[ExpenseFields]


class LineItemGroup(BaseModel):
    LineItems: list[LineItemFields]


class ExpenseDocument(BaseModel):
    ExpenseIndex: int
    SummaryFields: list[ExpenseFields]
    LineItemGroups: list[LineItemGroup]


def interpret_analysis(expense_documents: list[dict]) -> Invoice:
    errors = []
    docs = [ExpenseDocument(**i) for i in expense_documents]

    # Find matching ruleset based on vendor.
    vendor_values = []
    for doc in docs:
        for summary_field in doc.SummaryFields:
            if summary_field.GroupProperties:
                for group_property in summary_field.GroupProperties:
                    if "VENDOR" in group_property.Types:
                        if summary_field.ValueDetection:
                            vendor = summary_field.ValueDetection.Text
                            vendor_values.append(vendor)

    rule_spec: RuleSpec | None = None
    for vendor in vendor_values:
        for rule_item in INVOICE_RULES:
            if re.search(rule_item.spec.nameHeuristic, vendor):
                rule_spec = rule_item.spec
                break

    if not rule_spec:
        raise ValidationException("invoice_no_rules_match")

    invoice_id_from_heuristic = None
    invoice_id = None
    invoice_line_items = []

    if rule_spec:
        multipack_regex = None
        if rule_spec.lineItems.multipackRegex:
            multipack_regex = rule_spec.lineItems.multipackRegex

        for doc in docs:
            for summary_field in doc.SummaryFields:
                if (
                    summary_field.Type
                    and summary_field.Type.Text == "INVOICE_RECEIPT_ID"
                ):
                    if summary_field.ValueDetection:
                        invoice_id = summary_field.ValueDetection.Text

                if rule_spec.invoice.invoiceIdHeuristic:
                    if summary_field.LabelDetection and re.match(
                        rule_spec.invoice.invoiceIdHeuristic,
                        summary_field.LabelDetection.Text,
                        re.IGNORECASE,
                    ):
                        if summary_field.ValueDetection:
                            invoice_id_from_heuristic = (
                                summary_field.ValueDetection.Text
                            )

            if doc.ExpenseIndex in rule_spec.invoice.ignorePages:
                continue

            for line_item_group in doc.LineItemGroups:
                for line_item in line_item_group.LineItems:
                    item: str | None = None
                    quantity: int = 1
                    price: int = 0
                    multipack: int = 1
                    other = ""

                    for field in line_item.LineItemExpenseFields:
                        if not field.ValueDetection:
                            errors.append("field_without_valuedetection")
                            continue
                        text = field.ValueDetection.Text

                        try:
                            if field.Type.Text == "ITEM":
                                item = text
                            elif field.Type.Text == "QUANTITY":
                                quantity = int(
                                    "".join([c for c in text if c.isdigit()])
                                )
                            elif field.Type.Text == "PRICE":
                                price = int("".join([c for c in text if c.isdigit()]))
                            elif field.Type.Text == "OTHER":
                                other += text + " "
                        except ValueError:
                            errors.append("field_nan")

                    if not item:
                        errors.append("item_missing")
                        continue

                    if rule_spec.lineItems.itemRegex:
                        item_match = re.match(rule_spec.lineItems.itemRegex, item, re.M)
                        if item_match:
                            item = item_match.group(1)

                    if rule_spec.lineItems.truncateItemToFirstLine:
                        item = item.split("\n", maxsplit=1)[0]
                    if rule_spec.lineItems.replaceNewlinesWithSpaces:
                        item = item.replace("\n", " ")
                    if rule_spec.lineItems.replace:
                        for k, v in rule_spec.lineItems.replace.items():
                            item = item.replace(k, v)

                    if rule_spec.lineItems.ignoreRegex:
                        if re.match(rule_spec.lineItems.ignoreRegex, item):
                            continue

                    if multipack_regex:
                        multipack_matchers = [
                            re.search(multipack_regex, item),
                            re.search(multipack_regex, other),
                        ]
                        for multipack_match in multipack_matchers:
                            if multipack_match:
                                for m in multipack_match.groups():
                                    if m:
                                        multipack *= int(m)

                    if not multipack:
                        multipack = 1

                    invoice_item = InvoiceLineItems(
                        item=item,
                        quantity=quantity,
                        totalPrice=price,
                        multipack=multipack,
                    )

                    invoice_line_items.append(invoice_item)

    if not invoice_id_from_heuristic and not invoice_id:
        raise ValidationException("invoice_no_receipt_id_found")

    invoice = Invoice(
        id=-1,
        invoice_id=invoice_id_from_heuristic or invoice_id,
        vendor=rule_spec.vendor,
        errors=errors,
        line_items=invoice_line_items,
    )

    return invoice


def process_and_interpret_invoice(file: IO, filename: str) -> tuple[Invoice, str]:
    document_analysis_result, s3_path = upload_and_analyse_with_textract(file, filename)
    return interpret_analysis(document_analysis_result["ExpenseDocuments"]), s3_path
