from datetime import date
from pydantic import BaseModel
from sqlalchemy import case
from sqlalchemy.orm.session import Session

from app.dependencies import UserRequestContext, RequestContext
from app.models import Product, ProductData, Order
from app.models.account_type import AccountType
from app.models.order import OrderRegister
from app.models.product import NewProductData
from app.repository import categories, products
from app.repository.util.filters import (
    BooleanFilter,
    ForeignKeyFilter,
    LiteralEqualsFilter,
    NullFilter,
    NumberFilter,
)
from app.repository.util.pagination import (
    PageParameters,
    SortParameters,
    PaginatedResponse,
    SearchParameters,
)
from app.service import audit_service, account_service
from app.views.exceptions import NotFoundException
from app.views.exceptions import ValidationException


def paginated_search_all(
    db_session: Session,
    page_params: PageParameters,
    search_params: SearchParameters,
    sort_params: SortParameters,
    price_filter: NumberFilter = None,
    deposit_filter: BooleanFilter = None,
    quantity_filter: NumberFilter = None,
    category_filter: ForeignKeyFilter = None,
    ledger_filter: ForeignKeyFilter = None,
    slide_filter: BooleanFilter = None,
    deleted_filter: NullFilter = None,
    id_filter: LiteralEqualsFilter = None,
) -> PaginatedResponse[ProductData]:
    return products.paginated_search_all(
        db_session,
        page_params,
        search_params,
        sort_params,
        price_filter,
        deposit_filter,
        quantity_filter,
        category_filter,
        ledger_filter,
        slide_filter,
        deleted_filter,
        id_filter,
    )


def mass_update_quantity(
    context: RequestContext, orders: list[Order], delta: int
) -> None:
    mapping: dict[int, int] = {}

    for order in orders:
        if order.product_id is None:
            continue
        if order.register != OrderRegister.DEFAULT:
            continue

        if order.product_id in mapping:
            mapping[order.product_id] += delta
        else:
            mapping[order.product_id] = delta

    if not mapping:
        return

    payload = {
        product: Product.quantity + quantity for product, quantity in mapping.items()
    }

    context.db_session.query(Product).filter(Product.id.in_(payload.keys())).update(
        {Product.quantity: case(payload, value=Product.id)}
    )


def delete_product(context: UserRequestContext, product_id: int) -> None:
    product = products.get_by_id(context.db_session, product_id)

    if not product:
        raise NotFoundException("product_not_found")

    if product.deleted:
        #  FIXME: Allow "undeleting" products?
        raise ValidationException("product_deleted")

    product.deleted = True
    context.db_session.add(product)

    audit_service.audit_create(context, "product.delete", object_id=product.id)


def validate_ledger_for_product(
    context: UserRequestContext, product: NewProductData
) -> None:
    try:
        acc = account_service.get_by_id(context, product.ledgerId)
    except NotFoundException:
        raise ValidationException("ledger_not_found") from None

    if acc.type != AccountType.PRODUCT_LEDGER:
        raise ValidationException("ledger_not_product_ledger")


def update_product(context: UserRequestContext, product: ProductData) -> ProductData:
    updated_product = products.get_by_id(context.db_session, product.id)
    if not updated_product:
        raise NotFoundException("product_not_found")

    if updated_product.deleted:
        raise ValidationException("product_deleted")

    validate_ledger_for_product(context, product)

    updated_product.name = product.name
    updated_product.price = product.price
    updated_product.ledger_id = product.ledgerId
    updated_product.deposit = product.deposit
    updated_product.quantity = product.quantity
    updated_product.categories = categories.get_all_with_ids(
        context.db_session, product.categoryIds
    )

    context.db_session.add(updated_product)
    context.db_session.flush()

    audit_service.audit_create(context, "product.update", object_id=updated_product.id)

    return updated_product.to_pydantic()


def create_product(context: UserRequestContext, product: NewProductData) -> ProductData:
    validate_ledger_for_product(context, product)

    new_product = Product()
    new_product.name = product.name
    new_product.price = product.price
    new_product.ledger_id = product.ledgerId
    new_product.deposit = product.deposit
    new_product.quantity = 0
    new_product.categories = categories.get_all_with_ids(
        context.db_session, product.categoryIds
    )

    # We need to flush to be able to create audit log.
    context.db_session.add(new_product)
    context.db_session.flush()

    audit_service.audit_create(context, "product.create", object_id=new_product.id)
    return new_product.to_pydantic()


class ProductCountAtDate(BaseModel):
    date: date
    purchases: int
    cumPurchases: int
    comparisonPurchases: int
    comparisonCumPurchases: int


class ProductCountComparedPerDay(BaseModel):
    year: int
    comparisonYear: int
    purchases: list[ProductCountAtDate]


def count_per_year(
    context: UserRequestContext,
    product_id: int,
    year: int,
) -> ProductCountComparedPerDay:
    comparison_year = year - 1
    purchases = [
        ProductCountAtDate(
            date=row.date,
            purchases=row.purchases,
            cumPurchases=row.cum_purchases,
            comparisonPurchases=row.comparison_purchases,
            comparisonCumPurchases=row.comparison_cum_purchases,
        )
        for row in products.count_per_year(
            context.db_session, product_id, year, comparison_year
        )
    ]

    return ProductCountComparedPerDay(
        purchases=purchases, year=year, comparisonYear=comparison_year
    )
