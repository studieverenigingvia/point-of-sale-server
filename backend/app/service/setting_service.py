from typing import Any

from sqlalchemy.orm import Session

from app.dependencies import UserRequestContext
from app.repository import settings
from app.service import mail_service
from app.views.exceptions import ValidationException
from app.models.setting import DatabaseSettings


def update_settings(context: UserRequestContext, values: dict[str, Any]) -> None:
    if "notification_sender" in values:
        if values["notification_sender"] not in mail_service.get_treasurer_aliases():
            raise ValidationException("invalid_alias")
    return settings.update_settings(context.db_session, values)


def get_db_settings(db_session: Session) -> DatabaseSettings:
    return settings.get_db_settings(db_session)


def save_db_settings(
    db_session: Session, database_settings: DatabaseSettings
) -> DatabaseSettings:
    return settings.save_db_settings(db_session, database_settings)
