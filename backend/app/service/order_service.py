from app.dependencies import RequestContext
from app.models import Order, Product
from app.models.account import AccountType
from app.models.checkout import CheckoutMethod
from app.models.order import OrderData, OrderRegister, OrderFlowType
from app.repository import orders
from app.views.exceptions import ValidationException


def delete_orders(
    context: RequestContext,
    flow_type: OrderFlowType,
    order_list: list[Order],
    do_commit: bool = True,
) -> None:
    """
    Deletes orders by creating the reverse of the given orders.

    The deleted property on an order indicates if THAT order deletes a
    different order. A deletion order cannot be deleted itself. I.e.
    they do not stack.

    This also credits the account with the balance they lost, only if the flow was
    completed.

    :param order_list:
    :param do_commit: indicates if db session will commit
    :return:
    """

    if flow_type not in [
        OrderFlowType.COMPLETED_TO_DELETED,
        OrderFlowType.PENDING_TO_DELETED,
        OrderFlowType.PENDING_TO_EXPIRED,
    ]:
        raise ValidationException("invalid_flow_type")

    new_orders = []

    for o in order_list:
        if o.deleted:
            raise ValidationException("order_is_deletion_order")
        if flow_type == OrderFlowType.COMPLETED_TO_DELETED:
            if (
                o.account.type == AccountType.USER
                or o.account.type == AccountType.EVENT
            ):
                o.account.balance += o.price

        new = create_reverse_order(o, flow_type)
        new_orders.append(new)

    orders.save_multiple(context.db_session, new_orders, do_commit=do_commit)


def create_reverse_order(original: Order, flow_type: OrderFlowType) -> Order:
    # Simply copy most of the properties
    new = Order(
        account_id=original.account_id,
        product_id=original.product_id,
        transaction_id=original.transaction_id,
        register=original.register,
        flow_type=flow_type,
        sepa_batch_id=original.sepa_batch_id,
    )

    # This uses a setter so can't have it as kwarg
    new.method = original.method

    # Revert the price
    new.price = -original.price

    # Set deleted to true so we know this is a deletion order.
    new.deleted = True

    return new


def create_order(
    product: Product,
    transaction_id: int,
    method: CheckoutMethod,
    flow_type: OrderFlowType,
    register: OrderRegister = OrderRegister.DEFAULT,
    sepa_batch_id: int = None,
) -> Order:
    o = Order(
        product_id=product.id,
        transaction_id=transaction_id,
        price=-product.price,
        account_id=product.ledger_id,
        register=register,
        flow_type=flow_type,
    )
    # This uses a setter so can't have it as kwarg
    o.method = method

    if sepa_batch_id is not None:
        o.sepa_batch_id = sepa_batch_id

    return o


def save_multiple(
    context: RequestContext, order_list: list[Order], do_commit: bool = True
) -> None:
    orders.save_multiple(context.db_session, order_list, do_commit)


def consolidate_orders(orders: list[OrderData]) -> list[OrderData]:
    orders_per_product_id: dict[tuple[int, str], OrderData] = {}
    consolidated_orders = []
    orders_are_deleted = False

    for o in orders:
        if o.deleted:
            #  Ignore deleted orders (the transaction will indicate that all
            #  orders are deleted)
            orders_are_deleted = True
            continue

        if not o.product:
            consolidated_orders.append(o)
            continue

        key = (o.product.id, o.order_register)
        if key in orders_per_product_id:
            orders_per_product_id[key].count += 1
            orders_per_product_id[key].price += o.price
        else:
            orders_per_product_id[key] = o

    for o in orders_per_product_id.values():
        if o.count > 1 and o.product:
            o.product.name = f"{o.count}x {o.product.name}"
        consolidated_orders.append(o)

    if orders_are_deleted:
        #  Mark all orders as deleted
        for o in consolidated_orders:
            o.deleted = True

    return sorted(consolidated_orders, key=lambda o: o.price)
