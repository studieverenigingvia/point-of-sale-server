import logging
import requests

from app.views.exceptions import ServiceUnavailableException

base_url = "https://svia.nl/api/"
_logger = logging.getLogger(__name__)


class ViaductUser:
    def __init__(self, id: int, tfa_enabled: bool):
        self.id = id
        self.tfa_enabled = tfa_enabled

    id: int
    tfa_enabled: bool


def get_self_user(token: str) -> ViaductUser:
    user_url = f"{base_url}users/self/"

    response = requests.request(
        "GET", user_url, headers={"Authorization": f"Bearer {token}"}
    )
    if response.status_code != 200:
        _logger.warning(response, response.content)
        raise ServiceUnavailableException("viaduct_unavailable")

    viaduct_user_response = response.json()

    return ViaductUser(
        id=viaduct_user_response.get("id"),
        tfa_enabled=viaduct_user_response.get("tfa_enabled"),
    )
