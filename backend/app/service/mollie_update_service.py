from mollie.api.client import Client as MollieClient
from mollie.api.objects.payment import Payment

import config
from app.dependencies import RequestContext
from app.models import IdealPayment
from app.models.checkout import CheckoutMethod
from app.models.transaction import TransactionStatus
from app.repository import mollie_repo
from app.service.flow.flow_service import process_flow_init
from app.service.flow.flow_types import (
    InitProcessFlowRequest,
    ProcessFlowUpgradeRequest,
)

mollie_client = MollieClient()


def update_ideal_payment(
    context: RequestContext, payment: IdealPayment
) -> tuple[IdealPayment, Payment]:
    mollie_data = get_mollie_data(payment.mollie_id)

    if mollie_data.is_paid() and payment.status != "paid":
        # Up the balance of this user
        amount = int(float(mollie_data["amount"]["value"]) * 100)

        init = InitProcessFlowRequest(
            nonce=f"ideal-nonce-{payment.id}1",
            desired_state=TransactionStatus.COMPLETED,
            method=CheckoutMethod.IDEAL,
            purchase_request=None,
            upgrade_request=ProcessFlowUpgradeRequest(
                upgrade_account_id=payment.account_id,
                upgrade_amount=amount,
                reason=None,
            ),
            account_source=None,
            card_terminal=None,
            reason=None,
        )

        result = process_flow_init(context, init)
        payment.transaction_id = result.transaction.id

    payment.status = mollie_data["status"]
    mollie_repo.save(context.db_session, payment)

    return payment, mollie_data


def get_mollie_data(payment_id: str) -> Payment:
    mollie_client.set_api_key(config.MOLLIE_API_KEY)

    return mollie_client.payments.get(payment_id)
