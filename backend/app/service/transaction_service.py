from sqlalchemy.orm.session import Session

import pydantic

from app.dependencies import UserRequestContext, RequestContext
from app.models import Transaction
from app.models.transaction import TransactionData
from app.repository import transactions
from app.repository.util.pagination import (
    PageParameters,
    PaginatedResponse,
    SearchParameters,
    SortParameters,
)
from app.repository.util.filters import DateTimeFilter, NullFilter, LiteralEqualsFilter
from app.service import order_service
from app.views.exceptions import NotFoundException


def get_by_id(
    db_session: Session, transaction_id: int, for_update: bool = False
) -> Transaction:
    t = transactions.get_transaction_by_id(db_session, transaction_id, for_update)
    if not t:
        raise NotFoundException("transaction_not_found")
    return t


def save(
    context: RequestContext, transaction: Transaction, do_commit: bool = True
) -> None:
    transactions.save(context.db_session, transaction, do_commit)


class ListTransactionParameters(pydantic.BaseModel):
    accountId: int | None
    sepaBatchId: int | None
    deleted: bool | None


def paginated_search_all(
    context: UserRequestContext,
    params: ListTransactionParameters,
    page_params: PageParameters,
) -> PaginatedResponse[TransactionData]:
    return transactions.paginated_search_all(context.db_session, params, page_params)


def consolidate_orders_in_transactions(
    transactions: list[TransactionData],
) -> list[TransactionData]:
    for t in transactions:
        t.orders = order_service.consolidate_orders(t.orders)
    return transactions


def paginated_search_all_v2(
    db_session: Session,
    page_params: PageParameters,
    search_params: SearchParameters,
    sort_params: SortParameters,
    id_filter: LiteralEqualsFilter = None,
    deleted_filter: NullFilter = None,
    product_filter: LiteralEqualsFilter = None,
    account_filter: LiteralEqualsFilter = None,
    created_at_filter: DateTimeFilter = None,
    flow_filter: LiteralEqualsFilter = None,
    sepa_batch_filter: LiteralEqualsFilter = None,
) -> PaginatedResponse[TransactionData]:
    result = transactions.paginated_search_all_v2(
        db_session,
        page_params,
        search_params,
        sort_params,
        id_filter,
        deleted_filter,
        product_filter,
        account_filter,
        created_at_filter,
        flow_filter,
        sepa_batch_filter,
    )
    result.items = consolidate_orders_in_transactions(result.items)
    return result
