from sqlalchemy.orm import Session

from app.models.card_terminal import CardTerminal
from app.models.transaction import TransactionCard
from app.service.flow.utils.process_flow import ProcessFlowCardTerminalData
from app.service.mollie_service import create_card_terminal_payment
from app.tasks.single.sepay import (
    sepay_create_transaction,
)


def start_card_transaction(
    card_terminal_input: ProcessFlowCardTerminalData,
    transaction_id: int,
    total: int,
) -> tuple[CardTerminal, TransactionCard]:
    terminal = card_terminal_input.terminal

    if terminal.brand == "sepay":
        sepay_create_transaction.delay(
            total / 100, str(transaction_id), terminal.external_id
        )
        external_id = None

    elif terminal.brand == "mollie":
        result = create_card_terminal_payment(
            transaction_id, total, terminal.external_id
        )
        external_id = result["id"]
    else:
        raise NotImplementedError(f"Unknown brand: {terminal.brand}")

    transaction_card = TransactionCard(
        id=transaction_id,
        card_terminal_id=terminal.id,
        external_id=external_id,
    )

    return terminal, transaction_card


def get_available_terminals(db_session: Session) -> list[CardTerminal]:
    return db_session.query(CardTerminal).all()
