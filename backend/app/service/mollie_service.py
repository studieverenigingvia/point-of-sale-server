from urllib.parse import urljoin

from mollie.api.client import Client as MollieClient
from starlette.requests import Request

import config
from app.dependencies import UserRequestContext
from app.models import IdealPayment
from app.repository import mollie_repo

mollie_client = MollieClient()


def create_ideal_payment(
    context: UserRequestContext,
    request: Request,
    account_id: int,
    amount_in_euros: float,
) -> tuple[IdealPayment, dict]:
    mollie_client.set_api_key(config.MOLLIE_API_KEY)
    if getattr(config, "DEBUG", False):
        webhook = urljoin(
            config.NGROK_HOST,
            request.scope["router"].url_path_for("mollie_callback"),
        )
    else:
        webhook = request.url_for("mollie_callback")

    ideal_payment = IdealPayment(account_id=account_id)
    mollie_repo.flush(context.db_session, ideal_payment)

    mollie_data = mollie_client.payments.create(
        {
            "amount": {
                "currency": "EUR",
                "value": f"{amount_in_euros:.2f}",
            },
            "metadata": {"accountId": account_id, "name": context.user.name},
            "description": "Upgrade via account",
            "redirectUrl": urljoin(
                str(request.base_url), f"/account?cb={ideal_payment.id}"
            ),
            "webhookUrl": webhook,
            "method": "ideal",
        }
    )

    ideal_payment.mollie_id = mollie_data["id"]
    mollie_repo.save(context.db_session, ideal_payment)

    return ideal_payment, mollie_data


def create_card_terminal_payment(
    transaction_id: int,
    amount: int,
    terminal_id: str,
) -> dict:
    mollie_client.set_api_key(config.MOLLIE_API_KEY)

    # TODO: Do we want to listen to webhooks?
    mollie_data = mollie_client.payments.create(
        {
            "amount": {
                "currency": "EUR",
                "value": f"{amount / 100:.2f}",
            },
            "description": f"Card Terminal Payment {transaction_id}",
            "method": "pointofsale",
            "terminalId": terminal_id,
        }
    )

    if (
        getattr(config, "DEBUG", False)
        and "changePaymentState" in mollie_data["_links"]
    ):
        print(
            "You can manually approve the payment at: ",
            mollie_data["_links"]["changePaymentState"]["href"],
        )

    return mollie_data
