import string
from datetime import datetime
from typing import Iterable

import bcrypt
import pydantic
from sqlalchemy.orm import Session

from app.dependencies import UserRequestContext, RequestContext
from app.models import UserDirectDebit
from app.models.account import (
    AccountRequest,
    AccountRequestStatus,
    Account,
    DirectDebitAccountData,
    SerializedAccountRequest,
    AccountExactLedger,
)
from app.models.checkout import CheckoutMethod
from app.models.order import Order
from app.repository import accounts
from app.repository.util.pagination import (
    PageParameters,
    PaginatedResponse,
    SearchParameters,
    SortParameters,
)
from app.service import mail_service
from app.service.flow.flow_types import BalanceResult
from app.service.setting_service import get_db_settings
from app.views.exceptions import (
    ValidationException,
    NotFoundException,
    ResourceConflictException,
)


def find_latest_account_request(
    context: UserRequestContext,
) -> AccountRequest | None:
    return accounts.find_latest_account_request(context)


def request_account(context: UserRequestContext, pin: str) -> AccountRequest:
    if find_latest_account_request(context) is not None:
        raise ValidationException("account_request_pending")

    _validate_pin(pin)

    encrypted_pin = bcrypt.hashpw(pin.encode("utf-8"), bcrypt.gensalt()).decode("utf-8")

    acc_req = accounts.create_account_request(context, encrypted_pin)

    command = mail_service.MailCommand(
        db_session=context.db_session, to="secretaris@svia.nl"
    )
    command.with_template(
        template_name="email/account_requested.html",
        name=acc_req.user.name,
    )
    mail_service.send_email(command)
    return acc_req


def paginated_search_requests(
    db_session: Session,
    page_params: PageParameters,
    search_params: SearchParameters,
    sort_params: SortParameters,
) -> PaginatedResponse[SerializedAccountRequest]:
    return accounts.paginated_search_requests(
        db_session, page_params, search_params, sort_params
    )


def set_account_request_status(
    context: UserRequestContext, account_request_id: int, status: AccountRequestStatus
) -> AccountRequest:
    account_request = accounts.find_account_request_by_id(context, account_request_id)
    if not account_request:
        raise NotFoundException()

    if account_request.status == AccountRequestStatus.APPROVED.value:
        raise ResourceConflictException("account_request_been_accepted")

    if status == AccountRequestStatus.APPROVED:
        create_account_for_request(context, account_request)

    accounts.set_account_request_status(context, account_request, status)

    return account_request


def create_account_for_request(
    context: UserRequestContext, account_request: AccountRequest
) -> Account:
    return accounts.create_account_and_link_user(context, account_request)


def _validate_pin(pin: str) -> None:
    """Checks if a pin is 4 digits in length"""
    if len(pin) != 4 or not all(d in string.digits for d in pin):
        raise ValidationException("not_a_pin")


def set_account_pin(account: Account, pin: str) -> None:
    _validate_pin(pin)

    account.pin = bcrypt.hashpw(pin.encode("utf-8"), bcrypt.gensalt()).decode("utf-8")


def check_pin(account: Account, pin: str) -> bool:
    if not account.pin:
        return False

    _validate_pin(pin)

    return bcrypt.checkpw(pin.encode("utf-8"), account.pin.encode("utf-8"))


class AccountBalanceResponse(pydantic.BaseModel):
    accountName: str
    currentBalance: int
    transactionsSince: int | None
    oldBalance: int


class BalanceAtDate(pydantic.BaseModel):
    accountList: list[AccountBalanceResponse]
    totalPositive: int
    totalNegative: int
    totalBalance: int


def get_total_account_balance_at_date(
    context: RequestContext, date: datetime
) -> BalanceAtDate:
    account_list: list[AccountBalanceResponse] = []
    total_positive, total_negative = 0, 0

    for e in accounts.get_account_balances_at_date(context, date):
        abr = AccountBalanceResponse(
            accountName=e[0],
            currentBalance=e[1],
            transactionsSince=e[2],
            oldBalance=e[1] + (e[2] or 0),
        )

        account_list.append(abr)

        if abr.oldBalance > 0:
            total_positive += abr.oldBalance
        else:
            total_negative += abr.oldBalance

    return BalanceAtDate(
        accountList=account_list,
        totalPositive=total_positive,
        totalNegative=total_negative,
        totalBalance=total_positive + total_negative,
    )


def get_by_id(context: RequestContext, account_id: int) -> Account:
    account = context.db_session.get(Account, account_id)
    if not account or account.deleted:
        raise NotFoundException()
    return account


def get_all(
    context: UserRequestContext, ledger: bool = False, with_pin: bool = False
) -> list[Account]:
    return accounts.get_all(context.db_session, ledger=ledger, with_pin=with_pin)


def get_all_with_joined_user_direct_debit(
    db_session: Session,
) -> Iterable[tuple[Account, UserDirectDebit | None]]:
    return accounts.get_all_with_joined_user_direct_debit(db_session)


def account_and_user_direct_debit_to_direct_debit_account_data(
    account: Account,
    direct_debit: UserDirectDebit | None,
) -> DirectDebitAccountData:
    if direct_debit:
        return DirectDebitAccountData(
            **account.to_pydantic().dict(),
            **direct_debit.to_pydantic().dict(),
            directDebit=True,
        )

    return DirectDebitAccountData(**account.to_pydantic().dict(), directDebit=False)


def get_all_eligible_for_direct_debit_batch(
    context: UserRequestContext,
) -> list[DirectDebitAccountData]:
    accounts_with_dd = accounts.get_all_eligible_for_direct_debit_batch(
        context.db_session
    )

    res: list[DirectDebitAccountData] = []
    for account, direct_debit in accounts_with_dd:
        res.append(
            DirectDebitAccountData(
                **account.to_pydantic().dict(),
                **direct_debit.to_pydantic().dict(),
                directDebit=True,
            )
        )

    return res


class AccountLedgerExactUpdate(pydantic.BaseModel):
    ledgerCode: str
    costCenterCode: str | None
    vatCode: str | None


def set_account_exact_ledger(
    context: UserRequestContext, account_id: int, exact_ledger: AccountLedgerExactUpdate
) -> AccountExactLedger:
    account = get_by_id(context, account_id)
    return accounts.set_account_exact_ledger(
        context.db_session,
        account,
        ledger_code=exact_ledger.ledgerCode,
        cost_center_code=exact_ledger.costCenterCode,
        vat_code=exact_ledger.vatCode,
    )


def get_account_for_checkout_method(db_session: Session, method: CheckoutMethod) -> int:
    account_id = get_db_settings(db_session).get_setting_for_checkout_method(method)

    if account_id is None:
        raise Exception(f"{method.value}_checkout_method_has_no_linked_account")

    return int(account_id)


def update_account_balances_from_orders(
    db_session: Session, orders: list[Order]
) -> BalanceResult | None:
    results = accounts.update_account_balances_from_orders(db_session, orders)

    if len(results) == 0:
        # No updates is find, e.g. in the case of direct pin purchase
        return None

    if len(results) > 1:
        # If we ever want transfers between user accounts, we should remove this throw
        raise ResourceConflictException("multiple_balances_updated")

    before, after = results[0]

    return BalanceResult(before=before, after=after)
