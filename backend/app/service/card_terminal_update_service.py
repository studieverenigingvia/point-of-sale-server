from datetime import datetime

from sqlalchemy.orm import Session

from app.dependencies import RequestContext
from app.models.transaction import TransactionStatus, Transaction
from app.service import mollie_update_service
from app.tasks.single.sepay import (
    sepay_get_transaction_status,
    sepay_status_to_transaction_status,
)
from app.service.flow.flow_service import process_flow_next, NextProcessFlowRequest


def update_transaction_based_on_status(
    db_session: Session, next_status: TransactionStatus, transaction_id: int
) -> TransactionStatus:
    if next_status == TransactionStatus.PENDING:
        return next_status

    req = NextProcessFlowRequest(
        desired_state=next_status, transaction_id=transaction_id
    )
    ctx = RequestContext(db_session)
    process_flow_next(ctx, req)

    return next_status


def get_status_of_transaction_and_update(
    db_session: Session, transaction: Transaction
) -> tuple[str, TransactionStatus]:
    card_data = transaction.card_data
    if card_data is None:
        raise ValueError("Transaction is not linked to a card terminal.")

    terminal = card_data.card_terminal
    if terminal.brand == "sepay":
        age_in_seconds = (datetime.now() - transaction.created_at).total_seconds()
        details, status = _get_status_of_sepay_transaction(
            transaction.id, terminal.external_id, age_in_seconds
        )
    elif terminal.brand == "mollie":
        details, status = _get_status_of_mollie_transaction(transaction)
    else:
        raise NotImplementedError(f"Unknown brand: {terminal.brand}")

    update_transaction_based_on_status(db_session, status, transaction.id)

    return details, status


def _get_status_of_sepay_transaction(
    transaction_id: int, terminal_id: str, age: float = 0
) -> tuple[str, TransactionStatus]:
    response, response_status_code = sepay_get_transaction_status(
        str(transaction_id), terminal_id
    )

    next_status = sepay_status_to_transaction_status(response_status_code, age)
    return str(response_status_code.name).lower(), next_status


def _get_status_of_mollie_transaction(
    transaction: Transaction,
) -> tuple[str, TransactionStatus]:
    if transaction.card_data.external_id is None:
        raise ValueError("Transaction is not linked to a Mollie payment.")

    data = mollie_update_service.get_mollie_data(transaction.card_data.external_id)

    if data.is_paid():
        return data.description, TransactionStatus.COMPLETED

    if data.is_failed() or data.is_canceled():
        return data.description, TransactionStatus.CANCELLED

    if data.is_expired():
        return data.description, TransactionStatus.EXPIRED

    return data.description, TransactionStatus.PENDING
