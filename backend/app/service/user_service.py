from datetime import date
import logging
from typing import Optional

import bcrypt
from pydantic import BaseModel, ConstrainedStr, Field, root_validator, validator
import requests

import config
from app.dependencies import UserRequestContext, RequestContext
from app.models import User, UserDirectDebit
from app.models.user_direct_debit import UserDirectDebitData
from app.repository import users, accounts as account_repo
from app.service import viaduct_service, audit_service, sepa_service
from app.service.viaduct_service import ViaductUser
from app.views.exceptions import (
    ValidationException,
    ServiceUnavailableException,
    NotFoundException,
    ResourceConflictException,
)

_logger = logging.getLogger(__name__)


class UsernameString(ConstrainedStr):
    strip_whitespace = True
    to_lower = True


class PasswordString(ConstrainedStr):
    min_length = 8


class UserCreateUpdateRequest(BaseModel):
    accountId: int | None
    isAdmin: bool = False
    longLogin: bool = False
    name: str
    registerId: int | None
    username: UsernameString
    viaductUserId: int | None
    password: PasswordString | None


class UserDirectDebitUpdateRequest(BaseModel):
    enabled: bool
    iban: str | None = Field(None, max_length=48)
    bic: str | None = Field(None, max_length=16)
    mandateId: str | None = Field(None, max_length=64)
    signDate: date | None

    @validator("iban")
    def remove_iban_whitespace(
        cls, v: str, values: dict[str, str | bool | None]
    ) -> str:
        return v.replace(" ", "") if v else v

    @root_validator
    def check_direct_debit(
        cls, values: dict[str, str | bool | date | None]
    ) -> dict[str, str | bool | date | None]:
        if values.get("enabled") is True:
            assert values.get(
                "signDate"
            ), "Sign date is required to enable direct debit"
            assert values.get(
                "mandateId"
            ), "Mandate ID is required to enable direct debit"
            assert values.get("iban"), "IBAN is required to enable direct debit"
            assert values.get("bic"), "BIC is required to enable direct debit"

        return values


def _is_clear_request(req: UserDirectDebitUpdateRequest) -> bool:
    return (
        req.enabled is False
        and req.iban is None
        and req.bic is None
        and req.mandateId is None
        and req.signDate is None
    )


def login_with_username_password(
    context: RequestContext, username: str, password: str
) -> Optional["User"]:
    """
    Get the user object using it's credentials
    Only "local" accounts are used, not viaduct accounts.
    """
    user = users.find_user_by_username(context.db_session, username)
    if user is None or user.deleted:
        return None

    if password and _verify_password(user, password):
        return user
    return None


def _verify_password(user: User, password: str) -> bool:
    if not password or type(password) is not str or not user.password:
        return False

    return bcrypt.checkpw(password.encode("utf-8"), user.password.encode("utf-8"))


def login_with_viaduct_oauth(
    context: RequestContext, host: str, url_root: str, authorization_code: str
) -> tuple[User, ViaductUser]:
    token_url = f"{host}oauth/token"
    token_data = {
        "code": authorization_code,
        "client_id": config.VIADUCT_CLIENT_ID,
        "client_secret": config.VIADUCT_CLIENT_SECRET,
        # request.url_root = https://svia.nl/
        "redirect_uri": f"{url_root}login/callback",
        "grant_type": "authorization_code",
        "scope": "user",
    }

    if config.DEBUG and config.VIADUCT_CLIENT_ID == "pos-proxy":
        token_data["redirect_uri"] = "https://proxy.svia.nl/login/callback"

    _logger.warning(
        "Validating oauth request [redirect_uri=%s]", token_data["redirect_uri"]
    )

    verify_responses = True
    try:
        response = requests.request(
            "POST", token_url, data=token_data, verify=verify_responses
        )
        if response.status_code != 200:
            _logger.error(
                "OAuth token request failed [status=%d, content=%s]",
                response.status_code,
                response.content,
            )
            raise ValidationException("oauth_code_invalid")

        login_response = response.json()

        access_token = login_response.get("access_token")

        # Check the actual user id in viaduct using OAuth token introspection.
        introspect_url = f"{host}oauth/introspect"
        introspect_data = {
            "token": access_token,
            "client_id": config.VIADUCT_CLIENT_ID,
            "client_secret": config.VIADUCT_CLIENT_SECRET,
        }

        response = requests.request(
            "POST", introspect_url, data=introspect_data, verify=verify_responses
        )
        if response.status_code != 200:
            _logger.warning(response, response.content)
            raise ServiceUnavailableException("oauth_unavailable")
    except requests.exceptions.ConnectionError as e:
        _logger.warning(e)
        raise ServiceUnavailableException("oauth_unavailable") from e

    introspect_response = response.json()
    # Check whether a user in the database without password and the correct
    # viaduct_user_id is present.
    viaduct_user_id = introspect_response.get("sub")

    username = introspect_response.get("username").lower()
    name = introspect_response.get("full_name")

    if type(viaduct_user_id) is not int:
        raise ValidationException()

    user = users.find_user_by_viaduct_id(context.db_session, viaduct_user_id)

    viaduct_user = viaduct_service.get_self_user(access_token)

    if not user:
        user = User(
            username=username,
            password=None,
            is_admin=False,
            viaduct_user_id=viaduct_user_id,
            name=name,
        )
    else:
        user.username = username
        user.name = name

    users.create_or_update_user(context.db_session, user)

    return user, viaduct_user


def get_by_id(context: UserRequestContext, user_id: int) -> User:
    user = users.get_by_id(context.db_session, user_id)
    if not user:
        raise NotFoundException()
    return user


def get_all(context: UserRequestContext) -> list[User]:
    return users.get_all(context.db_session)


def find_by_account_id(context: UserRequestContext, account_id: int) -> User | None:
    return users.find_user_by_account_id(context.db_session, account_id)


def is_account_taken_by_user(
    context: UserRequestContext, current_user_id: int, requested_account_id: int | None
) -> bool:
    if not requested_account_id:
        return False

    user = users.find_user_by_account_id(context.db_session, requested_account_id)
    if not user:
        return False

    return user.id != current_user_id


def update_account_allow_negative_balance(
    context: UserRequestContext,
    user_dd: UserDirectDebit | None,
    old_account_id: int | None,
    requested_account_id: int | None,
) -> None:
    if requested_account_id == old_account_id:
        # Not changing account, no-op
        return

    enable_direct_debit = user_dd is not None and user_dd.enabled

    if requested_account_id:
        requested_account = account_repo.get_by_id(
            context.db_session, requested_account_id
        )
        if requested_account is None:
            raise ValidationException("requested_account_does_not_exist")

        requested_account.allow_negative_balance = enable_direct_debit
        context.db_session.add(requested_account)

    if old_account_id:
        old_account = account_repo.get_by_id(context.db_session, old_account_id)
        if old_account is None:
            raise ValidationException("old_account_does_not_exist")

        #  Account will not be linked to a user anymore, cannot be negative
        old_account.allow_negative_balance = False
        context.db_session.add(old_account)


def update_user(
    context: UserRequestContext, user_request: UserCreateUpdateRequest, user_id: int
) -> User:
    user = get_by_id(context, user_id)

    if is_account_taken_by_user(context, user_id, user_request.accountId):
        raise ResourceConflictException("account_taken_by_user")

    update_account_allow_negative_balance(
        context, user.direct_debit, user.account_id, user_request.accountId
    )

    user.username = user_request.username
    user.is_admin = user_request.isAdmin
    user.long_login = user_request.longLogin
    user.account_id = user_request.accountId
    user.viaduct_user_id = user_request.viaductUserId
    user.register_id = user_request.registerId
    user.name = user_request.name
    if user_request.password:
        user.set_password(user_request.password)

    audit_service.audit_create(context, "user.edit", object_id=user_id)

    context.db_session.add(user)
    context.db_session.commit()
    return user


def get_user_direct_debit(
    context: UserRequestContext, user_id: int
) -> UserDirectDebit | None:
    return users.get_user_direct_debit(context.db_session, user_id)


def check_disable_direct_debit(
    context: UserRequestContext, direct_debit_enabled: bool, account_id: int | None
) -> None:
    if not account_id:
        return

    if direct_debit_enabled:
        # Can always enable
        return

    batch, _ = sepa_service.get_pending_batch_for_account(context, account_id)

    if batch:
        raise ValidationException(
            "cannot_disable_direct_debit_when_pending_sepa_batch_exists"
        )


def update_user_direct_debit(
    context: UserRequestContext,
    user_id: int,
    direct_debit: UserDirectDebitUpdateRequest,
) -> UserDirectDebitData:
    user = get_by_id(context, user_id)

    check_disable_direct_debit(context, direct_debit.enabled, user.account_id)

    if _is_clear_request(direct_debit):
        user_dd = UserDirectDebit(
            user_id=user_id,
            enabled=False,
        )
        users.delete_user_direct_debit(context.db_session, user_id)
    elif user_dd := user.direct_debit:
        user_dd.enabled = direct_debit.enabled
        user_dd.iban = direct_debit.iban
        user_dd.bic = direct_debit.bic
        user_dd.mandate_id = direct_debit.mandateId
        user_dd.sign_date = direct_debit.signDate
        context.db_session.add(user_dd)
    else:
        user_dd = UserDirectDebit(
            user_id=user_id,
            enabled=direct_debit.enabled,
            iban=direct_debit.iban,
            bic=direct_debit.bic,
            mandate_id=direct_debit.mandateId,
            sign_date=direct_debit.signDate,
        )
        context.db_session.add(user_dd)

    update_account_allow_negative_balance(context, user_dd, None, user.account_id)

    return user_dd.to_pydantic()
