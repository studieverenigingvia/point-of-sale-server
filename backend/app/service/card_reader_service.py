import logging

from sqlalchemy.orm import Session

from app.dependencies import UserRequestContext
from app.models import Account
from app.models.checkout_card import CheckoutCard
from app.repository import (
    checkout_repo,
)
from app.service import (
    account_service,
    setting_service,
)
from app.views.exceptions import (
    ValidationException,
    NotFoundException,
    UnprocessableEntityException,
    ResourceConflictException,
)

_logger = logging.getLogger(__name__)


def _validate_card_secret(db_session: Session, card_secret: str) -> str:
    reader_secret = setting_service.get_db_settings(db_session).checkout_reader_secret

    if reader_secret is None:
        _logger.warning(
            "Checkout using reader not enabled, configure 'CHECKOUT_READER_SECRET'."
        )
        raise ResourceConflictException("reader_disabled")

    if not card_secret.startswith(reader_secret):
        raise ValidationException("invalid_card_secret")
    card_secret = card_secret.removeprefix(reader_secret)
    if not card_secret:
        raise ValidationException("invalid_card_secret")
    return card_secret


def get_account_for_card_secret(db_session: Session, card_secret: str) -> Account:
    card_secret = _validate_card_secret(db_session, card_secret)

    user = checkout_repo.get_user_for_card_secret(db_session, card_secret)

    if not user:
        raise NotFoundException("unknown_card_secret")

    if not user.account:
        raise NotFoundException("no_linked_account")

    return user.account


def register_card_for_account(
    context: UserRequestContext, account_id: int, card_secret: str, pin: str
) -> None:
    account = account_service.get_by_id(context, account_id)
    if not account_service.check_pin(account, pin):
        raise UnprocessableEntityException("invalid_account_pin")

    card_secret = _validate_card_secret(context.db_session, card_secret)

    if checkout_repo.find_card_by_secret(context.db_session, card_secret):
        raise ResourceConflictException("card_secret_exists")

    if not account.user:
        raise NotFoundException("no_user_for_account")

    context.db_session.add(
        CheckoutCard(card_secret=card_secret, user_id=account.user.id)
    )
    context.db_session.commit()
