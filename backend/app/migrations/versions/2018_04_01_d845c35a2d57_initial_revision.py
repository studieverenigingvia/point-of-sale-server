"""Initial revision

Revision ID: d845c35a2d57
Revises:
Create Date: 2018-04-01 23:48:11.753903

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "d845c35a2d57"
down_revision = None
branch_labels = ("default",)
depends_on = None


def upgrade():
    op.create_table(
        "account",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.Column("deleted_at", sa.DateTime(), nullable=True),
        sa.Column("name", sa.String(length=64), nullable=True),
        sa.Column("pin", sa.String(length=64), nullable=True),
        sa.Column("balance", sa.Integer(), nullable=True),
        sa.Column("iban", sa.String(length=48), nullable=True),
        sa.Column("bic", sa.String(length=16), nullable=True),
        sa.Column("direct_debit", sa.Boolean(), nullable=True),
        sa.Column("mandate_id", sa.String(length=64), nullable=True),
        sa.Column("sign_date", sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint("id"),
    )

    op.create_table(
        "category",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.Column("deleted_at", sa.DateTime(), nullable=True),
        sa.Column("name", sa.String(length=64), nullable=True),
        sa.Column("position", sa.Integer(), nullable=True),
        sa.Column("color", sa.Integer(), nullable=True),
        sa.Column("bar_only", sa.Boolean(), nullable=True),
        sa.PrimaryKeyConstraint("id"),
    )

    op.create_table(
        "configuration",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.Column("deleted_at", sa.DateTime(), nullable=True),
        sa.Column("sepa_name", sa.String(), nullable=True),
        sa.Column("sepa_iban", sa.String(), nullable=True),
        sa.Column("sepa_bic", sa.String(), nullable=True),
        sa.Column("sepa_batch", sa.Boolean(), nullable=True),
        sa.Column("sepa_creditor_id", sa.String(), nullable=True),
        sa.Column("sepa_currency", sa.String(), nullable=True),
        sa.Column("sepa_purpose", sa.String(), nullable=True),
        sa.PrimaryKeyConstraint("id"),
    )

    op.create_table(
        "user",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.Column("deleted_at", sa.DateTime(), nullable=True),
        sa.Column("username", sa.String(length=64), nullable=True),
        sa.Column("password", sa.String(length=64), nullable=True),
        sa.Column("is_admin", sa.Boolean(), nullable=True),
        sa.Column("is_bar", sa.Boolean(), nullable=True),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("username"),
    )

    op.create_table(
        "login_token",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.Column("deleted_at", sa.DateTime(), nullable=True),
        sa.Column("user_id", sa.Integer(), nullable=True),
        sa.Column("token", sa.String(length=32), nullable=True),
        sa.Column("is_bar", sa.Boolean(), nullable=True),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["user.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("token"),
    )

    op.create_table(
        "product",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.Column("deleted_at", sa.DateTime(), nullable=True),
        sa.Column("category_id", sa.Integer(), nullable=True),
        sa.Column("name", sa.String(), nullable=False),
        sa.Column("price", sa.Integer(), nullable=False),
        sa.Column("position", sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(["category_id"], ["category.id"]),
        sa.PrimaryKeyConstraint("id"),
    )

    op.create_table(
        "transaction",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.Column("deleted_at", sa.DateTime(), nullable=True),
        sa.Column("account_id", sa.Integer(), nullable=True),
        sa.Column(
            "method",
            sa.Enum(
                "cash", "card", "account", "transfer", "other", name="payment_methods"
            ),
            nullable=True,
        ),
        sa.Column("success", sa.Boolean(), nullable=True),
        sa.ForeignKeyConstraint(
            ["account_id"],
            ["account.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )

    op.create_table(
        "order",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.Column("deleted_at", sa.DateTime(), nullable=True),
        sa.Column("product_id", sa.Integer(), nullable=True),
        sa.Column("transaction_id", sa.Integer(), nullable=True),
        sa.Column("price", sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(["product_id"], ["product.id"]),
        sa.ForeignKeyConstraint(["transaction_id"], ["transaction.id"]),
        sa.PrimaryKeyConstraint("id"),
    )


def downgrade():
    op.drop_table("order")
    op.drop_table("transaction")
    op.drop_table("product")
    op.drop_table("login_token")
    op.drop_table("user")
    op.drop_table("configuration")
    op.drop_table("category")
    op.drop_table("account")
