"""Add long_login to User

Revision ID: baecfe64c7ee
Revises: c1f05c300432
Create Date: 2019-07-06 17:17:52.522087

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "baecfe64c7ee"
down_revision = "c1f05c300432"
branch_labels = ()
depends_on = None


def upgrade():
    op.add_column("user", sa.Column("long_login", sa.Boolean(), nullable=True))


def downgrade():
    op.drop_column("user", "long_login")
