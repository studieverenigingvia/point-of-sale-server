"""settings to uppercase

Revision ID: 8193203c033f
Revises: 6def7d1e1b67
Create Date: 2023-03-11 16:15:45.321264

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "8193203c033f"
down_revision = "6def7d1e1b67"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
UPDATE setting
SET key = UPPER(key)
    """
    )


def downgrade():
    pass
