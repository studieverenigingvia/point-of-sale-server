"""Convert all transactions to double-entry

Revision ID: d1b9a9a57944
Revises: eb9c9d7dae91
Create Date: 2018-10-16 09:31:18.013743

"""
from alembic import op
from sqlalchemy.orm import Session, load_only
from tqdm import tqdm

from app.migrations.util.transaction import Transaction
from app.migrations.util.order import Order


# revision identifiers, used by Alembic.
revision = "d1b9a9a57944"
down_revision = "eb9c9d7dae91"
branch_labels = ()
depends_on = None


def upgrade():
    session = Session(bind=op.get_bind())

    transactions = (
        session.query(Transaction).order_by(Transaction.created_at.desc()).all()
    )

    for transaction in tqdm(transactions):
        total = sum(o.price for o in transaction.orders)
        if total <= 0:
            continue

        order = transaction.orders[0]
        if order.method == "sepa":
            continue

        compensation = Order(
            account_id=order.account_id,
            method=order.method,
            price=total,
            transaction_id=transaction.id,
        )
        session.add(compensation)

        for order in transaction.orders:
            order.price = -order.price
            order.account_id = None
            session.add(order)

    session.commit()


def downgrade():
    pass
