"""user register

Revision ID: ea4bcb13cf96
Revises: 511a3b0ad3a1
Create Date: 2019-04-24 14:40:06.520876

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "ea4bcb13cf96"
down_revision = "511a3b0ad3a1"
branch_labels = ()
depends_on = None


def upgrade():
    op.add_column("user", sa.Column("register_id", sa.Integer(), nullable=True))
    op.create_foreign_key(None, "user", "register", ["register_id"], ["id"])


def downgrade():
    op.drop_constraint("user_register_id_fkey", "user", type_="foreignkey")
    op.drop_column("user", "register_id")
