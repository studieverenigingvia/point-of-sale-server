"""register capabilities

Revision ID: 824d5013a8f7
Revises: 414cfd0d94e8
Create Date: 2019-04-23 22:28:26.614247

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "824d5013a8f7"
down_revision = "414cfd0d94e8"
branch_labels = ()
depends_on = None


def upgrade():
    op.add_column(
        "register",
        sa.Column(
            "capabilities", sa.Integer(), nullable=False, default=0, server_default="0"
        ),
    )


def downgrade():
    op.drop_column("register", "capabilities")
