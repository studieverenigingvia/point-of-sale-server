"""Add nonce to transaction

Revision ID: 26c7a8c4c8df
Revises: 87b3157555d4
Create Date: 2018-11-12 12:29:52.662605

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = "26c7a8c4c8df"
down_revision = "87b3157555d4"
branch_labels = ()
depends_on = None


def upgrade():
    op.add_column("transaction", sa.Column("nonce", sa.String(), nullable=True))
    op.create_unique_constraint(None, "transaction", ["nonce"])


def downgrade():
    op.drop_constraint("transaction_nonce_key", "transaction", type_="unique")
    op.drop_column("transaction", "nonce")
