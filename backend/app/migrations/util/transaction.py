from sqlalchemy import orm
import sqlalchemy as sa
from datetime import datetime
import app


class Transaction(app.migrations.util.Base):
    __tablename__ = "transaction"

    id = sa.Column(sa.Integer, primary_key=True)
    created_at = sa.Column(sa.DateTime, default=datetime.now)
    updated_at = sa.Column(sa.DateTime, default=datetime.now, onupdate=datetime.now)
    deleted_at = sa.Column(sa.DateTime, default=None)

    orders = orm.relationship("Order", backref="transaction", order_by="Order.price")
