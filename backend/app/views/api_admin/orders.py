import logging
from datetime import date, datetime

from fastapi import Depends
from pydantic import BaseModel
from sqlalchemy import func, and_, text, DateTime
from sqlalchemy.orm import Mapped
from sqlalchemy.sql import ColumnElement

from app.dependencies import get_date_range, UserRequestContext, admin_required
from app.models import Transaction, Order
from app.models.checkout import CheckoutMethod
from app.util.util import (
    DatetimeRangeParameters,
)
from app.views.api_admin import fapi_admin
from app.views.exceptions import NotImplementedException

_logger = logging.getLogger(__name__)


class CardOrderResponse(BaseModel):
    class DateCount(BaseModel):
        date: date
        amount: int
        count: int

    __root__: list[DateCount]


@fapi_admin.get("/orders/card/", response_model=CardOrderResponse)
def api_admin_orders_card(
    timezone: int = 1,
    date_range: DatetimeRangeParameters = Depends(get_date_range),
    context: UserRequestContext = Depends(admin_required),
) -> CardOrderResponse:
    created: Mapped[datetime] | ColumnElement[DateTime] = Transaction.created_at
    bind = context.db_session.bind
    if bind is not None and bind.dialect.name == "postgresql":

        # converts to UTC+x or UTC-X
        utc = f"UTC{timezone:+d}"
        created = created.op("AT TIME ZONE")(utc)
    elif timezone is not None:
        # AT TIME ZONE is Postgresql dialect
        raise NotImplementedException("timezone_not_implemented_for_sql_engine")

    query = (
        context.db_session.query(
            func.date(created).label("day"), func.sum(Order.price), func.count(Order.id)
        )
        .select_from(Order)
        .join(Transaction)
        .filter(
            and_(
                Order.deleted_at.is_(None),
                Transaction.deleted_at.is_(None),
                Order.method == CheckoutMethod.CARD,
                Order.price > 0,
                created.between(date_range.start, date_range.end),
            )
        )
        .group_by(text("day"))
        .order_by(text("day ASC"))
    )
    return CardOrderResponse(
        __root__=[
            CardOrderResponse.DateCount(date=e[0], amount=e[1], count=e[2])
            for e in query.all()
        ]
    )
