from fastapi import Depends, Query

from app.dependencies import admin_required, UserRequestContext
from app.models.audit_trail import AuditTrailData
from app.repository.util.pagination import (
    PageParameters,
    PaginatedResponse,
    SearchParameters,
    SortParameters,
    page_sort_parameters,
)
from app.service import audit_service
from app.views.api_admin import fapi_admin
from app.repository.util.filters import DateTimeFilter, LiteralEqualsFilter


@fapi_admin.get("/audit_log/", response_model=PaginatedResponse[AuditTrailData])
def api_admin_audit_log(
    context: UserRequestContext = Depends(admin_required),
    page_params: PageParameters = Depends(),
    search_params: SearchParameters = Depends(),
    sort_params: SortParameters = Depends(
        page_sort_parameters(
            "createdAt",
            "event",
            default_sort="-createdAt",
        )
    ),
    id_filter: LiteralEqualsFilter | None = Query(None, alias="idFilter"),
    created_at_filter: DateTimeFilter | None = Query(None, alias="createdAtFilter"),
    user_filter: LiteralEqualsFilter | None = Query(None, alias="userFilter"),
) -> PaginatedResponse[AuditTrailData]:
    return audit_service.audit_get_logs(
        context.db_session,
        page_params,
        search_params,
        sort_params,
        id_filter,
        created_at_filter,
        user_filter,
    )
