import logging

from fastapi import APIRouter, Depends

from app.dependencies import admin_required
from app.views.error_handling import ErrorResponse

_logger = logging.getLogger(__name__)

fapi_admin: APIRouter = APIRouter(
    prefix="/api/admin",
    tags=["Administrative"],
    dependencies=[Depends(admin_required)],
    responses={"4XX": {"model": ErrorResponse}},
)


from app.views.api_admin import (  # noqa: E402 F401
    accounts,
    bank_transactions,
    categories,
    ledgers,
    orders,
    products,
    sepa,
    slides,
    transactions,
    users,
    registers,
    settings,
    audit_log,
    exact,
    mollie,
    invoice,
)
