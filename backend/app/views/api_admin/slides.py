from http import HTTPStatus
from fastapi import Depends, Query, Response

from app.dependencies import admin_required, UserRequestContext
from app.repository.util.pagination import (
    PageParameters,
    PaginatedResponse,
    SearchParameters,
    SortParameters,
    page_sort_parameters,
)
from app.service import slide_service
from app.views.api_admin import fapi_admin
from app.repository.util.filters import BooleanFilter, LiteralEqualsFilter
from app.models.slide import NewSlideProductData, SlideProductData
from app.repository import (
    slides as slides_repo,
)


@fapi_admin.get("/slides/", response_model=PaginatedResponse[SlideProductData])
def api_admin_slides(
    context: UserRequestContext = Depends(admin_required),
    page_params: PageParameters = Depends(),
    search_params: SearchParameters = Depends(),
    sort_params: SortParameters = Depends(
        page_sort_parameters(
            "name",
            "order",
            "show",
            "trends",
            "spacing",
            default_sort="-show,order",
        )
    ),
    id_filter: LiteralEqualsFilter | None = Query(None, alias="idFilter"),
    product_filter: LiteralEqualsFilter | None = Query(None, alias="productFilter"),
    show_filter: BooleanFilter | None = Query(None, alias="showFilter"),
    trends_filter: BooleanFilter | None = Query(None, alias="trendsFilter"),
    spacing_filter: BooleanFilter | None = Query(None, alias="spacingFilter"),
) -> PaginatedResponse[SlideProductData]:
    return slides_repo.get_all(
        context.db_session,
        page_params,
        search_params,
        sort_params,
        id_filter,
        product_filter,
        show_filter,
        trends_filter,
        spacing_filter,
    )


@fapi_admin.put("/slides/{slide_id}/", response_model=SlideProductData)
def api_admin_slides_put(
    slide_id: int,
    updated_slide: SlideProductData,
    context: UserRequestContext = Depends(admin_required),
) -> SlideProductData:
    p = slide_service.update_slide_product(context, updated_slide)
    context.db_session.commit()
    return p


@fapi_admin.post(
    "/slides/", response_model=SlideProductData, status_code=HTTPStatus.CREATED
)
def api_admin_slides_post(
    new_slide: NewSlideProductData,
    context: UserRequestContext = Depends(admin_required),
) -> SlideProductData:
    p = slide_service.create_slide_product(context, new_slide)
    context.db_session.commit()
    return p


@fapi_admin.delete(
    "/slides/{slide_id}/",
    status_code=HTTPStatus.NO_CONTENT,
    response_class=Response,
)
def api_admin_slides_delete(
    slide_id: int,
    context: UserRequestContext = Depends(admin_required),
) -> None:
    slide_service.delete_slide_product(context, slide_id)
    context.db_session.commit()
