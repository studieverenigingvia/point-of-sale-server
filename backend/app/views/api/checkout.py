from datetime import datetime, timedelta
from http import HTTPStatus

import pydantic
from fastapi import Depends
from sqlalchemy.orm import Session
from starlette.responses import Response

from app import dependencies
from app.dependencies import UserRequestContext, RegisterCapabilities, get_db
from app.models.card_terminal import CardTerminalData
from app.models.checkout import CheckoutMethod
from app.models.register import RegisterCap
from app.models.transaction import TransactionData, TransactionStatus
from app.service import (
    register_service,
    transaction_service,
    user_condition_service,
    setting_service,
    card_reader_service,
)
from app.service.flow.flow_service import process_flow_init, process_flow_next
from app.service.flow.flow_types import (
    InitProcessFlowRequest,
    ProcessFlowPurchaseRequest,
    ProcessFlowUpgradeRequest,
    NextProcessFlowRequest,
    ProcessFlowAccountSource,
    StateTransitionOutput,
)
from app.service.flow.utils.cart import (
    CartItem,
    MAX_CHECKOUT_AMOUNT_ADMIN,
    MAX_CHECKOUT_AMOUNT,
)
from app.service.flow.utils.process_flow import ProcessFlowCardTerminalRequest
from app.util.util import PinStr
from app.views.api import fapi, fapi_auth
from app.views.api_admin.settings import AdminCheckoutReason
from app.views.exceptions import (
    ValidationException,
    AuthorizationException,
)

MAX_UNDO_TIMEOUT = timedelta(seconds=60)


class ReasonStr(pydantic.ConstrainedStr):
    strip_whitespace = True
    max_length = 60


class CardSecretStr(pydantic.ConstrainedStr):
    strip_whitespace = True
    max_length = 64


class CheckoutDirectJson(pydantic.BaseModel):
    nonce: str
    products: list[CartItem]  # Can be empty, with cash/card upgrade.
    upgrade_account_id: int | None
    upgrade_amount: int | None
    reason: str | None


class CheckoutAccountJson(pydantic.BaseModel):
    nonce: str
    products: list[CartItem] = pydantic.Field(..., min_items=1)
    account_id: int | None
    pin: PinStr | None


class CheckoutAccountReaderJson(pydantic.BaseModel):
    nonce: str
    products: list[CartItem] = pydantic.Field(..., min_items=1)
    cardSecret: CardSecretStr


class CheckoutResponse(pydantic.BaseModel):
    transaction_id: int


def api_checkout(
    request_json: CheckoutDirectJson,
    context: UserRequestContext,
    method: CheckoutMethod,
    desired_state: TransactionStatus = TransactionStatus.COMPLETED,
) -> tuple[CheckoutResponse, StateTransitionOutput]:
    """
    Controller for /api/checkout/(cash|card|other)/, which tries to perform
    a checkout for <method>, registering the checkout on success.

    Expects a JSON object with a products field, which is a list of CartEntry
    objects. A CartEntry object contains an id field (the product ID) and an
    amount field (how many times the product is ordered).
    """
    register = register_service.get_register_for_user(context.db_session, context.user)
    caps = register_service.get_capabilities_for_register(register)
    max_amount = (
        MAX_CHECKOUT_AMOUNT_ADMIN if context.user.is_admin else MAX_CHECKOUT_AMOUNT
    )

    if (
        request_json.upgrade_account_id is not None
        and request_json.upgrade_amount is not None
    ):
        if RegisterCap.PAYMENT_ACCOUNT_OTHER not in caps:
            raise AuthorizationException("insufficient_permissions")

    init = InitProcessFlowRequest(
        nonce=request_json.nonce,
        desired_state=desired_state,
        method=method,
        purchase_request=None,
        upgrade_request=None,
        account_source=None,
        card_terminal=None,
        reason=request_json.reason,
    )

    if request_json.products:
        init["purchase_request"] = ProcessFlowPurchaseRequest(
            request_json.products, register, max_amount
        )

    if request_json.upgrade_account_id is not None:
        if request_json.upgrade_amount is None:
            raise ValidationException("no_upgrade_amount_given")

        init["upgrade_request"] = ProcessFlowUpgradeRequest(
            request_json.upgrade_account_id,
            request_json.upgrade_amount,
            request_json.reason,
        )

    if method == CheckoutMethod.CARD:
        init["card_terminal"] = ProcessFlowCardTerminalRequest(register)

    result = process_flow_init(context, init)
    return CheckoutResponse(transaction_id=result.transaction.id), result


@fapi_auth.post(
    "/api/checkout/cash/",
    response_model=CheckoutResponse,
    dependencies=[
        Depends(RegisterCapabilities(require_any_of=[RegisterCap.PAYMENT_CASH]))
    ],
)
def api_checkout_cash(
    request_json: CheckoutDirectJson,
    context: UserRequestContext = Depends(dependencies.login_required),
) -> CheckoutResponse:
    return api_checkout(request_json, context, method=CheckoutMethod.CASH)[0]


class CheckoutCardResponse(CheckoutResponse):
    terminal: CardTerminalData | None


@fapi_auth.post(
    "/api/checkout/card/",
    response_model=CheckoutCardResponse,
    dependencies=[
        Depends(RegisterCapabilities(require_any_of=[RegisterCap.PAYMENT_CARD]))
    ],
)
def api_checkout_card(
    request_json: CheckoutDirectJson,
    context: UserRequestContext = Depends(dependencies.login_required),
) -> CheckoutCardResponse:
    api_response, state_output = api_checkout(
        request_json,
        context,
        method=CheckoutMethod.CARD,
        desired_state=TransactionStatus.PENDING,
    )

    return CheckoutCardResponse(
        transaction_id=api_response.transaction_id,
        terminal=state_output.terminal.to_pydantic() if state_output.terminal else None,
    )


class NextTransactionResponse(pydantic.BaseModel):
    transaction: TransactionData


class CheckoutNextRequest(pydantic.BaseModel):
    next_state: TransactionStatus


@fapi_auth.post(
    "/api/transactions/{transaction_id}/finish/",
    response_model=NextTransactionResponse,
    dependencies=[
        Depends(RegisterCapabilities(require_any_of=[RegisterCap.PAYMENT_CARD]))
    ],
)
def api_transaction_next(
    transaction_id: int,
    context: UserRequestContext = Depends(dependencies.login_required),
) -> NextTransactionResponse:
    transaction = transaction_service.get_by_id(context.db_session, transaction_id)

    if transaction.status != TransactionStatus.PENDING:
        raise ValidationException("invalid_transaction_state")

    next_request = NextProcessFlowRequest(
        transaction_id=transaction_id,
        desired_state=TransactionStatus.COMPLETED,
    )

    result = process_flow_next(context, next_request)

    return NextTransactionResponse(transaction=result.transaction.to_pydantic())


@fapi_auth.post(
    "/api/transactions/{transaction_id}/undo/",
    response_model=NextTransactionResponse,
    dependencies=[
        Depends(RegisterCapabilities(require_any_of=[RegisterCap.PAYMENT_UNDO]))
    ],
)
def api_transaction_undo(
    transaction_id: int,
    context: UserRequestContext = Depends(dependencies.login_required),
) -> NextTransactionResponse:
    transaction = transaction_service.get_by_id(context.db_session, transaction_id)

    if transaction.status != TransactionStatus.COMPLETED:
        raise ValidationException("invalid_transaction_state")

    if datetime.now() - transaction.created_at > MAX_UNDO_TIMEOUT:
        raise AuthorizationException("delete_timeout_expired")

    next_request = NextProcessFlowRequest(
        transaction_id=transaction_id,
        desired_state=TransactionStatus.DELETED,
    )

    result = process_flow_next(context, next_request)

    return NextTransactionResponse(transaction=result.transaction.to_pydantic())


@fapi_auth.post(
    "/api/checkout/other/",
    response_model=CheckoutResponse,
    dependencies=[
        Depends(RegisterCapabilities(require_any_of=[RegisterCap.PAYMENT_OTHER]))
    ],
)
def api_checkout_other(
    request_json: CheckoutDirectJson,
    context: UserRequestContext = Depends(dependencies.login_required),
) -> CheckoutResponse:
    return api_checkout(request_json, context, method=CheckoutMethod.OTHER)[0]


class CheckoutAccountResponse(CheckoutResponse):
    balance_before: int
    balance_after: int


@fapi_auth.post(
    "/api/checkout/account/",
    dependencies=[
        Depends(
            RegisterCapabilities(
                require_any_of=[
                    RegisterCap.PAYMENT_ACCOUNT_AUTHED,
                    RegisterCap.PAYMENT_ACCOUNT_OTHER,
                ]
            )
        )
    ],
)
def api_checkout_account(
    request_json: CheckoutAccountJson,
    context: UserRequestContext = Depends(dependencies.login_required),
) -> CheckoutAccountResponse:
    """
    Controller for /api/checkout/account/, which tries to perform a checkout
    for an account, checking its PIN and balance, registering the checkout
    on success.

    Expects a JSON object with a products field, which is a list of CartEntry
    objects, an account_id field (the ID of the credit account) and a pin field
    (string with the account's PIN code)A CartEntry object contains an id field
    (the product ID) and an amount field (how many times the product is
    ordered).
    """
    register = register_service.get_register_for_user(context.db_session, context.user)
    caps = register_service.get_capabilities_for_register(register)
    max_amount = (
        MAX_CHECKOUT_AMOUNT_ADMIN if context.user.is_admin else MAX_CHECKOUT_AMOUNT
    )
    check_pin = RegisterCap.PAYMENT_ACCOUNT_OTHER not in caps

    if (account_id := request_json.account_id) is None:
        raise ValidationException("account_id_missing")

    if check_pin and request_json.pin is None:
        raise ValidationException("pin_missing")

    req = InitProcessFlowRequest(
        nonce=request_json.nonce,
        desired_state=TransactionStatus.COMPLETED,
        method=CheckoutMethod.ACCOUNT,
        purchase_request=None,
        upgrade_request=None,
        account_source=ProcessFlowAccountSource(
            account_id=account_id,
            check_pin=check_pin,
            pin_code=request_json.pin,
        ),
        card_terminal=None,
        reason=None,
    )

    if request_json.products:
        req["purchase_request"] = ProcessFlowPurchaseRequest(
            request_json.products, register, max_amount
        )

    result = process_flow_init(context, req)

    assert result.balance, result

    return CheckoutAccountResponse(
        balance_before=result.balance.before,
        balance_after=result.balance.after,
        transaction_id=result.transaction.id,
    )


@fapi_auth.post(
    "/api/checkout/account/self/",
    response_model=CheckoutAccountResponse,
    dependencies=[
        Depends(
            RegisterCapabilities(
                require_any_of=[
                    RegisterCap.PAYMENT_ACCOUNT_SELF,
                ]
            )
        )
    ],
)
def api_checkout_account_self(
    request_json: CheckoutAccountJson,
    context: UserRequestContext = Depends(dependencies.login_required),
) -> CheckoutAccountResponse:
    """
    Controller for /api/checkout/account/self/, which tries to perform a checkout
    for a users own account.
    """
    user = context.user
    register = register_service.get_register_for_user(context.db_session, user)
    max_amount = MAX_CHECKOUT_AMOUNT_ADMIN if user.is_admin else MAX_CHECKOUT_AMOUNT

    if not user.account:
        raise ValidationException("no_linked_account")
    account_id = user.account.id
    if not user_condition_service.verify_accepted_condition(context):
        raise ValidationException("not_accepted_terms")

    req = InitProcessFlowRequest(
        nonce=request_json.nonce,
        desired_state=TransactionStatus.COMPLETED,
        method=CheckoutMethod.ACCOUNT,
        purchase_request=None,
        upgrade_request=None,
        account_source=ProcessFlowAccountSource(
            account_id=account_id,
            check_pin=False,
            pin_code=None,
        ),
        card_terminal=None,
        reason=None,
    )

    if request_json.products:
        req["purchase_request"] = ProcessFlowPurchaseRequest(
            request_json.products, register, max_amount
        )

    result = process_flow_init(context, req)
    assert result.balance, result

    return CheckoutAccountResponse(
        balance_before=result.balance.before,
        balance_after=result.balance.after,
        transaction_id=result.transaction.id,
    )


class CheckoutReaderResponse(CheckoutAccountResponse):
    accountName: str


@fapi.post(
    "/api/checkout/account/reader/",
    response_model=CheckoutReaderResponse,
    dependencies=[
        Depends(
            RegisterCapabilities(
                require_any_of=[
                    RegisterCap.PAYMENT_ACCOUNT_AUTHED,
                ]
            )
        )
    ],
)
def api_checkout_account_reader(
    request_json: CheckoutAccountReaderJson,
    context: UserRequestContext = Depends(dependencies.login_required),
) -> CheckoutReaderResponse:
    """
    Controller for /api/checkout/account/reader/, which tries to perform a checkout
    for a users own account using a pre-registered card.
    """
    register = register_service.get_register_for_user(context.db_session, context.user)
    max_amount = (
        MAX_CHECKOUT_AMOUNT_ADMIN if context.user.is_admin else MAX_CHECKOUT_AMOUNT
    )

    account = card_reader_service.get_account_for_card_secret(
        context.db_session, request_json.cardSecret
    )

    req = InitProcessFlowRequest(
        nonce=request_json.nonce,
        desired_state=TransactionStatus.COMPLETED,
        method=CheckoutMethod.ACCOUNT,
        purchase_request=None,
        upgrade_request=None,
        account_source=ProcessFlowAccountSource(
            account_id=account.id,
            check_pin=False,
            pin_code=None,
        ),
        card_terminal=None,
        reason=None,
    )

    if request_json.products:
        req["purchase_request"] = ProcessFlowPurchaseRequest(
            request_json.products, register, max_amount
        )

    result = process_flow_init(context, req)
    assert result.balance, result

    return CheckoutReaderResponse(
        balance_before=result.balance.before,
        balance_after=result.balance.after,
        transaction_id=result.transaction.id,
        accountName=account.name,
    )


class RegisterCheckoutCardRequest(pydantic.BaseModel):
    cardSecret: CardSecretStr
    accountId: int
    pin: PinStr


@fapi_auth.post(
    "/api/reader/register/",
    status_code=HTTPStatus.NO_CONTENT,
    response_class=Response,
    dependencies=[
        Depends(
            RegisterCapabilities(
                require_any_of=[
                    RegisterCap.PAYMENT_ACCOUNT_AUTHED,
                    # TODO Should this still require pin, when payments normally dont?
                    RegisterCap.PAYMENT_ACCOUNT_OTHER,
                ]
            )
        )
    ],
)
def register_checkout_card(
    request_json: RegisterCheckoutCardRequest,
    context: UserRequestContext = Depends(dependencies.login_required),
) -> None:
    card_reader_service.register_card_for_account(
        context, request_json.accountId, request_json.cardSecret, request_json.pin
    )


class ApiCheckoutReasonsResponse(pydantic.BaseModel):
    reasons: list[AdminCheckoutReason]


@fapi.get(
    "/api/checkout/reasons/",
    dependencies=[
        Depends(RegisterCapabilities(require_any_of=[RegisterCap.PAYMENT_OTHER]))
    ],
)
def api_checkout_other_reasons(
    db_session: Session = Depends(get_db),
) -> ApiCheckoutReasonsResponse:
    reasons = setting_service.get_db_settings(db_session).checkout_reasons

    return ApiCheckoutReasonsResponse(
        reasons=[AdminCheckoutReason(name=r.name, pin=PinStr(r.pin)) for r in reasons]
    )
