import pydantic
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

import config
from app.dependencies import (
    find_login_token,
    get_db,
    login_required,
    UserRequestContext,
)
from app.models import RegisterCap, LoginToken
from app.models.register import RegisterData
from app.service import register_service, setting_service, user_condition_service
from app.views.error_handling import ErrorResponse

fapi: APIRouter = APIRouter(tags=["Public"])
fapi_auth: APIRouter = APIRouter(
    tags=["Authenticated"],
    dependencies=[Depends(login_required)],
    responses={"4XX": {"model": ErrorResponse}},
)


class UserDetails(pydantic.BaseModel):
    id: int
    username: str
    name: str
    isAdmin: bool
    accountId: int | None
    viaductUserId: int | None


class ApiConfigResponse(pydantic.BaseModel):
    viaductClientId: str
    user: UserDetails | None
    # Register is a pydantic.BaseModel field.
    register_: RegisterData | None = pydantic.Field(default=None, alias="register")
    requireSettingAccountPin: bool | None
    requireTfa: bool | None
    aprilFoolsEnabled: bool
    posWrappedAvailable: bool
    acceptedCurrentCondition: bool | None


@fapi.get("/api/config/", response_model=ApiConfigResponse)
def api_config(
    token: LoginToken = Depends(find_login_token),
    db_session: Session = Depends(get_db),
) -> ApiConfigResponse:
    user_details = None
    require_tfa = None
    accepted_current_condition = None

    user = None
    if token:
        user = token.user
    if user and token:
        require_tfa = user.is_admin and not token.tfa_enabled

        user_details = {
            "id": user.id,
            "username": user.username,
            "isAdmin": user.is_admin and token.tfa_enabled,
            "accountId": user.account_id
            if user.account and not user.account.deleted
            else None,
            "viaductUserId": user.viaduct_user_id,
            "name": user.name,
        }
        accepted_current_condition = user_condition_service.verify_accepted_condition(
            UserRequestContext(db_session=db_session, user=user)
        )

    register_details = None
    require_account_pin = None

    if user:
        register_details = register_service.get_register_for_user(db_session, user)
        account_self = (
            RegisterCap.PAYMENT_ACCOUNT_SELF.value & register_details.capabilities
        ) != 0
        require_account_pin = account_self and bool(
            user.account and not user.account.deleted and not user.account.pin
        )

    settings = setting_service.get_db_settings(db_session)

    return ApiConfigResponse.parse_obj(
        {
            "viaductClientId": config.VIADUCT_CLIENT_ID,
            "user": user_details,
            "register": register_details.to_pydantic() if register_details else None,
            "requireSettingAccountPin": require_account_pin,
            "requireTfa": require_tfa,
            "aprilFoolsEnabled": settings.april_fools_enabled,
            "posWrappedAvailable": settings.pos_wrapped_available,
            "acceptedCurrentCondition": accepted_current_condition,
        }
    )


from app.views.api import (  # noqa: F401, E402
    accounts,
    checkout,
    health,
    login,
    mollie,
    products,
    slides,
    transactions,
    user_condition,
)
