import logging
from http import HTTPStatus

from fastapi import Depends
from sqlalchemy.orm import Session
from starlette.responses import JSONResponse

from app.dependencies import get_db
from app.service import health_service
from app.service.health_service import SystemHealth
from app.views.api import fapi

_logger = logging.getLogger(__name__)


@fapi.get(
    "/api/health/",
    response_model=SystemHealth,
    responses={200: {"model": SystemHealth}, 503: {"model": SystemHealth}},
)
def api_health(
    db_session: Session = Depends(get_db),
) -> JSONResponse:
    system_status = health_service.get_system_health(db_session)
    status = HTTPStatus.OK if system_status.healthy else HTTPStatus.SERVICE_UNAVAILABLE
    return JSONResponse(system_status.dict(), status_code=status)
