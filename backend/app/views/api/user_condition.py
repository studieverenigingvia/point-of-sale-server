import pydantic
from fastapi import Depends

from app.dependencies import login_required, UserRequestContext
from app.service import user_condition_service
from app.views.api import fapi_auth


class SerializedUserCondition(pydantic.BaseModel):
    id: int
    userId: int
    acceptedConditionVersion: int


class ApiUserConditionResponse(pydantic.BaseModel):
    acceptedCondition: SerializedUserCondition


@fapi_auth.post("/api/user/condition/", response_model=ApiUserConditionResponse)
def api_update_user_condition(
    context: UserRequestContext = Depends(login_required),
) -> ApiUserConditionResponse:
    condition = user_condition_service.update_condition(context)
    return ApiUserConditionResponse.parse_obj(
        {"acceptedCondition": condition.serialize()}
    )
