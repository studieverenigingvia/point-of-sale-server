import pydantic
from fastapi import Depends, Query

from app import dependencies
from app.dependencies import (
    AccountRequestContext,
    RegisterCapabilities,
    UserRequestContext,
)
from app.models import RegisterCap
from app.models.transaction import TransactionData, TransactionStatus
from app.repository.util.filters import DateTimeFilter, LiteralEqualsFilter, NullFilter
from app.repository.util.pagination import (
    PageParameters,
    PaginatedResponse,
    SearchParameters,
    SortParameters,
    page_sort_parameters,
)
from app.service import (
    transaction_service,
    card_terminal_update_service,
)
from app.views.api import fapi
from app.views.exceptions import ValidationException


@fapi.get("/api/transactions/", response_model=PaginatedResponse[TransactionData])
def api_transactions(
    page_params: PageParameters = Depends(),
    search_params: SearchParameters = Depends(),
    sort_params: SortParameters = Depends(
        page_sort_parameters(
            "created_at",
            default_sort="-created_at",
        )
    ),
    deleted_filter: NullFilter | None = Query(None, alias="deletedFilter"),
    flow_filter: LiteralEqualsFilter | None = Query(None, alias="flowFilter"),
    product_filter: LiteralEqualsFilter | None = Query(None, alias="productFilter"),
    created_at_filter: DateTimeFilter | None = Query(None, alias="createdAtFilter"),
    context: AccountRequestContext = Depends(dependencies.account_required),
) -> PaginatedResponse[TransactionData]:
    account_filter = LiteralEqualsFilter(context.account.id)
    return transaction_service.paginated_search_all_v2(
        context.db_session,
        page_params,
        search_params,
        sort_params,
        None,  # id filter
        deleted_filter,
        product_filter,
        account_filter,
        created_at_filter,
        flow_filter,
        None,  # sepa filter
    )


class TransactionDetailedStatus(pydantic.BaseModel):
    status: TransactionStatus
    details: str | None


@fapi.get(
    "/api/transactions/card/{transaction_id}/status/",
    response_model=TransactionDetailedStatus,
    dependencies=[
        Depends(RegisterCapabilities(require_any_of=[RegisterCap.PAYMENT_CARD]))
    ],
)
def api_transaction_status(
    transaction_id: int,
    context: UserRequestContext = Depends(dependencies.login_required),
) -> TransactionDetailedStatus:
    transaction = transaction_service.get_by_id(context.db_session, transaction_id)

    if not transaction.card_data:
        raise ValidationException("Transaction is not not a card payment.")

    if not transaction.status == TransactionStatus.PENDING:
        return TransactionDetailedStatus(
            status=transaction.status,
            details=None,
        )

    (
        details,
        status,
    ) = card_terminal_update_service.get_status_of_transaction_and_update(
        context.db_session, transaction
    )

    return TransactionDetailedStatus(
        status=status,
        details=details,
    )
