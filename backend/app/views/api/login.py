import logging

import pydantic
from fastapi import Depends
from sqlalchemy.orm import Session
from starlette.requests import Request

from app.dependencies import get_db, RequestContext
from app.models import User
from app.service import user_service, login_service
from app.views.api import fapi
from app.views.exceptions import AuthorizationException

_logger = logging.getLogger(__name__)


class LoginResponse(pydantic.BaseModel):
    user_id: int
    access_token: str
    username: str


def _login_response(
    context: RequestContext, user: User, tfa_enabled: bool = False
) -> LoginResponse:
    token = login_service.generate_login_token(context, user.id, tfa_enabled)
    return LoginResponse(
        user_id=user.id, access_token=token.token, username=user.username
    )


class CodeStr(pydantic.ConstrainedStr):
    strip_whitespace = True


class LoginOAuthJson(pydantic.BaseModel):
    code: CodeStr


@fapi.post("/api/login/oauth/viaduct/", response_model=LoginResponse)
def api_login_oauth_viaduct_callback(
    request: Request,
    request_json: LoginOAuthJson,
    db_session: Session = Depends(get_db),
) -> LoginResponse:
    """
    Controller which logs in the user and returns an access token for pos.

    Expects a json body with an access token for svia.nl on which token
    introspection can be applied.
    """
    _logger.debug("scope %s", str(request.scope))
    url_root = str(request.url.replace(path="/"))

    host = "https://svia.nl/"
    context = RequestContext(db_session=db_session)
    user, viaduct_user = user_service.login_with_viaduct_oauth(
        context, host, url_root, request_json.code
    )

    return _login_response(context, user, viaduct_user.tfa_enabled)


class UsernameStr(pydantic.ConstrainedStr):
    strip_whitespace = True
    to_lower = True


class LoginJson(pydantic.BaseModel):
    username: UsernameStr
    password: str


@fapi.post("/api/login/", response_model=LoginResponse)
def api_login(
    request_json: LoginJson,
    db_session: Session = Depends(get_db),
) -> LoginResponse:
    """
    Controller for /api/login/, which logs in the user and returns an access
    token.

    Expects a json body with a username and password field, and returns json
    containing the user ID and the access token for this session.
    """
    context = RequestContext(db_session=db_session)
    user = user_service.login_with_username_password(
        context,
        username=request_json.username,
        password=request_json.password,
    )

    if not user:
        raise AuthorizationException("invalid_username_password")

    return _login_response(context, user)
