# How to use a production database locally

## Obtaining a copy from the server.
1. SSH into the server

2. Execute the following

Note that this might take a while.

`docker exec -t possvianl_db_1 pg_dumpall -c -U postgres > pos-03-11-2021.sql`

3. Copy to local compyter
`scp svia.nl:pos-03-11-2021.sql .`

## Load database locally

(When not using docker, execute the commands directly)

1. Make sure the backend and worker are not running.

2. Create extra role.

This is needed because we cannot rename a database that we are currently connected to.

`docker-compose exec database psql -U pos -c 'CREATE ROLE pos2 LOGIN; ALTER ROLE pos2 SUPERUSER;';`

`docker-compose exec database psql -U pos -c 'CREATE DATABASE pos2;';`

3. Rename database
`docker-compose exec database psql -U pos2 -c 'ALTER DATABASE pos RENAME TO pos_temp;';`

4. Copy production database into the Docker container
`docker cp pos-03-11-2021.sql point-of-sale-server_database_1:.`

5. Execute all SQL commands
`docker-compose exec database psql -U pos2 -f pos-03-11-2021.sql`

6. Fix the password
`docker-compose exec database psql -U postgres -c 'ALTER USER pos WITH PASSWORD 'pos123';';`

### Use a temp database to swap the databases around:
`docker-compose exec database psql -U pos -c 'ALTER DATABASE pos RENAME TO pos_temp;';`
