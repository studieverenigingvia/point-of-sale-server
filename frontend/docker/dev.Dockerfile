FROM node:18-alpine
WORKDIR /app

RUN npm install -g npm

COPY package.json package-lock.json tsconfig.json vite.config.ts ./
RUN npm install

CMD npm run start
