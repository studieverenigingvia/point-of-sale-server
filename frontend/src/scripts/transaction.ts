import type { Order } from "./types";
import { TransactionFlowType } from "./types";

export function transactionFlowName(flow: string) {
  switch (flow) {
    case TransactionFlowType.ACCOUNT_PAYMENT:
      return "Account Payment";
    case TransactionFlowType.OTHER_ADMIN_PAYMENT:
      return "Other Top-up";
    case TransactionFlowType.OTHER_PAYMENT:
      return "Other Payment";
    case TransactionFlowType.CARD_PAYMENT:
      return "Card Payment";
    case TransactionFlowType.CARD_ADMIN_PAYMENT:
      return "Card Payment (Legacy)";
    case TransactionFlowType.IDEAL_PAYMENT:
      return "iDeal Payment";
    case TransactionFlowType.SEPA_PAYMENT:
      return "Direct Debit Top-up";
    case TransactionFlowType.CASH_PAYMENT:
      return "Cash Payment";
    case TransactionFlowType.TRANSFER_PAYMENT:
      return "Transfer";
    default:
      return flow;
  }
}

export function transactionMethodName(method: string) {
  switch (method) {
    case "cash":
      return "Cash";
    case "card":
      return "PIN";
    case "account":
      return "Account";
    case "ideal":
      return "iDEAL";
    case "sepa":
      return "Direct Debit";
    case "other":
      return "Other";
    case "transfer":
      return "Transfer";
    default:
      return method;
  }
}

export function orderTypeName(orderType: Order["type"], price?: number) {
  switch (orderType) {
    case "ACCOUNT_TOP_UP_OR_ACCOUNT_PAYMENT":
      if (typeof price === "number" && price < 0) {
        return "Top-up";
      } else if (typeof price === "number" && price > 0) {
        return "Payment";
      }
      return "Top-up / Account Payment";
    case "DIRECT_PAYMENT":
      return "Payment";
    case "PRODUCT_PURCHASE":
      return "Purchase";
    case "INVALID":
      return "Invalid";
  }
}
