import { Api, ApiResponse } from "./api";
import { CheckoutReason } from "../types";

export interface NotificationSettings {
  notificationSepaDestination: string;
  notificationAccountRequestDestination: string;
  notificationProductQuantityDestination: string;
  notificationSender: string;
}

export interface NotificationSenderOptions {
  options: string[];
}

export class SettingsApi extends Api {
  public async notifications(): Promise<ApiResponse<NotificationSettings>> {
    return this.get<NotificationSettings>("/api/admin/settings/notification/");
  }

  public async postNotifications(data: NotificationSettings) {
    return this.post<void>("/api/admin/settings/notification/", data);
  }

  public async notificationSenderOptions(): Promise<
    ApiResponse<NotificationSenderOptions>
  > {
    return this.get<NotificationSenderOptions>(
      "/api/admin/settings/notification/sender_options/"
    );
  }

  async setCheckoutReasons(reasons: CheckoutReason[]) {
    return this.post<unknown>("/api/admin/checkout/reasons/", {
      reasons: reasons,
    });
  }
}

export const settingsApi = new SettingsApi();
