import { Api, ApiResponse } from "./api";
import { queryString } from "../util";

export class AuditLog {
  comment: string | null;
  createdAt: string;
  event: string;
  objectId: number | null;
  resource: string;
}

export interface AuditPage {
  data: AuditLog[];
  page: number;
  perPage: number;
  pageCount: number;
}

export class AuditPageResonse {
  auditPage: AuditPage;
}

class AuditApi extends Api {
  public async search(
    page?: number,
    search?: string,
    userId?: number
  ): Promise<ApiResponse<AuditPageResonse>> {
    // Page 0 means the last page, page 1 means second to last etc
    const query = {};

    if (typeof page === "number") {
      query["page"] = page;
    }

    if (typeof search === "string") {
      query["search"] = search;
    }

    if (typeof userId === "number") {
      query["userId"] = userId;
    }

    return this.get<AuditPageResonse>(
      "/api/admin/audit_log/" + queryString(query)
    );
  }
}

export const auditApi = new AuditApi();
