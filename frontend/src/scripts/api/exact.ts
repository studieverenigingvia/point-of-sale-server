import { Api, ApiResponse, isApiResponse } from "./api";
import { notification } from "ant-design-vue";
import { queryString } from "../util";

export interface BaseExactItem {
  code: string;
}
export interface ExactLedger extends BaseExactItem {
  description: string;
}

export interface ExactDivision extends BaseExactItem {
  name: string;
}

export interface ExactCostCenter extends BaseExactItem {
  description: string;
}

export interface ExactVatCode extends BaseExactItem {
  description: string;
  percentage: number;
  type: "E" | "I" | "N" | "B";
}

interface LedgerResponse {
  ledgers: ExactLedger[];
}

interface CostCenterResponse {
  costCenters: ExactCostCenter[];
}

interface VatRespsponse {
  vatCodes: ExactVatCode[];
}

interface ReadyResponse {
  allLedgersSet: boolean;
  setupFinished: boolean;
}

interface TokenResponse {
  finished: boolean;
}

interface ExactDivisionResponse {
  current: string;
  administrations: ExactDivision[];
}

interface OldDivisionResponse {
  oldDivision: number | null;
}

interface ExactUrlResponse {
  url: string;
}

export interface ExactExportStatusMessage {
  status: "error" | "warn" | "success" | "fatal";
  description?: string;
  node: string;
  data?: string;
}

export interface ExactExport {
  id: number | null;
  createdAt: string | null;
  date: string;
  division: string | null;
  data: string | null;
  status: "exported" | "pending" | "deleted" | "unexported" | "error";
  orderCount: number;
  messages: ExactExportStatusMessage[];
}

export class ExactApi extends Api {
  hasNotifiedTokenExpired = false;

  protected executeRequest<T>(
    method: string,
    path: string,
    data?: any
  ): Promise<ApiResponse<T>> {
    const promise = super.executeRequest<T>(method, path, data);
    promise.catch((e) => {
      if (isApiResponse(e)) {
        if (
          e.errorIdentifier === "exact_token_expired" &&
          !this.hasNotifiedTokenExpired
        ) {
          notification.error({
            message: "Exact authentication failure",
            description: "Please refresh authentication using dropdown menu.",
            placement: "topLeft", // Do not cover dropdown.
            duration: 10,
          });

          this.hasNotifiedTokenExpired = true;
        }
      }
    });
    return promise;
  }

  public async getLedgers(): Promise<ApiResponse<LedgerResponse>> {
    return this.get<LedgerResponse>(`/api/admin/exact/ledgers/`);
  }

  public async getCostCenters(): Promise<ApiResponse<CostCenterResponse>> {
    return this.get<CostCenterResponse>(`/api/admin/exact/cost-centers/`);
  }

  public async getVatCodes(): Promise<ApiResponse<VatRespsponse>> {
    return this.get<VatRespsponse>(`/api/admin/exact/vat-codes/`);
  }

  public async getDivisions(): Promise<ApiResponse<ExactDivisionResponse>> {
    return this.get<ExactDivisionResponse>("/api/admin/exact/divisions/");
  }

  public async unlink(): Promise<ApiResponse<unknown>> {
    return this.post<unknown>("/api/admin/exact/setup/unlink/", undefined);
  }

  public async ready(): Promise<ApiResponse<ReadyResponse>> {
    return this.get<ReadyResponse>("/api/admin/exact/export/ready/");
  }

  public async reauth(): Promise<ApiResponse<ExactUrlResponse>> {
    return this.get<ExactUrlResponse>("/api/admin/exact/setup/reauth/");
  }

  public async oldDivision(): Promise<ApiResponse<OldDivisionResponse>> {
    return this.get<OldDivisionResponse>(
      "/api/admin/settings/exact/old_division/"
    );
  }

  public async setupToken(
    authenticationCode: string
  ): Promise<ApiResponse<TokenResponse>> {
    return this.post<TokenResponse>("/api/admin/exact/setup/token/", {
      code: authenticationCode,
    });
  }

  async deleteExport(exportId: number): Promise<ApiResponse<ExactExport>> {
    return this.delete<ExactExport>(
      `/api/admin/exact/export/${exportId}/`,
      undefined
    );
  }

  async getExport(exportId: number): Promise<ApiResponse<ExactExport>> {
    return this.get<ExactExport>(`/api/admin/exact/export/${exportId}/`);
  }

  async getExportJsonDump(
    year: number,
    month: number,
    day: number
  ): Promise<string> {
    const query = queryString({
      year: year,
      month: month,
      day: day,
    });
    return JSON.stringify(
      await Api.rawRequest("GET", `/api/admin/exact/export/data.json${query}`),
      null,
      4
    );
  }

  async export(
    year: number,
    month: number,
    day: number
  ): Promise<ApiResponse<{ exportId: number }>> {
    const query = queryString({
      year: year,
      month: month,
      day: day,
    });
    return this.post<{ exportId: number }>(
      `/api/admin/exact/export/${query}`,
      undefined
    );
  }
}

export const exactApi = new ExactApi();
