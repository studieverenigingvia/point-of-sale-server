import { Api, urlWithSearchParams } from "./api";

export interface CardOrdersByDayItem {
  date: string;
  amount: number;
  count: number;
}

export class OrdersApi extends Api {
  public async getCardOrdersByDay(
    year: number
  ): Promise<CardOrdersByDayItem[]> {
    const start = `${year}-01-01`;
    const end = `${year}-12-31`;

    const d = new Date(start);

    // check what timezone that date was in (daylight saving or not)
    // results of this query won't be 100% correct when start and end dates
    // pass a time change. We don't care.
    const timezone = Math.round(d.getTimezoneOffset() / -60);

    return Api.rawRequest(
      "GET",
      urlWithSearchParams("/api/admin/orders/card/", { start, end, timezone })
    );
  }
}

export const ordersApi = new OrdersApi();
