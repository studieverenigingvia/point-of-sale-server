import { Api, urlWithSearchParams } from "./api";

export interface UpdateFromExactResponse {
  first_date: string | null;
  last_date: string | null;
}

export interface BankTransactionItem {
  date: string;
  count: number;
  amount: number;
}

export interface BankTransactionsResponse {
  bankTransactions: BankTransactionItem[];
}

export class BankTransactionsApi extends Api {
  public async getTransactions(
    year: number
  ): Promise<BankTransactionsResponse> {
    const start = `${year}-01-01`;
    const end = `${year}-12-31`;

    return Api.rawRequest(
      "GET",
      urlWithSearchParams("/api/admin/bank_transactions/", { start, end })
    );
  }

  public async updateFromExact(
    year?: number
  ): Promise<UpdateFromExactResponse> {
    return Api.rawRequest(
      "POST",
      urlWithSearchParams("/api/admin/bank_transactions/exact/", { year })
    );
  }
}

export const bankTransactionsApi = new BankTransactionsApi();
