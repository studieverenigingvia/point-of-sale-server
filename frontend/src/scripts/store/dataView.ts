import { accountsApi } from "../api/accounts";
import { Account } from "../types";
import { unwrapPagination } from "../api/api";

/**
 * This store contains data that is used in components such as `LedgerDisplay`. These components
 * require knowledge of ledgers before they can display data or modify it.
 *
 * Note that currently this data is never invalidated (e.g. when other admins add a new ledger
 * and assign it to a product). This means `[X]Display` components should be written defensively
 * to not crash when for example a product has a non-existing ledger or category.
 */
class DataViewStore {
  _ready = false;

  private _ledgers: Account[] = [];

  get isReady() {
    return this._ready;
  }

  get ledgers(): Account[] {
    return this._ledgers;
  }

  public async initialize() {
    if (this._ready) {
      return;
    }

    await Promise.all([this.updateLedgers()]);

    this._ready = true;
  }

  public async updateLedgers() {
    try {
      // This might throw if we ever start using dataView for non-admins
      this._ledgers = await unwrapPagination(
        accountsApi.ledgers.bind(accountsApi)
      );
    } catch (e) {
      console.error(e);
    }
  }
}

export const dataViewStore = new DataViewStore();
export default dataViewStore;
