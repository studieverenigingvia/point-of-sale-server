import { useRouteQuery } from "@vueuse/router";
import { computed } from "vue";

/**
 * Overload for vue-use's useRouteQuery that always returns a number.
 * Note: Currently there are no overloads for types besides number.
 */
export function useTypedRouteQuery(name: string, defaultValue: number) {
  const data = useRouteQuery(name, defaultValue + "");

  return computed<number>({
    get() {
      const numberValue = +data.value;
      if (Number.isNaN(numberValue)) {
        return defaultValue;
      }

      return numberValue;
    },
    set(v) {
      data.value = v + "";
    },
  });
}
