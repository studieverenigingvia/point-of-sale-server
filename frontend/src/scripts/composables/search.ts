import { useTypedRouteQuery } from "./query";
import { Ref, watchEffect } from "vue";
import { useRouteQuery } from "@vueuse/router";
import { notification, PaginationProps } from "ant-design-vue";
import { usePaginatedSearchPagedQuery } from "../query/pagination";
import { PydanticError } from "../api/api";

export const DEFAULT_PAGE = 1 as const;
export const DEFAULT_PAGE_SIZE = 15 as const;

export interface AntPaginationProps extends PaginationProps {
  // Make these three properties non-optional
  current: number;
  pageSize: number;
  total: number;
}

export interface UseSearchOptions {
  /**
   * onChangePage can be used to alter the query before executing.
   */
  onChangePage?: (page: number) => void;
  /**
   * Additional query parameters to be used and shown in the url bar.
   */
  extraQueryParameters?: string[];
  /**
   * Query parameters that are sent but not shown to the user.
   */
  hiddenQueryParameters?: Record<string, string>;
}

export function useSearch<T>(endpoint: string, options: UseSearchOptions = {}) {
  const currentPageQuery = useTypedRouteQuery("page", DEFAULT_PAGE);
  const pageSize = useTypedRouteQuery("pageSize", DEFAULT_PAGE_SIZE);

  const searchQuery = useRouteQuery<string>("search", "");
  const sortQuery = useRouteQuery<string>("sort", "");

  const extraQueryParameters: Record<string, Ref<string>> = {};
  for (const param of options.extraQueryParameters || []) {
    extraQueryParameters[param] = useRouteQuery<string>(param, "");
  }

  const {
    data,
    pagination,
    isLoading,
    changePage,
    error,
    status,
    currentPage,
  } = usePaginatedSearchPagedQuery<T>(
    endpoint,
    {
      perPage: pageSize.value,
      page: currentPageQuery.value,
    },
    {
      search: searchQuery,
      sort: sortQuery,
      ...extraQueryParameters,
      ...options.hiddenQueryParameters,
    },
    {
      onChangePage: options.onChangePage,
    }
  );

  watchEffect(() => {
    // Syncs currentPage to query
    currentPageQuery.value = currentPage.value;
  });

  const unwatchError = watchEffect(() => {
    if (status.value === "error") {
      unwatchError();

      if (error.value instanceof PydanticError) {
        notification.error({
          message: "Failed to retrieve data.",
          description: error.value.toDiv(),
        });
      } else {
        notification.error({
          message: "Failed to retrieve data.",
          description: "Unknown error.",
        });

        throw new Error("Unable to get data: " + JSON.stringify(error.value));
      }
    }
  });

  return {
    data,
    pagination,
    isLoading,
    searchQuery,
    extraQueryParameters,
    sortQuery,
    changePage,
  };
}
