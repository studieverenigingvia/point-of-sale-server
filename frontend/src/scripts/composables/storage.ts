import { useStorage } from "@vueuse/core";

import dayjs from "dayjs";
import type { Dayjs } from "dayjs";

export function useDateStorage(
  key: string,
  defaultDate: Dayjs = dayjs().startOf("year")
) {
  return useStorage(key, defaultDate, undefined, {
    serializer: {
      read: (v: any) => (v ? dayjs(v) : defaultDate),
      write: (v: Dayjs) => v.format("YYYY-MM-DD"),
    },
  });
}
