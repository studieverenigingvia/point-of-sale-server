import { Account } from "./types";

export class Category {
  public id: number;
  public name: string;
  public position: number;
  public color: number;
}

export interface ProductData extends NewProductData {
  id: number;
}

// What is needed for our view.
export interface NewProductViewModel {
  ledgerId?: number;
  name: string;
  price: number;
  deposit: boolean;
  categoryIds: number[];
  quantity: number;
}

// This is what needed on the server-side.
export interface NewProductData extends NewProductViewModel {
  ledgerId: number;
}

export interface CategoryData {
  id: number;
  name: string;
  position: number;
  color: number;
  deleted: boolean;
}

export type NewCategoryData = Omit<CategoryData, "id">;

export class Product implements ProductData {
  public static compare(a: Product, b: Product): number {
    const aName = a.name.toLowerCase();
    const bName = b.name.toLowerCase();

    if (a.deleted == b.deleted) {
      if (aName < bName) {
        return -1;
      } else if (aName > bName) {
        return 1;
      } else {
        return 0;
      }
    } else {
      if (a.deleted) {
        return 1;
      } else if (b.deleted) {
        return -1;
      } else {
        return 0;
      }
    }
  }

  public id: number;
  public ledgerId: number;
  public name: string = "";
  public price: number = 0;
  public deposit: boolean = false;
  public categoryIds: number[];
  public quantity: number = 0;

  // The following fields are only used for the client-side:
  public priceStr: string = "0";
  public deleted: boolean = false;
  public updated: boolean = false;
}

export class SepaBatch {
  public id: number;
  public description: string;
  public executionDate: Date;
  public mailDate: Date | null;
  public success: boolean;
  public deletedAt: string | null;
}

export interface IdealPayment {
  id: number;
  createdAt: string;
  updatedAt: string;
  account: Account;
  status: string;
  mollieId: string;
}

export interface Invoice {
  vendor: string;
  errors: string[];
  line_items: InvoiceLineItem[];
}

export interface InvoiceLineItem {
  item: string;
  quantity: number;
  totalPrice: number;
  multipack: number;
  productId: number | null;
  productPrice: number;
  matchWasGuessed: boolean;

  originalProductId: number | null;
  originalMultipack: number;
}
