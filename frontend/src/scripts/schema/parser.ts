import { OpenAPIV3 } from "openapi-types";

// OpenAPIV3 does not support arbitrary http methods.
export type OpenAPIV3HttpMethods =
  | "get"
  | "put"
  | "post"
  | "delete"
  | "options"
  | "head"
  | "patch"
  | "trace";

export interface SchemaObjectMap {
  [key: string]: OpenAPIV3.SchemaObject;
}

export default class SchemaParser {
  document: OpenAPIV3.Document;

  constructor(document: OpenAPIV3.Document) {
    this.document = document;
  }

  resolveReference<T>(ref: string): T | null {
    if (!ref.startsWith("#/")) {
      // References can also be to e.g. other files. We do not support that.
      throw new Error("Unknown reference.");
    }

    ref = ref.slice(2);
    const refParts = ref.split("/");

    let obj = this.document;
    do {
      let key = refParts.shift();

      if (!key || !obj.hasOwnProperty(key)) {
        return null;
      }

      obj = obj[key];
    } while (refParts.length);

    return (obj as unknown) as T;
  }

  getParameterSchemaForPath(
    method: OpenAPIV3HttpMethods,
    path: string,
    parameterName: string
  ): OpenAPIV3.SchemaObject | null {
    const parameters = this.document.paths[path]?.[method]?.parameters;

    if (!parameters || !parameters.length) {
      return null;
    }

    let parameter: OpenAPIV3.ParameterObject | null = null;
    for (const p of parameters) {
      if ("$ref" in p) {
        const resolvedParameterObject = this.resolveReference<
          OpenAPIV3.ParameterObject
        >(p.$ref);

        if (resolvedParameterObject?.name === parameterName) {
          parameter = resolvedParameterObject;
          break;
        }
      } else {
        if (p.name === parameterName) {
          parameter = p;
          break;
        }
      }
    }

    if (!parameter) {
      return null;
    }

    const schema = parameter.schema;

    if (!schema) {
      return null;
    }

    if ("$ref" in schema) {
      return this.resolveReference<OpenAPIV3.SchemaObject>(schema.$ref);
    }

    return schema;
  }

  getSuccessResponseForPath(
    method: OpenAPIV3HttpMethods,
    path: string,
    type: string = "application/json"
  ): OpenAPIV3.SchemaObject | null {
    if (!this.document.paths[path]?.[method]) {
      return null;
    }

    const operation = this.document.paths[path]?.[method];

    if (!operation?.responses?.["200"]) {
      return null;
    }

    let response = operation.responses["200"];

    if ("$ref" in response) {
      const resolvedResponse = this.resolveReference<OpenAPIV3.ResponseObject>(
        response.$ref
      );

      if (!resolvedResponse) {
        return null;
      }

      response = resolvedResponse;
    }

    if (!response.content) {
      return null;
    }

    if (!(type in response.content)) {
      return null;
    }

    const schema = response.content[type].schema;

    if (!schema) {
      return null;
    }

    if ("$ref" in schema) {
      return this.resolveReference<OpenAPIV3.SchemaObject>(schema.$ref);
    }

    return schema;
  }

  getPaginatedResponseGenericChild(
    paginatedGeneric: OpenAPIV3.SchemaObject | null
  ): OpenAPIV3.SchemaObject | null {
    if (!paginatedGeneric) {
      return null;
    }

    // Title is of form `PaginatedResponse[ProductResponse]`.
    if (!/^PaginatedResponse\[.*\]$/.test(paginatedGeneric.title || "")) {
      return null;
    }

    if (!paginatedGeneric.properties?.items) {
      return null;
    }

    // Here 'items' is the actual name of the property we use (defined in Python)
    let childSchema = paginatedGeneric.properties.items;

    if ("$ref" in childSchema) {
      const resolvedChildSchema = this.resolveReference<OpenAPIV3.SchemaObject>(
        childSchema.$ref
      );

      if (!resolvedChildSchema) {
        return null;
      }

      childSchema = resolvedChildSchema;
    }

    // items is always an array within PaginatedResponse
    if (childSchema.type !== "array") {
      return null;
    }

    // Here 'items' comes from the OpenAPIV3 definition.
    if ("$ref" in childSchema.items) {
      return this.resolveReference(childSchema.items.$ref);
    }

    return childSchema.items;
  }

  getResolvedProperties(schema: OpenAPIV3.SchemaObject): SchemaObjectMap {
    if (!schema.properties) {
      return {};
    }

    const resolvedSchemaObjects: SchemaObjectMap = {};

    for (const propName of Object.keys(schema.properties)) {
      const prop = schema.properties[propName];

      if (!("$ref" in prop)) {
        resolvedSchemaObjects[propName] = prop;
        continue;
      }

      const resolved = this.resolveReference<OpenAPIV3.SchemaObject>(prop.$ref);
      if (resolved) {
        resolvedSchemaObjects[propName] = resolved;
      }
    }

    return resolvedSchemaObjects;
  }
}
