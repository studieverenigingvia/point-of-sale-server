{{/*
Expand the name of the chart.
*/}}
{{- define "pos.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "pos.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "pos.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "pos.labels" -}}
helm.sh/chart: {{ include "pos.chart" . }}
{{ include "pos.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "pos.selectorLabels" -}}
app.kubernetes.io/name: {{ include "pos.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "pos.lookupSecretKey" -}}
  {{- $namespace := index . 0 -}}
  {{- $secretName := index . 1 -}}
  {{- $key := index . 2 -}}
  {{- $secret := lookup "v1" "Secret" $namespace $secretName -}}
  {{- if $secret -}}
    {{- if hasKey $secret.data $key -}}
      {{- $secretValue := index $secret.data $key -}}
      {{ $secretValue | b64dec }}
    {{- else -}}
      {{- fail (printf "Secret '%s' does not have key '%s'" $secretName $key) -}}
    {{- end -}}
  {{- else -}}
    {{- fail (printf "Secret '%s' not found in namespace '%s'" $secretName $namespace) -}}
  {{- end -}}
{{- end -}}

{{- define "pos.databasePassword" -}}
  {{- if and .Values.postgresql.auth.existingSecret (not (empty .Values.postgresql.auth.existingSecret)) -}}
    {{ include "pos.lookupSecretKey" (list .Release.Namespace .Values.postgresql.auth.existingSecret "password") }}
  {{- else -}}
    {{ .Values.postgresql.auth.password }}
  {{- end -}}
{{- end -}}

{{- define "pos.brokerPassword" -}}
  {{- if and .Values.redis.auth.existingSecret (not (empty .Values.redis.auth.existingSecret)) -}}
    {{ include "pos.lookupSecretKey" (list .Release.Namespace .Values.redis.auth.existingSecret "redis-password") }}
  {{- else -}}
    {{ .Values.redis.auth.password }}
  {{- end -}}
{{- end -}}

{{- define "pos.databaseDsn" -}}
postgresql+psycopg2://{{ .Values.postgresql.auth.username }}:{{ (include "pos.databasePassword" .) }}@{{ .Release.Name }}-postgresql-hl/{{ .Values.postgresql.auth.database }}
{{- end -}}

{{- define "pos.brokerDsn" -}}
redis://:{{ (include "pos.brokerPassword" .) }}@{{ .Release.Name }}-redis-master
{{- end -}}

{{- define "pos.secretOrLiteral" -}}
{{- if .existingSecret }}
    valueFrom:
        secretKeyRef:
            name: {{ .existingSecret }}
            key: {{ .key }}
{{- else }}
    value: {{ .literal }}
{{- end }}
{{- end }}

{{/*
Create the environment variables used by the Python app
*/}}
{{- define "pos.appEnvironment" }}
  - name: SQLALCHEMY_DATABASE_URI
    value: {{ include "pos.databaseDsn" . }}
  - name: POSTGRES_HOST
    value: {{  .Release.Name }}-postgresql-hl
  - name: POSTGRES_USER
    value: {{ .Values.postgresql.auth.username }}
  - name: POSTGRES_PASS
    value: {{ (include "pos.databasePassword" .) }}
  - name: POSTGRES_DB
    value: {{ .Values.postgresql.auth.database }}
  - name: BROKER_URL
    value: {{ include "pos.brokerDsn" . }}
  - name: SENTRY_DSN
    value: {{ .Values.pos.sentry_dsn }}
  - name: ENVIRONMENT
    value: {{ .Values.pos.environment }}
  - name: AWS_ACCESS_KEY_ID
    value: {{ .Values.pos.s3.aws_access_key_id  }}
  - name: VIADUCT_CLIENT_ID
    value: {{ .Values.pos.viaduct_client_id  }}
  - name: VIADUCT_CLIENT_SECRET
{{- include "pos.secretOrLiteral" (dict "existingSecret" .Values.pos.viaduct_client_secret_existing_secret "literal" .Values.pos.viaduct_client_secret_literal "key" "token") }}
  - name: AWS_SECRET_ACCESS_KEY
{{- include "pos.secretOrLiteral" (dict "existingSecret" .Values.pos.s3.aws_secret_access_key_existing_secret "literal" .Values.pos.s3.aws_secret_access_key_literal "key" "s3-access-key") }}
  - name: MOLLIE_API_KEY
{{- include "pos.secretOrLiteral" (dict "existingSecret" .Values.pos.mollie_api_key_existing_secret "literal" .Values.pos.mollie_api_key_literal "key" "key") }}
{{ if .Values.pos.googleApiKeyExistingSecret }}
  - name: GOOGLE_API_JSON_KEY
    value: "/secrets/google/api-key.json"
{{ end }}
{{- end }}


{{/*
App volumes and volumeMounts for secret files (api keys).
*/}}
{{- define "pos.volumeMounts" -}}
{{- if .Values.pos.googleApiKeyExistingSecret }}
  - name: google-api-key-file
    mountPath: /secrets/google/
    readOnly: true
{{- end }}
{{- if .Values.pos.sepayExistingSecret }}
  - name: sepay-config
    mountPath: /app/sepay/sepay.pem
    subPath: sepay.pem
    readOnly: true
  - name: sepay-config
    mountPath: /app/sepay/viapos.pem
    subPath: viapos.pem
    readOnly: true
{{- end }}
{{- end }}

{{- define "pos.volumes" -}}
{{- if .Values.pos.googleApiKeyExistingSecret }}
  - name: google-api-key-file
    secret:
      secretName: {{ .Values.pos.googleApiKeyExistingSecret }}
      items:
        - key: google-api-key
          path: "api-key.json"
{{- end }}
{{- if .Values.pos.sepayExistingSecret }}
  - name: sepay-config
    secret:
      secretName: {{ .Values.pos.sepayExistingSecret }}
      items:
        - key: viapos.pem
          path: "viapos.pem"
        - key: sepay.pem
          path: "sepay.pem"
{{- end }}
{{- end }}
